string = "echo -n before1\"my world is $USER 'yes $USER'\"'hello 'world'hmm' | xargs 'ignore $USER'>>ou>t>>p|ut $PATH p'lols'lmao>|<"
# string = f"echo -n 'ignore $USER' \"account $USER\""
# string = "export a=\"echo hi | echo bye\""

# to fix:
# string = "echo \"hello world\"\"test\" $USER"
# string = "$USER '$HLLO' |"
# free functions

tokens = []
start = 0
i = 0
specials = ["|", ">", "<", '"', "'"]
and_exclude_chars = [">", "<", "|", " "]
is_word = False
while i < len(string):
    char = string[i]
    print(f"{i} {string[i:]}")
    if (char == '"'):
        i += 1
        start = i
        while (i < len(string) and string[i] != '"'):
            i += 1
        tokens.append((start, i)) # as a string
        if (i+1<len(string) and (string[i+1] not in and_exclude_chars)):
            tokens.append("AND")
        i += 1
    elif (char == "'"):
        i += 1
        start = i
        while (i < len(string) and string[i] != "'"):
            i += 1
        tokens.append((start, i)) # as a string without expansion
        if (i+1<len(string) and (string[i+1] not in and_exclude_chars)):
            tokens.append("AND")
        i += 1
    while (i < len(string) and string[i] == " "):
        i += 1
    start = i
    if (i < len(string) and (string[i] == '"' or string[i] == "'")):
        continue
    # handle start of string with special chars:
    if (i < len(string) and string[i] in specials):
        if (i + 1 < len(string)):
            if (string[i] == string[i + 1] and string[i] == ">"):
                i += 1
        i += 1
        tokens.append((start, i)) # as a special char
        continue
    while (i < len(string) and string[i] != ' ' and string[i] not in specials):
        i += 1
    # handle end of string with special chars:
    if (i < len(string) and string[i] in specials):
        tokens.append((start, i)) # as a string
        if (string[i] == "'" or string[i] == '"'):
            tokens.append("AND")
        i -= 1
    elif (start != i):
        tokens.append((start, i)) # as a string
    i += 1

for token in tokens:
    if (token == "AND"):
        print("AND")
    else:
        print(string[token[0]:token[1]])