# Minishell

## How it works
- Tokenization / Lexing: Break command into tokens: ARG_EXPAND, ARG_NO_EXPAND, PIPE, >, <, >>, <<, | (refer to lexer.py)
- Parsing: Makes sense of tokens by grouping them: ARG, |, redirections with ARG (e.g. > outfile)
- Executor: For each pipeline, (1) link redirections and pipes and, (2) create a fork process (except for parent-modifiable built-ins like exit, unset, expor

## Characteristics

### C1: The default value from variable expansion is an empty string
- Empty string tokens are not removed as empty strings could be valid arguments to a program

### C2: The input string is splitted by pipes into pipelines, and all programs in the pipes will run simultaneously / concurrently

### C3: The shell is inspired heavily by bash, but does not provide all the functionalities of bash
- Compared to zsh, there will only be one stdin to a program in a pipeline, with the latest one being that stdin
- Compared to zsh, there will only be one stdout to a program in a pipeline, with the latest one being that stdout
- Syntax error will be thrown, and nothing gets executed in all pipelines, when it does not follow the style: <operator> <arg>, while bash may handle it
- Only a generic "Syntax Error" message will be shown and nothing specific about it is known

### C4: Built-in echo will treat empty string arguments as arguments (and not ignore them like bash)

### C5: Only one instance of heredoc can be processed at anytime (i.e. multiple minishell instances running heredoc will have undefined behavior)

### C6: Export, unset that belongs in plural pipelines will continue run (they will always be run in the parent process, not forked)