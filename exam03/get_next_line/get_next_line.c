/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/18 10:35:11 by keyu              #+#    #+#             */
/*   Updated: 2024/05/18 12:18:48 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "get_next_line.h"

char	*agnl(int fd)
{
	int	rd_byte = 1;
	static char buff[100000];
	static int	i;
	int	k = 0;
	char	*line;

	buff[0] = 0;
	if (fd < 0 || BUFFER_SIZE <= 0)
		return (0);
	while (rd_byte > 0)
	{
		rd_byte = read(fd, buff + k, BUFFER_SIZE);
		if (rd_byte == -1 || buff[i] == 0)
			return (0);
		k += rd_byte;
	}
	rd_byte = 0;
	while (buff[i + rd_byte] && buff[i + rd_byte] != '\n')
		rd_byte++;
	line = malloc((1 + rd_byte + (buff[i + rd_byte] == '\n')) * sizeof(char));
	if (!line)
		return (0);
	line[rd_byte + (buff[i + rd_byte] == '\n')] = 0;
	k = 0;
	while (k <= rd_byte)
		line[k++] = buff[i++];
	return (line);
}
