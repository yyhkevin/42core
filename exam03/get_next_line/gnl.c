#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "get_next_line.h"

char	*gnl(int fd)
{
	int	rd = 1;
	static char	buffer[100000] = {0};
	int	k;
	char	*line;
	static int	i;

	k = 0;
	if (fd < 0 || BUFFER_SIZE <= 0)
		return (0);
	while (rd > 0)
	{
		rd = read(fd, buffer + k, BUFFER_SIZE);
		if (rd == -1 || buffer[i] == 0)
			return (0);
		k += rd;
	}
	rd = 0;
	while (buffer[i + rd] && buffer[i + rd] != '\n')
		rd++;
	line = malloc((1 + rd + (buffer[i + rd] == '\n')) * sizeof(char));
	if (!line)
		return (0);
	k = 0;
	line[rd + (buffer[i + rd] == '\n')] = 0;
	while (k <= rd)
		line[k++] = buffer[i++];
	return (line);
}
