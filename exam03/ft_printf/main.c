/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/18 09:54:36 by keyu              #+#    #+#             */
/*   Updated: 2024/05/18 10:13:31 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdarg.h>

int	ft_putstr(char *s)
{
	int	i;

	if (!s)
		return ft_putstr("(null)");
	i = 0;
	while (s[i])
		i += write(1, s + i, 1);
	return (i);
}

int	ft_putnbr(long n, int base)
{
	char	*hex;
	int		i;

	i = 0;
	hex = "0123456789abcdef";
	if (n < 0)
	{
		i += write(1, "-", 1);
		n *= -1;
	}
	if (n >= base)
		i += ft_putnbr(n / base, base);
	i += write(1, hex + (n % base), 1);
	return (i);
}

int	ft_printf(const char *f, ...)
{
	va_list	ptr;
	int		i;
	int		len;

	i = 0;
	len = 0;
	va_start(ptr, f);
	while (f[i])
	{
		if (f[i] == '%')
		{
			i++;
			if (f[i] == 's')
				len += ft_putstr(va_arg(ptr, char *));
			if (f[i] == 'd')
				len += ft_putnbr(va_arg(ptr, int), 10);
			if (f[i] == 'x')
				len += ft_putnbr(va_arg(ptr, unsigned int), 16);
		}
		else
			len += write(1, &f[i], 1);
		i++;
	}
	va_end(ptr);
	return (len);
}

#include <stdio.h>
int	main(int argc, char **argv)
{
	printf("%d\n", ft_printf("%s %d %x", "hello world", -81236237, 15));
}
