/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pipex_utils.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/02 11:32:32 by keyu              #+#    #+#             */
/*   Updated: 2023/12/02 12:31:50 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_pipex.h"

void	ft_err(char *msg)
{
	perror(msg);
	exit(EXIT_FAILURE);
}

void	ft_free_all(char *path, char **args)
{
	char	**p;

	if (path != 0)
		free(path);
	p = args;
	while (*p)
	{
		free(*p);
		p++;
	}
	free(args);
}

char	**obtain_paths(char **env)
{
	char	**splitted;

	splitted = 0;
	while (*env)
	{
		if (!ft_strncmp(*env, "PATH=", 5))
		{
			splitted = ft_split((*env) + 5, ':');
			break ;
		}
		env++;
	}
	return (splitted);
}

char	*obtain_path(char **paths, char *cmd)
{
	char	*temp[2];
	char	*output;

	output = 0;
	while (*paths)
	{
		if ((*paths)[ft_strlen(*paths) - 1] != '/')
		{
			temp[0] = ft_strjoin(*paths, "/");
			temp[1] = temp[0];
			temp[0] = ft_strjoin(temp[1], cmd);
			free(temp[1]);
		}
		else
			temp[0] = ft_strjoin(*paths, cmd);
		free(*paths);
		if (!output && !access(temp[0], X_OK))
			output = temp[0];
		else
			free(temp[0]);
		paths++;
	}
	return (output);
}
