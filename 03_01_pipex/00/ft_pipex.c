/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pipex.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/02 11:32:23 by keyu              #+#    #+#             */
/*   Updated: 2023/12/02 13:09:50 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_pipex.h"

static void	free_paths(char **paths)
{
	char	**p;

	p = paths;
	while (*p)
		free(*p++);
	free(paths);
}

static void	ft_exec_left(char *cmd, char *file, char **paths, int *pipe_fd)
{
	char	**args;
	char	*path;
	int		fd;

	close(pipe_fd[0]);
	fd = open(file, O_RDONLY);
	if (fd == -1)
	{
		close(pipe_fd[1]);
		free_paths(paths);
		ft_err("Error opening input file");
	}
	dup2(fd, STDIN_FILENO);
	close(fd);
	dup2(pipe_fd[1], STDOUT_FILENO);
	close(pipe_fd[1]);
	args = ft_split(cmd, ' ');
	path = obtain_path(paths, args[0]);
	execve(path, args, NULL);
	ft_err("Error executing left pipe");
	ft_free_all(path, args);
}

static void	ft_exec_right(char **paths, char *cmd, int *pipe_fd, int file_fd)
{
	char	**args;
	char	*path;

	close(pipe_fd[1]);
	if (file_fd == -1)
	{
		close(pipe_fd[0]);
		free_paths(paths);
		ft_err("Error opening output file");
	}
	args = ft_split(cmd, ' ');
	path = obtain_path(paths, args[0]);
	dup2(pipe_fd[0], STDIN_FILENO);
	close(pipe_fd[0]);
	dup2(file_fd, STDOUT_FILENO);
	close(file_fd);
	execve(path, args, NULL);
	ft_err("Error executing right pipe");
	ft_free_all(path, args);
}

static void	get_resources(int argc, char **env, struct s_rsrc *res, int *p_fd)
{
	if (argc != 5)
		ft_err("Not enough arguments");
	res -> paths = obtain_paths(env);
	if (!res -> paths)
		ft_err("Error getting path");
	res -> pid = pipe(p_fd);
	if (res -> pid == -1)
	{
		free_paths(res -> paths);
		ft_err("Error opening pipe");
	}
	res -> pid = fork();
	if (res -> pid == -1)
	{
		free_paths(res -> paths);
		close(p_fd[0]);
		close(p_fd[1]);
		ft_err("Error with fork");
	}
}

int	main(int argc, char **argv, char **env)
{
	int				pipe_fd[2];
	struct s_rsrc	res;
	int				ofile_fd;

	get_resources(argc, env, &res, pipe_fd);
	ofile_fd = open(argv[4], O_WRONLY | O_CREAT | O_TRUNC, 0644);
	if (res.pid == 0)
	{
		close(ofile_fd);
		ft_exec_left(argv[2], argv[1], res.paths, pipe_fd);
	}
	else
	{
		wait(NULL);
		ft_exec_right(res.paths, argv[3], pipe_fd, ofile_fd);
	}
	free(res.paths);
	return (0);
}
