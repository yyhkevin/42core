/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pipex.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/02 11:35:44 by keyu              #+#    #+#             */
/*   Updated: 2023/12/02 12:31:30 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PIPEX_H
# define FT_PIPEX_H
// pipe, fork, access, etc
# include <unistd.h>
// malloc
# include <stdlib.h>
// open, close
# include <fcntl.h>
// For size_t
# include <string.h>
// For printf
# include <stdio.h>
// wait
# include <sys/wait.h>

int		ft_strncmp(const char *s1, const char *s2, size_t n);
char	**ft_split(char const *s, char c);
char	*ft_strjoin(char const *s1, char const *s2);
int		ft_strlen(char *s);
char	**obtain_paths(char **env);
char	*obtain_path(char **paths, char *cmd);
int		ft_strlen(char *s);
int		ft_strncmp(const char *s1, const char *s2, size_t n);
void	ft_err(char *msg);
void	ft_free_all(char *path, char **args);

struct	s_rsrc
{
	int		pid;
	char	**paths;
};

#endif
