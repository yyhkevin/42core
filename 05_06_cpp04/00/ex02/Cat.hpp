#pragma once

#include "AAnimal.hpp"
#include <iostream>
#include "Brain.hpp"

class Cat : public AAnimal {
    private:
        Brain *_brain;
    public:
        Cat();
        Cat(Cat const &a);
        Cat & operator = (Cat const &a);
        virtual ~Cat();
        void makeSound() const;
        std::string getIdea(int index);
        void setIdea(int index, std::string idea);
};