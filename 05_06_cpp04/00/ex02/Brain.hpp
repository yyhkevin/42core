#pragma once

#include <iostream>

#define IDEA_COUNT 100

class Brain {
    private:
        std::string ideas[IDEA_COUNT];
    public:
        Brain();
        Brain(Brain const &b);
        Brain & operator = (Brain const &b);
        ~Brain();
        std::string getIdea(int index);
        void setIdea(int index, std::string idea);
};