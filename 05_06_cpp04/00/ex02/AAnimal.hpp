#pragma once

#include <iostream>

class AAnimal {
    protected:
        std::string _type;
    public:
        AAnimal();
        AAnimal(AAnimal const &a);
        AAnimal & operator = (AAnimal const &a);
        virtual ~AAnimal();
        virtual void makeSound() const = 0;
        std::string getType() const;
};