#include "Cat.hpp"

Cat::Cat() {
    std::cout << "Cat default constructor called" << std::endl;
    this->_type = "Cat";
    this->_brain = new Brain();
}

Cat::Cat(Cat const &a) {
    std::cout << "Cat copy constructor called" << std::endl;
    this->_type = a._type;
    this->_brain = new Brain();
    *(this->_brain) = *a._brain;
}

Cat & Cat::operator = (Cat const &a) {
    std::cout << "Cat copy assignment called" << std::endl;
    if (this != &a) {
        this->_type = a._type;
        // shallow copy:
        // this->_brain = a._brain;
        // deep copy:
        delete this->_brain;
        this->_brain = new Brain();
        *(this->_brain) = *a._brain;
    }
    return *this;
}

Cat::~Cat() {
    std::cout << "Cat destructor called" << std::endl;
    delete _brain;
}

void Cat::makeSound() const {
    std::cout << "meeeeeeow~" << std::endl;
}

std::string Cat::getIdea(int index) {
    return this->_brain->getIdea(index);
}

void Cat::setIdea(int index, std::string idea) {
    this->_brain->setIdea(index, idea);
}