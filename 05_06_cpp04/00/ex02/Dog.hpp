#pragma once

#include "AAnimal.hpp"
#include <iostream>
#include "Brain.hpp"

class Dog : public AAnimal {
    private:
        Brain *_brain;
    public:
        Dog();
        Dog(Dog const &a);
        Dog & operator = (Dog const &a);
        virtual ~Dog();
        void makeSound() const;
        std::string getIdea(int index);
        void setIdea(int index, std::string idea);
};