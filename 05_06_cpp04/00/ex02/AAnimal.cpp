#include "AAnimal.hpp"

AAnimal::AAnimal() : _type("Generic Animal") {
    std::cout << "Animal default constructor called" << std::endl;
}

AAnimal::AAnimal(AAnimal const &a) {
    std::cout << "Animal copy constructor called" << std::endl;
    *this = a;
}

AAnimal & AAnimal::operator = (AAnimal const &a) {
    std::cout << "Animal copy assignment called" << std::endl;
    if (this != &a) {
        this->_type = a._type;
    }
    return *this;
}

AAnimal::~AAnimal() {
    std::cout << "Animal destructor called" << std::endl;
}

void AAnimal::makeSound() const {
    std::cout << "Wait what animal am I...?" << std::endl;
}

std::string AAnimal::getType() const {
    return this->_type;
}
