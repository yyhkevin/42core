#include "Dog.hpp"

Dog::Dog() {
    std::cout << "Dog default constructor called" << std::endl;
    this->_type = "Dog";
    this->_brain = new Brain();
}

Dog::Dog(Dog const &a) {
    std::cout << "Dog copy constructor called" << std::endl;
    this->_type = a._type;
    this->_brain = new Brain();
    *(this->_brain) = *a._brain;
}

Dog & Dog::operator = (Dog const &a) {
    std::cout << "Dog copy assignment called" << std::endl;
    if (this != &a) {
        this->_type = a._type;
        // shallow copy:
        // this->_brain = a._brain;
        // deep copy:
        delete this->_brain;
        this->_brain = new Brain();
        *(this->_brain) = *a._brain;
    }
    return *this;
}

Dog::~Dog() {
    std::cout << "Dog destructor called" << std::endl;
    delete _brain;
}

void Dog::makeSound() const {
    std::cout << "Wooooof!" << std::endl;
}

std::string Dog::getIdea(int index) {
    return this->_brain->getIdea(index);
}

void Dog::setIdea(int index, std::string idea) {
    this->_brain->setIdea(index, idea);
}