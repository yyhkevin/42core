#include "AAnimal.hpp"
#include "Dog.hpp"
#include "Cat.hpp"
#include "WrongAnimal.hpp"
#include "WrongCat.hpp"

int main(void) {
    std::cout << "---------Basic construction + destruction without leaks" << std::endl;
    {
        const AAnimal* j = new Dog();
        const AAnimal* i = new Cat();
        delete j;//should not create a leak
        delete i;
    }
    // Does not work as abstract classes
    // std::cout << "---------Animal constructors" << std::endl;
    // {
    //     AAnimal a;
    //     AAnimal a_cp(a);
    //     a = a_cp;

    //     std::cout << a.getType() << std::endl;
    //     std::cout << a_cp.getType() << std::endl;
    // }
    std::cout << "---------Dog constructors" << std::endl;
    {
        std::cout << "---Dog default constructor" << std::endl;
        Dog d;
        std::cout << d.getType() << std::endl;
        std::cout << "---Dog copy constructor" << std::endl;
        Dog d_cp(d);
        std::cout << d_cp.getType() << std::endl;
        std::cout << "Idea 10: " << d_cp.getIdea(10) << std::endl;
        std::cout << "---Dog copy assignment" << std::endl;
        d = d_cp;
        std::cout << d.getType() << std::endl;
        std::cout << "(before change) Idea 10 of d: " << d.getIdea(10) << std::endl;
        std::cout << "(before change) Idea 10 of d_cp: " << d_cp.getIdea(10) << std::endl;
        d.setIdea(10, "New Doggo idea");
        std::cout << "(after change) Idea 10 of d: " << d.getIdea(10) << std::endl;
        std::cout << "(after change) Idea 10 of d_cp: " << d_cp.getIdea(10) << std::endl;

        Dog freshDoggo;
        AAnimal *dogAsAnimal = &freshDoggo;
        std::cout << "Dog as Animal type: " << dogAsAnimal->getType() << std::endl;
        std::cout << "Dog as Animal sound: ";
        dogAsAnimal->makeSound();
    }
    std::cout << "---------Cat constructors" << std::endl;
    {
        std::cout << "---Cat default constructor" << std::endl;
        Cat c;
        std::cout << c.getType() << std::endl;
        std::cout << "---Cat copy constructor" << std::endl;
        Cat c_cp(c);
        std::cout << c_cp.getType() << std::endl;
        std::cout << "Idea 10: " << c_cp.getIdea(10) << std::endl;
        std::cout << "---Cat copy assignment" << std::endl;
        c = c_cp;
        std::cout << c.getType() << std::endl;
        std::cout << "(before change) Idea 10 of d: " << c.getIdea(10) << std::endl;
        std::cout << "(before change) Idea 10 of d_cp: " << c_cp.getIdea(10) << std::endl;
        c.setIdea(10, "New kittie idea");
        std::cout << "(after change) Idea 10 of d: " << c.getIdea(10) << std::endl;
        std::cout << "(after change) Idea 10 of d_cp: " << c_cp.getIdea(10) << std::endl;

        Cat freshCat;
        AAnimal *catAsAnimal = &freshCat;
        std::cout << "Cat as Animal type: " << catAsAnimal->getType() << std::endl;
        std::cout << "Cat as Animal sound: ";
        catAsAnimal->makeSound();
    }
    std::cout << "---------Another way to test copy assignment" << std::endl;
    {
        Dog basic;
        {
            Dog tmp;
            tmp = basic;
            basic = tmp;
        }
    }
    std::cout << "---------Polymorphism in action" << std::endl;
    {
        // AAnimal generic;
        Dog good_doggo;
        Cat whimsical_kittie;
        AAnimal *animals[2] = {
            // &generic,
            &good_doggo,
            &whimsical_kittie
        };
        for (int i = 0; i < 2; i++) {
            std::cout << animals[i]->getType() << " makes this sound: ";
            animals[i]->makeSound();
            std::cout << "They also make this sound as AAnimal: ";
            animals[i]->AAnimal::makeSound();
        }
    }
    return 0;
}