#pragma once

#include <iostream>

class Animal {
    protected:
        std::string _type;
    public:
        Animal();
        Animal(Animal const &a);
        Animal & operator = (Animal const &a);
        virtual ~Animal();
        virtual void makeSound() const;
        std::string getType() const;
};