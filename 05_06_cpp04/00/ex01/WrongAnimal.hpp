#pragma once

#include <iostream>

class WrongAnimal {
    protected:
        std::string _type;
    public:
        WrongAnimal();
        WrongAnimal(WrongAnimal const &a);
        WrongAnimal & operator = (WrongAnimal const &a);
        ~WrongAnimal();
        void makeSound() const;
        std::string getType() const;
};