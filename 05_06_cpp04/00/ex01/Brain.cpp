#include "Brain.hpp"

Brain::Brain() {
    std::cout << "Brain constructor called" << std::endl;
    for (int i = 0; i < IDEA_COUNT; i++) {
        ideas[i] = "Idea A";
    }
}

Brain::Brain(Brain const &b) {
    std::cout << "Brain copy constructor called" << std::endl;
    *this = b;
}

Brain & Brain::operator = (Brain const &b) {
    std::cout << "Brain copy assignment called" << std::endl;
    if (this != &b) {
        for (int i = 0; i < IDEA_COUNT; i++) {
            ideas[i] = b.ideas[i];
        }
    }
    return *this;
}

Brain::~Brain() {
    std::cout << "Brain destructor called" << std::endl;
}

std::string Brain::getIdea(int index) {
    if (index < 0 || index > IDEA_COUNT)
        return "";
    return this->ideas[index];
}

void Brain::setIdea(int index, std::string idea) {
    if (index < 0 || index > IDEA_COUNT)
        return;
    this->ideas[index] = idea;
}