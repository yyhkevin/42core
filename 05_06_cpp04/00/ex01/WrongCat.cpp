#include "WrongCat.hpp"

WrongCat::WrongCat() {
    std::cout << "WrongCat default constructor called" << std::endl;
    this->_type = "WrongCat";
}

WrongCat::WrongCat(WrongCat const &a) {
    std::cout << "WrongCat copy constructor called" << std::endl;
    *this = a;
}

WrongCat & WrongCat::operator = (WrongCat const &a) {
    std::cout << "WrongCat copy assignment called" << std::endl;
    if (this != &a) {
        this->_type = a._type;
    }
    return *this;
}

WrongCat::~WrongCat() {
    std::cout << "WrongCat destructor called" << std::endl;
}

void WrongCat::makeSound() const {
    std::cout << "I am a human-language-speaking cat and you're dreaming" << std::endl;
}