#include "Animal.hpp"

Animal::Animal() : _type("Generic Animal") {
    std::cout << "Animal default constructor called" << std::endl;
}

Animal::Animal(Animal const &a) {
    std::cout << "Animal copy constructor called" << std::endl;
    *this = a;
}

Animal & Animal::operator = (Animal const &a) {
    std::cout << "Animal copy assignment called" << std::endl;
    if (this != &a) {
        this->_type = a._type;
    }
    return *this;
}

Animal::~Animal() {
    std::cout << "Animal destructor called" << std::endl;
}

void Animal::makeSound() const {
    std::cout << "Wait what animal am I...?" << std::endl;
}

std::string Animal::getType() const {
    return this->_type;
}
