#pragma once

#include "Animal.hpp"
#include <iostream>

class Cat : public Animal {
    public:
        Cat();
        Cat(Cat const &a);
        Cat & operator = (Cat const &a);
        virtual ~Cat();
        void makeSound() const;
};