#pragma once

#include "Animal.hpp"
#include <iostream>

class Dog : public Animal {
    public:
        Dog();
        Dog(Dog const &a);
        Dog & operator = (Dog const &a);
        virtual ~Dog();
        void makeSound() const;
};