#include "WrongAnimal.hpp"

WrongAnimal::WrongAnimal() : _type("Generic non-Animal") {
    std::cout << "WrongAnimal default constructor called" << std::endl;
}

WrongAnimal::WrongAnimal(WrongAnimal const &a) {
    std::cout << "WrongAnimal copy constructor called" << std::endl;
    *this = a;
}

WrongAnimal & WrongAnimal::operator = (WrongAnimal const &a) {
    std::cout << "WrongAnimal copy assignment called" << std::endl;
    if (this != &a) {
        this->_type = a._type;
    }
    return *this;
}

WrongAnimal::~WrongAnimal() {
    std::cout << "WrongAnimal destructor called" << std::endl;
}

void WrongAnimal::makeSound() const {
    std::cout << "A bug in the universe. I shouldn't exist....." << std::endl;
}

std::string WrongAnimal::getType() const {
    return this->_type;
}
