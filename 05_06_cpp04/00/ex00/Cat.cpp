#include "Cat.hpp"

Cat::Cat() {
    std::cout << "Cat default constructor called" << std::endl;
    this->_type = "Cat";
}

Cat::Cat(Cat const &a) {
    std::cout << "Cat copy constructor called" << std::endl;
    *this = a;
}

Cat & Cat::operator = (Cat const &a) {
    std::cout << "Cat copy assignment called" << std::endl;
    if (this != &a) {
        this->_type = a._type;
    }
    return *this;
}

Cat::~Cat() {
    std::cout << "Cat destructor called" << std::endl;
}

void Cat::makeSound() const {
    std::cout << "meeeeeeeeow~" << std::endl;
}