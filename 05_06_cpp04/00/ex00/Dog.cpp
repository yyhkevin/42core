#include "Dog.hpp"

Dog::Dog() {
    std::cout << "Dog default constructor called" << std::endl;
    this->_type = "Dog";
}

Dog::Dog(Dog const &a) {
    std::cout << "Dog copy constructor called" << std::endl;
    *this = a;
}

Dog & Dog::operator = (Dog const &a) {
    std::cout << "Dog copy assignment called" << std::endl;
    if (this != &a) {
        this->_type = a._type;
    }
    return *this;
}

Dog::~Dog() {
    std::cout << "Dog destructor called" << std::endl;
}

void Dog::makeSound() const {
    std::cout << "Wooooof!" << std::endl;
}