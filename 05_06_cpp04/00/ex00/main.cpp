#include "Animal.hpp"
#include "Dog.hpp"
#include "Cat.hpp"
#include "WrongAnimal.hpp"
#include "WrongCat.hpp"

int main(void) {
    {
        const Animal* meta = new Animal();
        const Animal* j = new Dog();
        const Animal* i = new Cat();
        std::cout << j->getType() << " " << std::endl;
        std::cout << i->getType() << " " << std::endl;
        i->makeSound(); //will output the cat sound!
        j->makeSound();
        meta->makeSound();
        delete meta;
        delete i;
        delete j;
    }
    std::cout << "---------" << std::endl;
    {
        Animal a;
        Animal a_cp(a);
        Animal a_assign = a_cp;

        std::cout << a.getType() << std::endl;
        std::cout << a_cp.getType() << std::endl;
        std::cout << a_assign.getType() << std::endl;

        Dog d;
        Dog d_cp(d);
        Dog d_assign = d_cp;

        std::cout << d.getType() << std::endl;
        std::cout << d_cp.getType() << std::endl;
        std::cout << d_assign.getType() << std::endl;

        Cat c;
        Cat c_cp(c);
        Cat c_assign = c_cp;

        std::cout << c.getType() << std::endl;
        std::cout << c_cp.getType() << std::endl;
        std::cout << c_assign.getType() << std::endl;
    }
    std::cout << "---------" << std::endl;
    // The following example does not work because of "slicing", where we are essentially "typecasting" all objects to Animal
    // Thus we lose information about what kind of subclass it is
    // {
    //     Animal generic;
    //     Dog good_doggo;
    //     Cat whimsical_kittie;
    //     Animal animals[3] = {
    //         generic,
    //         good_doggo,
    //         whimsical_kittie
    //     };
    //     for (int i = 0; i < 3; i++) {
    //         std::cout << animals[i].getType() << " makes this sound: ";
    //         animals[i].makeSound();
    //     }
    // }
    {
        Animal generic;
        Dog good_doggo;
        Cat whimsical_kittie;
        Animal *animals[3] = {
            &generic,
            &good_doggo,
            &whimsical_kittie
        };
        for (int i = 0; i < 3; i++) {
            std::cout << animals[i]->getType() << " makes this sound: ";
            animals[i]->makeSound();
        }
    }
    std::cout << "---------" << std::endl;
    {
        const WrongAnimal* meta = new WrongAnimal();
        const WrongCat* j = new WrongCat();
        std::cout << "WrongCat as WrongCat: " << j->getType() << " " << std::endl;
        std::cout << "WrongAnimal as WrongAnimal: " << meta->getType() << " " << std::endl;
        std::cout << "WrongCat as WrongCat: ";
        j->makeSound();
        std::cout << "WrongAnimal as WrongAnimal: ";
        meta->makeSound();

        const WrongAnimal* catAsAnimal = j;
        std::cout << "WrongCat as WrongAnimal: " << catAsAnimal->getType() << " " << std::endl;
        std::cout << "WrongCat as WrongAnimal: ";
        catAsAnimal->makeSound();

        delete meta;
        delete j;
    }
    return 0;
}