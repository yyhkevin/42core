/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fork.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/09/06 19:33:12 by keyu              #+#    #+#             */
/*   Updated: 2024/09/06 19:34:18 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

int	get_left_fork(int philo_index)
{
	return (philo_index - 1);
}

int	get_right_fork(int philo_index, int n_philo)
{
	if (philo_index == 1)
		return (n_philo - 1);
	return (philo_index - 2);
}

static void	get_lock(t_philo *philo, int is_after)
{
	if (philo->index % 2 == is_after)
		pthread_mutex_lock(&(philo->settings->forks
			[get_left_fork(philo->index)]));
	else
		pthread_mutex_lock(&(philo->settings->forks
			[get_right_fork(philo->index, philo->settings->n_philo)]));
}

void	get_before_lock(t_philo *philo)
{
	get_lock(philo, 0);
}

void	get_after_lock(t_philo *philo)
{
	get_lock(philo, 1);
}
