/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checks.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/09/04 19:15:09 by keyu              #+#    #+#             */
/*   Updated: 2024/09/04 19:17:14 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

// died of starvation
static int	philo_is_dead(t_philo *philo)
{
	int	dead;

	pthread_mutex_lock(&(philo->settings->meal_lock));
	dead = (ft_now() >= philo->last_eaten + philo->settings->t_die)
		&& !philo->is_eating;
	pthread_mutex_unlock(&(philo->settings->meal_lock));
	return (dead);
}

int	all_philos_alive(t_philo **philos, int n_philo)
{
	while (n_philo > 0)
	{
		n_philo--;
		if (philo_is_dead(philos[n_philo]))
		{
			ft_log(philos[n_philo], DIED);
			pthread_mutex_lock(&(philos[n_philo]->settings->ended_lock));
			philos[n_philo]->settings->has_ended = 1;
			pthread_mutex_unlock(&(philos[n_philo]->settings->ended_lock));
			return (0);
		}
	}
	return (1);
}

int	all_philos_eaten(t_philo **philos, int n_philo, int n_eat)
{
	if (n_eat == -1)
		return (0);
	pthread_mutex_lock(&((*philos)->settings->meal_lock));
	while (n_philo > 0)
	{
		n_philo--;
		if (philos[n_philo]->eaten_count < n_eat)
			return (pthread_mutex_unlock(&((*philos)->settings->meal_lock)), 0);
	}
	pthread_mutex_unlock(&((*philos)->settings->meal_lock));
	pthread_mutex_lock(&(philos[n_philo]->settings->ended_lock));
	philos[n_philo]->settings->has_ended = 1;
	pthread_mutex_unlock(&(philos[n_philo]->settings->ended_lock));
	return (1);
}

int	sim_running(t_philo *philo)
{
	pthread_mutex_lock(&(philo->settings->ended_lock));
	if (philo->settings->has_ended)
	{
		pthread_mutex_unlock(&(philo->settings->ended_lock));
		return (0);
	}
	pthread_mutex_unlock(&(philo->settings->ended_lock));
	return (1);
}
