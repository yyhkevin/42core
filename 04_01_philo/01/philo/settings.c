/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   settings.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/09/04 19:33:58 by keyu              #+#    #+#             */
/*   Updated: 2024/09/04 19:34:56 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void	parse_settings(t_settings *settings, int argc, char **argv)
{
	settings->forks = 0;
	settings->has_ended = 0;
	argv++;
	settings->n_philo = ft_atoi(*argv++);
	settings->t_die = ft_atoi(*argv++);
	settings->t_eat = ft_atoi(*argv++);
	settings->t_sleep = ft_atoi(*argv++);
	settings->n_eat = -1;
	if (argc == 6)
		settings->n_eat = ft_atoi(*argv++);
}

int	ft_check_valid_settings(t_settings *s)
{
	return (s->n_philo >= 0 && s->t_die >= 0 && s->t_eat >= 0
		&& s->t_sleep >= 0 && s->n_eat >= -1);
}

int	obtain_locks(t_settings *settings)
{
	int	i;

	if (pthread_mutex_init(&(settings->write_lock), NULL) != 0)
		return (0);
	if (pthread_mutex_init(&(settings->ended_lock), NULL) != 0)
		return (pthread_mutex_destroy(&(settings->write_lock)), 0);
	if (pthread_mutex_init(&(settings->meal_lock), NULL) != 0)
	{
		pthread_mutex_destroy(&(settings->write_lock));
		pthread_mutex_destroy(&(settings->ended_lock));
		return (0);
	}
	settings->forks = malloc(sizeof(pthread_mutex_t) * settings->n_philo);
	if (!settings->forks)
		return (kill_locks(settings, 0), 0);
	i = 0;
	while (i < settings->n_philo)
	{
		if (pthread_mutex_init((settings->forks) + i, NULL) != 0)
			return (kill_locks(settings, i), 0);
		i++;
	}
	return (1);
}
