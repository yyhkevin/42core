/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_philo.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/09/04 22:06:55 by keyu              #+#    #+#             */
/*   Updated: 2024/09/04 22:06:57 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PHILO_H
# define FT_PHILO_H

// write, usleep
# include <unistd.h>

// malloc, free
# include <stdlib.h>

// printf
# include <stdio.h>

// threads
# include <pthread.h>

// gettimeofday
# include <sys/time.h>

# define TAKEFORK 0
# define EATING	1
# define SLEEPING 2
# define THINKING 3
# define DIED 4

// n_eat -1 means no value given
typedef struct s_settings
{
	int				n_philo;
	int				t_die;
	int				t_eat;
	int				t_sleep;
	int				n_eat;
	int				has_ended;
	pthread_mutex_t	write_lock;
	pthread_mutex_t	ended_lock;
	pthread_mutex_t	meal_lock;
	pthread_mutex_t	*forks;
}	t_settings;

typedef struct s_philo
{
	int				index;
	int				eaten_count;
	pthread_t		thread;
	t_settings		*settings;
	long			start_time;
	long			last_eaten;
	int				is_eating;
}	t_philo;

// checks.c
int		all_philos_alive(t_philo **philos, int n_philo);
int		all_philos_eaten(t_philo **philos, int n_philo, int n_eat);
int		sim_running(t_philo *philo);

// fork.c
int		get_left_fork(int philo_index);
int		get_right_fork(int philo_index, int n_philo);
void	get_before_lock(t_philo *philo);
void	get_after_lock(t_philo *philo);

// ft_strings.c
void	ft_putstr(char *s);
void	ft_putusage(void);
void	ft_putnbr(int i);
void	ft_log(t_philo *philo, int mode);

// ft_utils.c
int		ft_atoi(char *s);

// philos.c
void	come_alive(t_philo **philos, t_settings *settings);
t_philo	**create_philos(t_settings *settings);
void	join_philos(t_philo **philos, int count);

// philos_kill.c
void	kill_philos(t_philo **philos, int count);
void	kill_locks(t_settings *settings, int n_forks);

// settings.c
void	parse_settings(t_settings *settings, int argc, char **argv);
int		ft_check_valid_settings(t_settings *s);
int		obtain_locks(t_settings *settings);

// debug_prints.c
//void	p_settings(t_settings settings);
//void	p_philos(t_philo **philos, int count);

// utils.c
long	ft_now(void);
void	ft_sleep(int msec);

// routine.c
void	think(t_philo *philo);
void	gosleep(t_philo *philo);
void	eat(t_philo *philo);

#endif
