/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strings.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/16 11:01:39 by keyu              #+#    #+#             */
/*   Updated: 2024/09/04 19:19:09 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void	ft_putstr(char *s)
{
	while (*s)
		write(1, s++, 1);
}

void	ft_putusage(void)
{
	char	*s;

	s = "Usage: philo number_of_philosophers time_to_die time_to_eat ";
	ft_putstr(s);
	s = "time_to_sleep [number_of_times_each_philosopher_must_eat]\n";
	ft_putstr(s);
}

void	ft_putnbr(int i)
{
	if (i >= 10)
		ft_putnbr(i / 10);
	i %= 10;
	write(1, &"0123456789"[i], 1);
}

// A check for has_ended is required as threads
// are not killed upon simulation end
// The check is within the mutex as it is possible
// some thread is already holding the lock
void	ft_log(t_philo *philo, int mode)
{
	pthread_mutex_lock(&(philo->settings->write_lock));
	if (!sim_running(philo))
	{
		pthread_mutex_unlock(&(philo->settings->write_lock));
		return ;
	}
	ft_putnbr(ft_now() - philo->start_time);
	ft_putstr(" ");
	ft_putnbr(philo->index);
	ft_putstr(" ");
	if (mode == TAKEFORK)
		ft_putstr("has taken a fork\n");
	else if (mode == EATING)
		ft_putstr("is eating\n");
	else if (mode == SLEEPING)
		ft_putstr("is sleeping\n");
	else if (mode == THINKING)
		ft_putstr("is thinking\n");
	else if (mode == DIED)
		ft_putstr("died\n");
	pthread_mutex_unlock(&(philo->settings->write_lock));
}
