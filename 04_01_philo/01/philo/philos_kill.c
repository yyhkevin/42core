/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philos_kill.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/09/04 19:32:59 by keyu              #+#    #+#             */
/*   Updated: 2024/09/04 19:33:00 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void	kill_philos(t_philo **philos, int count)
{
	while (count > 0)
	{
		free(philos[--count]);
	}
	if (philos)
		free(philos);
}

void	kill_locks(t_settings *settings, int n_forks)
{
	int	i;

	pthread_mutex_destroy(&(settings->write_lock));
	pthread_mutex_destroy(&(settings->ended_lock));
	pthread_mutex_destroy(&(settings->meal_lock));
	i = 0;
	while (i < n_forks)
	{
		pthread_mutex_destroy((settings->forks) + i);
		i++;
	}
	if (settings->forks)
		free(settings->forks);
}
