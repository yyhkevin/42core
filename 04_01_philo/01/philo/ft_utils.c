/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/16 11:53:09 by keyu              #+#    #+#             */
/*   Updated: 2024/09/05 19:59:15 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

// modified atoi
// negative numbers will be treated as issues
int	ft_atoi(char *s)
{
	long long	i;
	int			len;

	i = 0;
	len = 0;
	while (*s == 32 || (*s >= 9 && *s <= 13))
		s++;
	if (*s == '+' || *s == '-')
	{
		if (*s == '-')
			return (-1);
		s++;
	}
	while (*s >= '0' && *s <= '9')
	{
		i = i * 10 + *s++ - '0';
		len++;
	}
	if (i > 2147483647 || len > 10)
		return (-1);
	return (i);
}
