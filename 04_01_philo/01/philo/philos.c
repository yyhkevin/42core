/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philos.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/09/04 19:22:40 by keyu              #+#    #+#             */
/*   Updated: 2024/09/04 19:32:49 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void	*routine(void *input)
{
	t_philo		*philo;

	philo = (t_philo *) input;
	if (philo->index % 2 == 0)
		ft_sleep(5);
	while (sim_running(philo))
	{
		eat(philo);
		gosleep(philo);
		think(philo);
	}
	return (0);
}

void	come_alive(t_philo **philos, t_settings *settings)
{
	int	i;

	i = 0;
	while (i < settings->n_philo)
	{
		if (pthread_create(&(philos[i]->thread), NULL,
				&routine, (void *) philos[i]) != 0)
		{
			kill_philos(philos, i);
			return ;
		}
		i++;
	}
}

static void	init_philo(t_philo **philos, int i, t_settings *settings)
{
	philos[i]->index = i + 1;
	philos[i]->eaten_count = 0;
	philos[i]->settings = settings;
	philos[i]->start_time = ft_now();
	philos[i]->last_eaten = philos[i]->start_time;
	philos[i]->is_eating = 0;
}

static void	just_init(t_philo ***p_philo, int *i)
{
	*p_philo = 0;
	*i = 0;
}

t_philo	**create_philos(t_settings *settings)
{
	t_philo	**philos;
	int		i;

	just_init(&philos, &i);
	if (settings->n_philo > 0)
	{
		philos = malloc(sizeof(t_philo *) * settings->n_philo);
		if (!philos)
		{
			kill_locks(settings, settings->n_philo);
			return (0);
		}
	}
	while (i < settings->n_philo)
	{
		philos[i] = malloc(sizeof(t_philo));
		if (!philos[i])
		{
			kill_locks(settings, settings->n_philo);
			kill_philos(philos, i);
			return (0);
		}
		init_philo(philos, i++, settings);
	}
	return (philos);
}
