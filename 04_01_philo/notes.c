#include <pthread.h>

// concurrent - multi thread processing
// parallel - simultaneously

// must return void pointer when passed in pthread
void	*computation(void *v)
{
	pthread_mutex_lock(&mutex);
	// do something
	// ...
	pthread_mutex_unlock(&mutex);
}

int	main()
{
	pthread_t	thread1;
	int			value;
	pthread_mutex_t	mutex;

	pthread_mutex_init(&mutex, 0);

	value = 1;
	// will actually start the thread and run
	// returns int 0 if success, otherwise failed
	//          *thread  attr  void*(fun)  args to fun via void*
	pthread_create(&thread1, 0, computation, (void *) &value);

	// will await until thread is done
	// without this, the program might exit before thread is done processing
	pthread_join(thread1, 0);

	// technically, if the main has a compute intensive enough function, it will take longer than the computation function
	// however, this will not be safe - race condition

	pthread_mutex_destroy(&mutex);
}
