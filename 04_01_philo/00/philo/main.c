/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/09/04 19:22:48 by keyu              #+#    #+#             */
/*   Updated: 2024/09/04 19:30:13 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void	observer(t_philo **philos)
{
	t_settings	*settings;

	settings = (*philos)->settings;
	while (1)
	{
		if (!(all_philos_alive(philos, settings->n_philo))
			|| all_philos_eaten(philos, settings->n_philo, settings->n_eat))
		{
			return ;
		}
	}
}

void	join_philos(t_philo **philos, int count)
{
	int	i;

	i = 0;
	while (i < count)
	{
		pthread_join(philos[i]->thread, NULL);
		i++;
	}
}

void	start_sim(t_philo **philos, t_settings *settings)
{
	come_alive(philos, settings);
	observer(philos);
	join_philos(philos, settings->n_philo);
	kill_locks(settings);
	kill_philos(philos, settings->n_philo);
}

int	main(int argc, char **argv)
{
	t_settings	settings;
	t_philo		**philos;

	if (argc != 5 && argc != 6)
	{
		ft_putusage();
		return (0);
	}
	parse_settings(&settings, argc, argv);
	if (!ft_check_valid_settings(&settings))
	{
		ft_putusage();
		return (0);
	}
	if (settings.n_philo == 0)
		return (0);
	obtain_locks(&settings);
	philos = create_philos(&settings);
	if (!philos)
	{
		ft_putstr("Failed to allocate memory for philos\n");
		return (0);
	}
	start_sim(philos, &settings);
	return (0);
}
