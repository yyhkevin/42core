/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   settings.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/09/04 19:33:58 by keyu              #+#    #+#             */
/*   Updated: 2024/09/04 19:34:56 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void	parse_settings(t_settings *settings, int argc, char **argv)
{
	settings->has_ended = 0;
	argv++;
	settings->n_philo = ft_atoi(*argv++);
	settings->t_die = ft_atoi(*argv++);
	settings->t_eat = ft_atoi(*argv++);
	settings->t_sleep = ft_atoi(*argv++);
	settings->n_eat = -1;
	if (argc == 6)
		settings->n_eat = ft_atoi(*argv++);
}

int	ft_check_valid_settings(t_settings *s)
{
	return (s->n_philo >= 0 && s->t_die >= 0 && s->t_eat >= 0
		&& s->t_sleep >= 0 && s->n_eat >= -1);
}

void	obtain_locks(t_settings *settings)
{
	int	i;

	pthread_mutex_init(&(settings->write_lock), NULL);
	pthread_mutex_init(&(settings->ended_lock), NULL);
	pthread_mutex_init(&(settings->meal_lock), NULL);
	settings->forks = malloc(sizeof(pthread_mutex_t) * settings->n_philo);
	i = 0;
	while (i < settings->n_philo)
	{
		pthread_mutex_init((settings->forks) + i, NULL);
		i++;
	}
}
