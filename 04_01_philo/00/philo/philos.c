/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philos.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/09/04 19:22:40 by keyu              #+#    #+#             */
/*   Updated: 2024/09/04 19:32:49 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

int	can_eat(t_philo *philo)
{
	t_settings	*settings;

	settings = philo->settings;
	return (1);
}

void	*routine(void *input)
{
	t_philo		*philo;

	philo = (t_philo *) input;
	if (philo->index % 2 == 1)
		ft_sleep(5);
	while (sim_running(philo))
	{
		eat(philo);
		gosleep(philo);
		think(philo);
	}
	return (0);
}

void	come_alive(t_philo **philos, t_settings *settings)
{
	int	i;

	i = 0;
	while (i < settings->n_philo)
	{
		if (pthread_create(&(philos[i]->thread), NULL,
				&routine, (void *) philos[i]) != 0)
		{
			kill_philos(philos, i);
			return ;
		}
		i++;
	}
}

static void	init_philo(t_philo *philo, int i, t_settings *settings)
{
	philo->index = i + 1;
	philo->eaten_count = 0;
	philo->settings = settings;
	philo->start_time = ft_now();
	philo->last_eaten = philo->start_time;
	philo->is_eating = 0;
}

t_philo	**create_philos(t_settings *settings)
{
	t_philo	**philos;
	int		i;

	philos = 0;
	i = 0;
	if (settings->n_philo > 0)
	{
		philos = malloc(sizeof(t_philo *) * settings->n_philo);
		if (!philos)
			return (0);
	}
	while (i < settings->n_philo)
	{
		philos[i] = malloc(sizeof(t_philo));
		if (!philos[i])
		{
			kill_philos(philos, i);
			return (0);
		}
		init_philo(philos[i], i, settings);
		i++;
	}
	return (philos);
}
