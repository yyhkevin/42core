/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   routine.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/09/04 19:25:16 by keyu              #+#    #+#             */
/*   Updated: 2024/09/04 19:36:54 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void	think(t_philo *philo)
{
	ft_log(philo, THINKING);
}

void	gosleep(t_philo *philo)
{
	ft_log(philo, SLEEPING);
	ft_sleep(philo->settings->t_sleep);
}

static int	get_left_fork(int philo_index)
{
	return (philo_index - 1);
}

static int	get_right_fork(int philo_index, int n_philo)
{
	if (philo_index == 1)
		return (n_philo - 1);
	return (philo_index - 2);
}

void	eat(t_philo *philo)
{
	pthread_mutex_lock(&(philo->settings->forks[get_left_fork(philo->index)]));
	ft_log(philo, TAKEFORK);
	if (philo->settings->n_philo == 1)
	{
		pthread_mutex_unlock(&(philo->settings->forks
			[get_left_fork(philo->index)]));
		ft_sleep(philo->settings->t_die);
		return ;
	}
	pthread_mutex_lock(&(philo->settings->forks
		[get_right_fork(philo->index, philo->settings->n_philo)]));
	ft_log(philo, TAKEFORK);
	ft_log(philo, EATING);
	philo->is_eating = 1;
	pthread_mutex_lock(&(philo->settings->meal_lock));
	philo->last_eaten = ft_now();
	philo->eaten_count++;
	pthread_mutex_unlock(&(philo->settings->meal_lock));
	ft_sleep(philo->settings->t_eat);
	philo->is_eating = 0;
	pthread_mutex_unlock(&(philo->settings->forks
		[get_left_fork(philo->index)]));
	pthread_mutex_unlock(&(philo->settings->forks
		[get_right_fork(philo->index, philo->settings->n_philo)]));
}
