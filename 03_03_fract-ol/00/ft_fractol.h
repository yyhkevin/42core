/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractol.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/07 16:36:47 by keyu              #+#    #+#             */
/*   Updated: 2024/04/20 10:10:21 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_FRACTOL_H
# define FT_FRACTOL_H

// for perror
# include <stdio.h>

// for free, exit
# include <stdlib.h>

// fot write
# include <unistd.h>

# include <X11/keysym.h>
# include "mlx_linux/mlx.h"

# define WIN_H 450
# define WIN_W 450
# define WIN_NAME "Fract-ol"

# define LEFT -2.0
# define RIGHT 2.0
# define UP 2.0
# define DOWN -2.0

# define ITERATIONS 80

# define ZOOM_FACTOR 1.1
# define OFFSET_FACTOR 0.1

# define JULIA 0
# define MANDELBROT 1
# define BURNINGSHIP 2
# define JULIA_NAME "julia"
# define MANDELBROT_NAME "mandelbrot"
# define BURNINGSHIP_NAME "burningship"

# define MOUSE_ZOOM_IN 4
# define MOUSE_ZOOM_OUT 5

// bpp	: bits_per_pixel
// ll	: line_length
// e	: endian
typedef struct s_image_data {
	void	*img;
	char	*addr;
	int		bpp;
	int		ll;
	int		e;
}	t_image_data;

typedef struct s_complex {
	double	a;
	double	b;
}	t_complex;

typedef struct s_settings {
	int			set;
	t_complex	z;
	int			shift;
}	t_settings;

typedef struct s_bounds {
	double	left;
	double	right;
	double	up;
	double	down;
}	t_bounds;

typedef struct s_mlx_data {
	void			*con;
	void			*win;
	t_settings		*settings;
	t_image_data	*img;
	t_bounds		*bounds;
}	t_mlx_data;

// ft_strings.c
void		ft_putchar(char c);
void		ft_putusage(void);

// ft_utils.c
int			ft_streq(char *s1, char *s2);
double		ft_atof(char *s);

// ft_mem.c
int			ft_killall(t_mlx_data *data);

// ft_fractol.c
t_complex	ft_get_pixel_complex(int x, int y, t_bounds *bounds);
int			ft_color(int deg, int shift);
void		ft_init_and_run(t_settings *settings);

// ft_calc.c
t_complex	ft_square(t_complex z, int abs);
t_complex	ft_add(t_complex z1, t_complex z2);
void		ft_print_complex(t_complex z);
t_complex	ft_zero(void);
int			ft_degree_out_set(t_complex z, t_complex c, int abs);

// ft_hooks.c
void		ft_offset(t_mlx_data *data, int h, int v);
void		ft_zoom(int is_zoom_out, t_bounds *bounds, float x, float y);
int			ft_handle_mouse(int keycode, int x, int y, t_mlx_data *data);
int			ft_handle_input(int keysym, t_mlx_data *data);

// ft_paint.c
void		ft_put_pixel(t_image_data *img, int x, int y, int color);
void		ft_paint(t_mlx_data *data);

#endif
