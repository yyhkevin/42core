/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/13 08:48:15 by keyu              #+#    #+#             */
/*   Updated: 2024/04/13 09:15:11 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_streq(char *s1, char *s2)
{
	while (*s1 && *s2 && *s1 == *s2)
	{
		s1++;
		s2++;
	}
	return (*s1 == *s2);
}

double	ft_atof_decimal(char *s)
{
	double	d;
	double	div;

	d = 0.0;
	div = 0.1;
	if (*s == '.')
	{
		s++;
		while (*s >= '0' && *s <= '9')
		{
			d += (*s++ - '0') * div;
			div /= 10;
		}
	}
	return (d);
}

double	ft_atof(char *s)
{
	double	d;
	int		is_neg;

	d = 0.0;
	is_neg = 0;
	while (*s == 32 || (*s > 9 && *s < 13))
		s++;
	if (*s == '-' || *s == '+')
	{
		if (*s == '-')
			is_neg = 1;
		s++;
	}
	while (*s >= '0' && *s <= '9')
		d = d * 10 + *s++ - '0';
	d += ft_atof_decimal(s);
	if (is_neg)
		d *= -1;
	return (d);
}
