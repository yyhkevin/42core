/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strings.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/13 08:36:04 by keyu              #+#    #+#             */
/*   Updated: 2024/04/13 09:18:58 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putusage(void)
{
	char	*s;

	s = "Usage: fractol set <a> <b>\n";
	while (*s)
		ft_putchar(*s++);
	exit(1);
}
