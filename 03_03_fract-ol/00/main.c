/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/07 13:59:29 by keyu              #+#    #+#             */
/*   Updated: 2024/04/16 10:29:17 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"

int	ft_get_set(char *c)
{
	if (ft_streq(c, JULIA_NAME))
		return (JULIA);
	else if (ft_streq(c, MANDELBROT_NAME))
		return (MANDELBROT);
	else if (ft_streq(c, BURNINGSHIP_NAME))
		return (BURNINGSHIP);
	return (-1);
}

void	ft_settings(int argc, char **argv, t_settings *s)
{
	t_complex	z;

	s -> set = ft_get_set(argv[1]);
	if (s -> set == -1)
		ft_putusage();
	if (s -> set == JULIA && argc != 4)
		ft_putusage();
	if (s -> set == MANDELBROT && argc != 2)
		ft_putusage();
	if (s -> set == BURNINGSHIP && argc != 2)
		ft_putusage();
	if (s -> set == JULIA)
	{
		z.a = ft_atof(argv[2]);
		z.b = ft_atof(argv[3]);
		s -> z = z;
	}
	s -> shift = 0;
}

int	main(int argc, char **argv)
{
	t_settings	settings;

	if (argc < 2 || argc > 4)
		ft_putusage();
	ft_settings(argc, argv, &settings);
	ft_init_and_run(&settings);
	return (0);
}
