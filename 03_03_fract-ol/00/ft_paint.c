/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_paint.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/13 19:32:45 by keyu              #+#    #+#             */
/*   Updated: 2024/04/16 10:49:13 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"

void	ft_put_pixel(t_image_data *img, int x, int y, int color)
{
	char	*dst;

	dst = img->addr + (y * img->ll + x * (img->bpp / 8));
	*(unsigned int *)dst = color;
}

void	ft_paint(t_mlx_data *data)
{
	int			x;
	int			y;
	int			deg;
	t_complex	z;

	x = 0;
	while (x < WIN_W)
	{
		y = 0;
		while (y < WIN_H)
		{
			z = ft_get_pixel_complex(x, y, data -> bounds);
			if (data -> settings -> set == JULIA)
				deg = ft_degree_out_set(z, data -> settings -> z, 0);
			else if (data -> settings -> set == MANDELBROT)
				deg = ft_degree_out_set(ft_zero(), z, 0);
			else if (data -> settings -> set == BURNINGSHIP)
				deg = ft_degree_out_set(ft_zero(), z, 1);
			ft_put_pixel(data -> img, x, y,
				ft_color(deg, data -> settings -> shift));
			y++;
		}
		x++;
	}
	mlx_put_image_to_window(data->con, data->win, data -> img -> img, 0, 0);
}
