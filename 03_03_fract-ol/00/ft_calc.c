/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/13 10:04:14 by keyu              #+#    #+#             */
/*   Updated: 2024/04/13 19:20:47 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"

t_complex	ft_square(t_complex z, int abs)
{
	t_complex	res;

	if (abs && z.a < 0)
		z.a *= -1;
	if (abs && z.b < 0)
		z.b *= -1;
	res.a = z.a * z.a - z.b * z.b;
	res.b = 2 * z.a * z.b;
	return (res);
}

t_complex	ft_add(t_complex z1, t_complex z2)
{
	t_complex	res;

	res.a = z1.a + z2.a;
	res.b = z1.b + z2.b;
	return (res);
}

void	ft_print_complex(t_complex z)
{
	printf("%f + %fi\n", z.a, z.b);
}

t_complex	ft_zero(void)
{
	t_complex	res;

	res.a = 0;
	res.b = 0;
	return (res);
}

int	ft_degree_out_set(t_complex z, t_complex c, int abs)
{
	int			i;
	t_complex	res;

	i = ITERATIONS;
	res = ft_square(z, abs);
	res = ft_add(res, c);
	while (i > 0)
	{
		if (res.a > 2 || res.b > 2 || res.a < -2 || res.b < -2)
			return (i);
		res = ft_square(res, abs);
		res = ft_add(res, c);
		i--;
	}
	return (0);
}
