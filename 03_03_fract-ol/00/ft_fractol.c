/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractol.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/13 09:22:45 by keyu              #+#    #+#             */
/*   Updated: 2024/04/16 10:28:34 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"

t_bounds	ft_initial_bounds(void)
{
	t_bounds	bounds;

	bounds.left = LEFT;
	bounds.right = RIGHT;
	bounds.up = UP;
	bounds.down = DOWN;
	return (bounds);
}

t_complex	ft_get_pixel_complex(int x, int y, t_bounds *bounds)
{
	t_complex	z;

	z.a = bounds -> left + (bounds -> right - bounds -> left) / WIN_W * x;
	z.b = bounds -> up - (bounds -> up - bounds -> down) / WIN_H * y;
	return (z);
}

int	ft_color(int deg, int s)
{
	int	c;

	c = 0x0E344E;
	c = (c & 0x0000FF) << s | (c & 0x00FF00) << s | (c & 0xFF0000) << s;
	while (deg)
	{
		c = (c & 0x0000FF) << 1 | (c & 0x00FF00) << 1 | (c & 0xFF0000) << 1;
		deg--;
	}
	return (c);
}

void	ft_init_win(t_mlx_data *data)
{
	data -> con = mlx_init();
	if (!(data -> con))
	{
		perror("Failed to get mlx_init");
		exit(1);
	}
	data -> win = mlx_new_window(data -> con, WIN_W, WIN_H, WIN_NAME);
	if (!(data -> win))
	{
		perror("Failed to get mlx_win");
		ft_killall(data);
	}
}

void	ft_init_and_run(t_settings *s)
{
	t_mlx_data		data;
	t_bounds		bounds;
	t_image_data	img;

	ft_init_win(&data);
	img.img = mlx_new_image(data.con, WIN_W, WIN_H);
	img.addr = mlx_get_data_addr(img.img, &img.bpp, &img.ll, &img.e);
	data.img = &img;
	data.settings = s;
	bounds = ft_initial_bounds();
	data.bounds = &bounds;
	mlx_hook(data.win, 17, 0, ft_killall, &data);
	mlx_key_hook(data.win, ft_handle_input, &data);
	mlx_mouse_hook(data.win, ft_handle_mouse, &data);
	ft_paint(&data);
	mlx_loop(data.con);
	ft_killall(&data);
}
