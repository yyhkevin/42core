/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mem.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/13 09:22:06 by keyu              #+#    #+#             */
/*   Updated: 2024/04/13 19:13:02 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"

int	ft_killall(t_mlx_data *data)
{
	if (data->img->img)
		mlx_destroy_image(data->con, data->img->img);
	if (data->win)
		mlx_destroy_window(data->con, data->win);
	mlx_loop_end(data->con);
	mlx_destroy_display(data->con);
	free(data->con);
	exit(0);
}
