/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hooks.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/13 19:27:19 by keyu              #+#    #+#             */
/*   Updated: 2024/04/16 10:31:18 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"

void	ft_offset(t_mlx_data *data, int h, int v)
{
	t_settings	*s;
	t_bounds	*b;
	double		diff;

	s = data -> settings;
	b = data -> bounds;
	if (h == 1 || h == -1)
	{
		diff = (b -> right - b -> left) * OFFSET_FACTOR;
		if (h == -1)
			diff *= -1;
		b -> left += diff;
		b -> right += diff;
	}
	else if (v == 1 || v == -1)
	{
		diff = (b -> up - b -> down) * OFFSET_FACTOR;
		if (v == -1)
			diff *= -1;
		b -> up += diff;
		b -> down += diff;
	}
}

void	ft_zoom(int is_zoom_out, t_bounds *bounds, float x, float y)
{
	double	diff;
	double	center;
	float	zoom;

	zoom = ZOOM_FACTOR;
	if (!is_zoom_out)
		zoom = 1 / zoom;
	diff = bounds -> right - bounds -> left;
	center = bounds -> left + diff * x;
	bounds -> left = center - diff / 2 * zoom;
	bounds -> right = center + diff / 2 * zoom;
	diff = bounds -> up - bounds -> down;
	center = bounds -> up - diff * y;
	bounds -> up = center + diff / 2 * zoom;
	bounds -> down = center - diff / 2 * zoom;
}

int	ft_handle_mouse(int keycode, int x, int y, t_mlx_data *data)
{
	if (keycode == MOUSE_ZOOM_IN)
		ft_zoom(0, data -> bounds, (float) x / WIN_W, (float) y / WIN_H);
	else if (keycode == MOUSE_ZOOM_OUT)
		ft_zoom(1, data -> bounds, (float) x / WIN_W, (float) y / WIN_H);
	ft_paint(data);
	return (0);
}

// could add in these to change between sets
//	else if (keysym == XK_j)
//	{
//		data -> settings -> set = JULIA;
//		data -> settings -> z.a = -0.5;
//		data -> settings -> z.b = 0.5;
//	}
//	else if (keysym == XK_m)
//		data -> settings -> set = MANDELBROT;

int	ft_handle_input(int keysym, t_mlx_data *data)
{
	if (keysym == XK_Escape)
		ft_killall(data);
	else if (keysym == XK_r)
	{
		data -> bounds -> left = LEFT;
		data -> bounds -> right = RIGHT;
		data -> bounds -> up = UP;
		data -> bounds -> down = DOWN;
		data -> settings -> shift = 0;
	}
	else if (keysym == XK_Left)
		ft_offset(data, -1, 0);
	else if (keysym == XK_Right)
		ft_offset(data, 1, 0);
	else if (keysym == XK_Down)
		ft_offset(data, 0, -1);
	else if (keysym == XK_Up)
		ft_offset(data, 0, 1);
	else if (keysym == XK_s)
		data -> settings -> shift++;
	ft_paint(data);
	return (0);
}
