/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_reverse.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 15:16:58 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 15:16:59 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

static	void	ft_reverse(t_list **lst)
{
	t_list	*first;
	t_list	*last;
	t_list	*curr;
	t_list	*next;

	if (ft_lstsize(*lst) <= 1)
		return ;
	first = *lst;
	last = ft_lstlast(first);
	curr = first;
	next = curr -> next;
	while (next != last)
	{
		curr = next;
		next = curr -> next;
	}
	*lst = last;
	last -> next = first;
	curr -> next = 0;
}

int	ft_reverse_rra(t_list **lst, int to_write)
{
	ft_reverse(lst);
	if (to_write)
		ft_write_str("rra\n");
	return (1);
}

int	ft_reverse_rrb(t_list **lst, int to_write)
{
	ft_reverse(lst);
	if (to_write)
		ft_write_str("rrb\n");
	return (1);
}

void	ft_reverse_rrr(t_list **lst1, t_list **lst2)
{
	ft_reverse(lst1);
	ft_reverse(lst2);
	ft_write_str("rrr\n");
}
