/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setup3.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 15:34:32 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 18:10:19 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

int	ft_setup_case(int cmp, t_list **stack_b)
{
	int	max;
	int	min;

	max = ft_get_minmax(*stack_b, 1);
	min = ft_get_minmax(*stack_b, 0);
	if (min < cmp && cmp < max)
		return (ft_count_case_between(cmp, stack_b));
	else
		return (ft_count_case_edge(stack_b));
}
