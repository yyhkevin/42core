/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setup2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 15:34:32 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 18:10:19 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

t_list	*ft_get_node(t_list **stack, int val)
{
	t_list	*node;

	node = *stack;
	while (*(int *)node -> content != val)
		node = node -> next;
	return (node);
}

void	ft_return_state(int count, t_list **stack)
{
	if (count > 0)
	{
		while (count)
		{
			ft_reverse_rrb(stack, 0);
			count--;
		}
	}
	else if (count < 0)
	{
		while (count != 0)
		{
			ft_rotate_rb(stack, 0);
			count++;
		}
	}
}

int	ft_count_case_between(int cmp, t_list **stack_b)
{
	int		count;
	t_list	*node1;
	t_list	*node2;
	int		size;

	size = ft_lstsize(*stack_b);
	count = 0;
	node1 = *stack_b;
	node2 = node1 -> next;
	while (cmp > *(int *)node1 -> content || cmp < *(int *)node2 -> content)
	{
		ft_rotate_rb(stack_b, 0);
		node1 = node2;
		node2 = node1 -> next;
		count++;
	}
	ft_rotate_rb(stack_b, 0);
	count++;
	if (count > size / 2)
		count -= size;
	ft_return_state(count, stack_b);
	return (count);
}

int	ft_cmp_maxmin(int mode, int num1, int num2)
{
	if (mode == 0)
	{
		if (num1 < num2)
			return (num1);
		else
			return (num2);
	}
	else
	{
		if (num1 > num2)
			return (num1);
		else
			return (num2);
	}
}

int	ft_count_case_edge(t_list **stack)
{
	int	count;
	int	tmp;

	count = 0;
	while (!ft_is_sorted(stack, 1))
	{
		ft_rotate_rb(stack, 0);
		count++;
	}
	tmp = count;
	while (tmp)
	{
		ft_reverse_rrb(stack, 0);
		tmp--;
	}
	if (count > ft_lstsize(*stack) / 2)
		return (count - ft_lstsize(*stack));
	return (count);
}
