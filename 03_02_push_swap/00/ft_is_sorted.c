/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_sorted.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 16:31:28 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 17:23:30 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

// mode0 means small to big, mode1 means big to small
int	ft_is_sorted(t_list **lst, int mode)
{
	t_list	*p;
	int		prev;
	int		curr;

	p = *lst;
	prev = *(int *)p -> content;
	p = p -> next;
	while (p)
	{
		curr = *(int *)p -> content;
		if (mode == 0 && curr < prev)
			return (0);
		if (mode == 1 && curr > prev)
			return (0);
		prev = curr;
		p = p -> next;
	}
	return (1);
}
