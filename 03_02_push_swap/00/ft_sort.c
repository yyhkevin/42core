/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 15:17:11 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 17:26:32 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

static	void	ft_end(t_list **s_a, t_list **s_b, char **parsed, int is_malloc)
{
	ft_lstclear(s_b, free);
	free(s_b);
	ft_killall(s_a, parsed, is_malloc, 0);
	exit(0);
}

void	ft_sort(char **parsed, int is_malloc)
{
	t_list	**stack_a;
	t_list	**stack_b;
	int		size;

	stack_a = ft_to_atol_list(parsed, is_malloc);
	stack_b = malloc(sizeof(t_list **));
	size = ft_lstsize(*stack_a);
	if (size == 2)
		ft_sort2(stack_a);
	else if (size == 3)
		ft_sort3(stack_a);
	else
		ft_sortn(stack_a, stack_b);
	ft_end(stack_a, stack_b, parsed, is_malloc);
}
