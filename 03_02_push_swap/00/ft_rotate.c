/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotate.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 15:17:03 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 15:17:05 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

static	void	ft_rotate(t_list **lst)
{
	t_list	*first;
	t_list	*last;

	if (ft_lstsize(*lst) <= 1)
		return ;
	first = *lst;
	last = ft_lstlast(first);
	*lst = first -> next;
	last -> next = first;
	first -> next = 0;
}

int	ft_rotate_ra(t_list **lst, int to_write)
{
	ft_rotate(lst);
	if (to_write)
		ft_write_str("ra\n");
	return (1);
}

int	ft_rotate_rb(t_list **lst, int to_write)
{
	ft_rotate(lst);
	if (to_write)
		ft_write_str("rb\n");
	return (1);
}

void	ft_rotate_rr(t_list **lst1, t_list **lst2)
{
	ft_rotate(lst1);
	ft_rotate(lst2);
	ft_write_str("rr\n");
}
