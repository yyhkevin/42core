/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/06 17:23:31 by keyu              #+#    #+#             */
/*   Updated: 2023/09/06 17:28:38 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

static	int	count_len(char const *s, char c)
{
	int	len;
	int	marked;

	len = 0;
	while (*s)
	{
		marked = 0;
		while (*s && *s != c)
		{
			if (!marked)
				len++;
			marked = 1;
			s++;
		}
		if (*s)
			s++;
	}
	return (len);
}

static	char	*dup_into(char const *s, int prev_i, int i)
{
	char	*buf;
	char	*p;

	buf = malloc((i - prev_i + 1) * sizeof(char));
	p = buf;
	while (prev_i < i)
		*p++ = s[prev_i++];
	*p = 0;
	return (buf);
}

static	void	split(char **dst, char const *s, char c)
{
	int	i;
	int	prev_i;

	i = 0;
	while (s[i])
	{
		prev_i = i;
		while (s[i] && s[i] != c)
			i++;
		if (i > prev_i)
			*dst++ = dup_into(s, prev_i, i);
		if (s[i])
			i++;
	}
	*dst = 0;
}

char	**ft_split(char const *s, char c)
{
	char	**splitted;

	splitted = malloc((count_len(s, c) + 1) * sizeof(char *));
	if (!splitted)
		return (0);
	split(splitted, s, c);
	return (splitted);
}
