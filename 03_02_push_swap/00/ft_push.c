/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 15:16:41 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 15:16:41 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

static	void	ft_push(t_list **from, t_list **to)
{
	t_list	*to_top;
	t_list	*from_top;

	to_top = *to;
	from_top = *from;
	*to = from_top;
	*from = from_top -> next;
	from_top -> next = to_top;
}

int	ft_push_pa(t_list **a, t_list **b, int to_write)
{
	ft_push(b, a);
	if (to_write)
		ft_write_str("pa\n");
	return (1);
}

int	ft_push_pb(t_list **a, t_list **b, int to_write)
{
	ft_push(a, b);
	if (to_write)
		ft_write_str("pb\n");
	return (1);
}
