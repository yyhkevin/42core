/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 15:19:35 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 15:19:35 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

static	void	ft_swap_s(t_list **lst)
{
	t_list	*first;
	t_list	*second;

	first = *lst;
	second = first -> next;
	*lst = second;
	first -> next = second -> next;
	second -> next = first;
}

void	ft_swap_sa(t_list **lst)
{
	ft_swap_s(lst);
	ft_write_str("sa\n");
}

void	ft_swap_sb(t_list **lst)
{
	ft_swap_s(lst);
	ft_write_str("sb\n");
}

void	ft_swap_ss(t_list **lst1, t_list **lst2)
{
	ft_swap_s(lst1);
	ft_swap_s(lst2);
	ft_write_str("ss\n");
}
