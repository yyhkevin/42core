/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/06 17:23:31 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 17:08:48 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

static	int	ft_is_valid_int(char *str)
{
	if (*str == '-' || (*str >= '0' && *str <= '9'))
	{
		if (*str == '-' && *(str + 1) == 0)
			return (0);
		str++;
		while (*str)
		{
			if (*str < '0' || *str > '9')
				return (0);
			str++;
		}
	}
	else
		return (0);
	return (1);
}

static	int	ft_has_dups(t_list **lst, int val)
{
	t_list	*p;

	p = *lst;
	while (p)
	{
		if (*(int *)p -> content == val)
			return (1);
		p = p -> next;
	}
	return (0);
}

t_list	**ft_to_atol_list(char **lst, int is_malloc)
{
	long long	converted;
	t_list		**head;
	t_list		*curr;
	int			*container;
	int			index;

	index = 0;
	converted = 0;
	head = malloc(sizeof(t_list **));
	while (lst[index])
	{
		if (!ft_is_valid_int(lst[index]))
			ft_error_killall(head, lst, is_malloc, 0);
		container = malloc(sizeof(int));
		converted = ft_atol(lst[index]);
		if (converted > 2147483647 || converted < -2147483648)
			ft_error_killall(head, lst, is_malloc, container);
		*container = (int) converted;
		if (ft_has_dups(head, *container))
			ft_error_killall(head, lst, is_malloc, container);
		curr = ft_lstnew(container);
		ft_lstadd_back(head, curr);
		index++;
	}
	return (head);
}

int	main(int argc, char **argv)
{
	char	**p;
	int		is_malloc;

	if (argc == 1)
		ft_perror();
	is_malloc = 0;
	if (argc == 2)
	{
		p = ft_split(argv[1], ' ');
		is_malloc = 1;
	}
	else
		p = argv + 1;
	ft_sort(p, is_malloc);
	return (0);
}
