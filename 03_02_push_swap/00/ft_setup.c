/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setup.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 15:34:32 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 18:10:19 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

// if a requires 2r, b requires 1r, total is 2r
int	ft_total_rotates(int from_a, int from_b, int len_a)
{
	if (from_a > len_a / 2)
		from_a -= len_a;
	if (from_a > 0 && from_b > 0)
	{
		if (from_a > from_b)
			return (from_a);
		else
			return (from_b);
	}
	if (from_a < 0 && from_b < 0)
	{
		if (from_a < from_b)
			return (-from_a);
		else
			return (-from_b);
	}
	if (from_a < 0)
		from_a *= -1;
	if (from_b < 0)
		from_b *= -1;
	return (from_a + from_b);
}

void	ft_calc_pos(t_position *pos, t_list **a, t_list **b, int min_i)
{
	int		tmp;
	int		tgt;
	t_list	*p;
	int		len;

	len = ft_lstsize(*a);
	tmp = min_i;
	p = *a;
	while (tmp)
	{
		p = p -> next;
		tmp--;
	}
	tmp = ft_setup_case(*(int *)p -> content, b);
	tgt = ft_total_rotates(min_i, tmp, ft_lstsize(*a));
	if (min_i > len / 2)
		pos -> a_rot = min_i - len;
	else
		pos -> a_rot = min_i;
	pos -> b_rot = tmp;
	pos -> total = tgt;
}

void	ft_do_opti_2(t_list **stack_a, t_list **stack_b, t_position pos)
{
	while (pos.a_rot > 0)
	{
		ft_rotate_ra(stack_a, 1);
		pos.a_rot--;
	}
	while (pos.a_rot < 0)
	{
		ft_reverse_rra(stack_a, 1);
		pos.a_rot++;
	}
	while (pos.b_rot > 0)
	{
		ft_rotate_rb(stack_b, 1);
		pos.b_rot--;
	}
	while (pos.b_rot < 0)
	{
		ft_reverse_rrb(stack_b, 1);
		pos.b_rot++;
	}
}

void	ft_do_optimizations(t_list **stack_a, t_list **stack_b, int min_index)
{
	t_position	pos;

	ft_calc_pos(&pos, stack_a, stack_b, min_index);
	while (pos.a_rot > 0 && pos.b_rot > 0)
	{
		ft_rotate_rr(stack_a, stack_b);
		pos.a_rot--;
		pos.b_rot--;
	}
	while (pos.a_rot < 0 && pos.b_rot < 0)
	{
		ft_reverse_rrr(stack_a, stack_b);
		pos.a_rot++;
		pos.b_rot++;
	}
	ft_do_opti_2(stack_a, stack_b, pos);
}

void	ft_setup(t_list **stack_a, t_list **stack_b)
{
	t_list		*curr;
	t_tracker	tr;

	curr = *stack_a;
	tr.i = 0;
	tr.min_index = 0;
	tr.min = -1;
	while (curr)
	{
		tr.tmp = ft_setup_case(*(int *)curr -> content, stack_b);
		tr.tmp = ft_total_rotates(tr.i, tr.tmp, ft_lstsize(*stack_a));
		if (tr.min == -1)
			tr.min = tr.tmp;
		else if (tr.tmp < tr.min)
		{
			tr.min = tr.tmp;
			tr.min_index = tr.i;
		}
		tr.i++;
		curr = curr -> next;
	}
	ft_do_optimizations(stack_a, stack_b, tr.min_index);
}
