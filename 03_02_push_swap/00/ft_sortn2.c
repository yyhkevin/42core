/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sortn2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 15:34:32 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 18:10:19 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

int	ft_get_minmax(t_list *stack, int mode)
{
	int	minmax;
	int	tmp;

	minmax = *(int *)stack -> content;
	stack = stack -> next;
	while (stack)
	{
		tmp = *(int *)stack -> content;
		if (mode == 1 && tmp > minmax)
			minmax = tmp;
		if (mode == 0 && tmp < minmax)
			minmax = tmp;
		stack = stack -> next;
	}
	return (minmax);
}

// mode 0 - reverse
// mode 1 - forward 
int	ft_count_rotates(int mode, t_list **stack, t_list *item)
{
	t_list	*cmp;
	int		count;
	int		tmp;

	cmp = *stack;
	count = 0;
	while (cmp != item)
	{
		if (mode == 0)
			ft_reverse_rra(stack, 0);
		if (mode == 1)
			ft_rotate_ra(stack, 0);
		cmp = *stack;
		count++;
	}
	tmp = 0;
	while (tmp < count)
	{
		if (mode == 0)
			ft_rotate_ra(stack, 0);
		if (mode == 1)
			ft_reverse_rra(stack, 0);
		tmp++;
	}
	return (count);
}

int	ft_count_sorted(int mode, t_list **stack)
{
	int	count;
	int	tmp;

	count = 0;
	while (!ft_is_sorted(stack, 0))
	{
		if (mode == 0)
			ft_reverse_rra(stack, 0);
		if (mode == 1)
			ft_rotate_ra(stack, 0);
		count++;
	}
	tmp = 0;
	while (tmp < count)
	{
		if (mode == 0)
			ft_rotate_ra(stack, 0);
		if (mode == 1)
			ft_reverse_rra(stack, 0);
		tmp++;
	}
	return (count);
}

void	ft_apply_between(t_list **a, t_list **b, t_list *item)
{
	int	rotate;
	int	reverse;

	rotate = ft_count_rotates(1, a, item);
	reverse = ft_count_rotates(0, a, item);
	if (reverse < rotate)
	{
		while (reverse > 0)
		{
			ft_reverse_rra(a, 1);
			reverse--;
		}
	}
	else
	{
		while (rotate > 0)
		{
			ft_rotate_ra(a, 1);
			rotate--;
		}
	}
	ft_push_pa(a, b, 1);
}

void	ft_apply_sorted(t_list **stack_a)
{
	int	rotate;
	int	reverse;

	rotate = ft_count_sorted(1, stack_a);
	reverse = ft_count_sorted(0, stack_a);
	if (reverse < rotate)
	{
		while (reverse > 0)
		{
			ft_reverse_rra(stack_a, 1);
			reverse--;
		}
	}
	else
	{
		while (rotate > 0)
		{
			ft_rotate_ra(stack_a, 1);
			rotate--;
		}
	}
}
