/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/20 14:36:43 by keyu              #+#    #+#             */
/*   Updated: 2023/09/20 14:44:20 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void *content)
{
	t_list	*buf;

	buf = malloc(sizeof(t_list));
	if (!buf)
		return (0);
	buf -> content = content;
	buf -> next = 0;
	return (buf);
}
