/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_kill.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/06 17:23:31 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 17:26:19 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

// is_m : is_malloc, is_e : is_error
void	ft_killall(t_list **lst, char **parsed, int is_m, int is_e)
{
	int	index;

	index = 0;
	ft_lstclear(lst, free);
	free(lst);
	if (is_m)
	{
		while (parsed[index])
			free(parsed[index++]);
		free(parsed);
	}
	if (is_e)
		ft_perror();
}

// is_m : is_malloc
void	ft_error_killall(t_list **lst, char **parsed, int is_m, int *c)
{
	int	index;

	index = 0;
	ft_lstclear(lst, free);
	free(lst);
	if (is_m)
	{
		while (parsed[index])
			free(parsed[index++]);
		free(parsed);
	}
	if (c)
		free(c);
	ft_perror();
}
