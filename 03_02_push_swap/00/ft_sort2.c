/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 15:34:32 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 18:10:19 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

void	ft_sort2(t_list **stack_a)
{
	int		first;
	int		second;
	t_list	*p;

	p = *stack_a;
	first = *(int *)p -> content;
	second = *(int *)p -> next ->content;
	if (first > second)
		ft_swap_sa(stack_a);
}
