/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 15:34:32 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 18:10:19 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

void	ft_sort3(t_list **stack_a)
{
	int		d1;
	int		d2;
	int		d3;

	d1 = *(int *)(*stack_a)-> content;
	d2 = *(int *)((*stack_a)-> next)-> content;
	d3 = *(int *)((*stack_a)-> next-> next)-> content;
	if (d1 <= d2 && d2 <= d3)
		return ;
	else if (d1 <= d2 && d2 >= d3 && d1 <= d3)
	{
		ft_swap_sa(stack_a);
		ft_rotate_ra(stack_a, 1);
	}
	else if (d1 >= d2 && d2 <= d3 && d1 <= d3)
		ft_swap_sa(stack_a);
	else if (d1 <= d2 && d2 >= d3 && d1 >= d3)
		ft_reverse_rra(stack_a, 1);
	else if (d1 >= d2 && d2 <= d3 && d1 >= d3)
		ft_rotate_ra(stack_a, 1);
	else
	{
		ft_swap_sa(stack_a);
		ft_reverse_rra(stack_a, 1);
	}
}
