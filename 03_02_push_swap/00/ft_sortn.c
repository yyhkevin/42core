/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sortn.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 15:34:32 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 18:10:19 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

static	void	ft_insert_edge(t_list **stack_a, t_list **stack_b)
{
	int	min;
	int	max;
	int	cmp;

	min = ft_get_minmax(*stack_a, 0);
	max = ft_get_minmax(*stack_a, 1);
	cmp = *(int *)(*stack_b)-> content;
	if (cmp <= min || cmp >= max)
	{
		ft_apply_sorted(stack_a);
		ft_push_pa(stack_a, stack_b, 1);
	}
}

void	ft_insert(t_list **stack_a, t_list **stack_b)
{
	int		cmp;
	t_list	*p1;
	t_list	*p2;

	p1 = *stack_a;
	p2 = p1 -> next;
	cmp = *(int *)(*stack_b)-> content;
	while (p2)
	{
		if (*(int *)p1 -> content <= cmp && cmp <= *(int *)p2 -> content)
		{
			ft_apply_between(stack_a, stack_b, p2);
			return ;
		}
		p1 = p2;
		p2 = p1 -> next;
	}
	p2 = *stack_a;
	if (*(int *)p1 -> content <= cmp && cmp <= *(int *)p2 -> content)
	{
		ft_apply_between(stack_a, stack_b, p2);
		return ;
	}
	ft_insert_edge(stack_a, stack_b);
}

void	ft_sortn(t_list **stack_a, t_list **stack_b)
{
	if (ft_is_sorted(stack_a, 0))
		return ;
	ft_push_pb(stack_a, stack_b, 1);
	ft_push_pb(stack_a, stack_b, 1);
	while (ft_lstsize(*stack_a) > 3)
	{
		ft_setup(stack_a, stack_b);
		ft_push_pb(stack_a, stack_b, 1);
	}
	if (ft_lstsize(*stack_a) == 2)
		ft_sort2(stack_a);
	else
		ft_sort3(stack_a);
	while (ft_lstsize(*stack_b))
		ft_insert(stack_a, stack_b);
	ft_apply_sorted(stack_a);
}
