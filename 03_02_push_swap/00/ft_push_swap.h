/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push_swap.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/06 17:23:31 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 17:26:19 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PUSH_SWAP_H
# define FT_PUSH_SWAP_H

// for write
# include <unistd.h>

// for malloc, free
# include <stdlib.h>

# include "libft.h"

typedef struct s_position
{
	int	a_rot;
	int	b_rot;
	int	total;
}	t_position;

typedef struct s_tracker
{
	int	i;
	int	min;
	int	min_index;
	int	tmp;
}	t_tracker;

char		**ft_split(char const *s, char c);
long long	ft_atol(char *s);
void		ft_perror(void);
void		ft_write_str(char *str);
void		ft_swap_sa(t_list **lst);
void		ft_swap_sb(t_list **lst);
void		ft_swap_ss(t_list **lst1, t_list **lst2);
int			ft_push_pa(t_list **a, t_list **b, int to_write);
int			ft_push_pb(t_list **a, t_list **b, int to_write);
int			ft_rotate_ra(t_list **lst, int to_write);
int			ft_rotate_rb(t_list **lst, int to_write);
void		ft_rotate_rr(t_list **lst1, t_list **lst2);
int			ft_reverse_rra(t_list **lst, int to_write);
int			ft_reverse_rrb(t_list **lst, int to_write);
void		ft_reverse_rrr(t_list **lst1, t_list **lst2);
void		ft_sort(char **parsed, int is_malloc);
void		ft_sort2(t_list **stack_a);
void		ft_sort3(t_list **stack_a);
void		ft_sortn(t_list **stack_a, t_list **stack_b);
void		ft_killall(t_list **lst, char **parsed, int is_m, int is_e);
void		ft_error_killall(t_list **lst, char **parsed, int is_m, int *c);
t_list		**ft_to_atol_list(char **lst, int is_malloc);
int			ft_is_sorted(t_list **lst, int mode);
void		ft_setup(t_list **stack_a, t_list **stack_b);
void		ft_insert(t_list **stack_a, t_list **stack_b);
int			ft_count_rotates(int mode, t_list **stack, t_list *item);
int			ft_get_minmax(t_list *stack, int mode);
t_list		*ft_get_node(t_list **stack, int val);
void		ft_return_state(int count, t_list **stack);
int			ft_count_case_between(int cmp, t_list **stack_b);
int			ft_cmp_maxmin(int mode, int num1, int num2);
int			ft_count_case_edge(t_list **stack);
int			ft_setup_case(int cmp, t_list **stack_b);
int			ft_get_minmax(t_list *stack, int mode);
int			ft_count_rotates(int mode, t_list **stack, t_list *item);
int			ft_count_sorted(int mode, t_list **stack);
void		ft_apply_between(t_list **a, t_list **b, t_list *item);
void		ft_apply_sorted(t_list **stack_a);

#endif
