/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/02 15:21:51 by keyu              #+#    #+#             */
/*   Updated: 2024/03/02 17:26:03 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

// forward (top to bottom of stack)
void	ft_read_lst(t_list **lst)
{
	t_list	*plist;

	plist = *lst;
	while (plist)
	{
		printf("%d\n", *(int *)(plist -> content));
		plist = plist -> next;
	}
}

// backward (bottom to top of stack)
void	ft_read_lst2(t_list *lst)
{
	if (lst == 0)
		return ;
	ft_read_lst2(lst -> next);
	printf("%d\n", *(int *)lst -> content);
}

void	ft_read(t_list **stack_a, t_list **stack_b)
{
	printf("---\n");
	ft_read_lst(stack_a);
	printf("---stack a\n");
	ft_read_lst(stack_b);
	printf("---stack b\n");
}
