#include "Account.hpp"
#include <iostream>
#include <ctime>

int	Account::_nbAccounts = 0;
int	Account::_totalAmount = 0;
int	Account::_totalNbDeposits = 0;
int	Account::_totalNbWithdrawals = 0;

void Account::_displayTimestamp( void ) {
	time_t now = time(NULL);
    tm *tm = localtime(&now);
    char str[18];
    std::strftime(str, 18, "[%Y%m%d_%H%M%S]", tm);
    std::cout << str;
    // std::cout << "[19920104_091532]";
}

int Account::getNbAccounts( void ) {
    return Account::_nbAccounts;
}

int Account::getTotalAmount( void ) {
    return Account::_totalAmount;
}

int Account::getNbDeposits( void ) {
    return Account::_totalNbDeposits;
}

int Account::getNbWithdrawals( void ) {
    return Account::_totalNbWithdrawals;
}

void Account::displayAccountsInfos( void ) {
    Account::_displayTimestamp();
    std::cout
        << " accounts:" << Account::getNbAccounts()
        << ";total:"<< Account::getTotalAmount()
        << ";deposits:" << Account::getNbDeposits()
        << ";withdrawals:" << Account::getNbWithdrawals()
        << std::endl;
}

Account::Account( int initial_deposit ) {
    this->_accountIndex = Account::_nbAccounts;
    this->_amount = initial_deposit;
    this->_nbDeposits = 0;
    this->_nbWithdrawals = 0;
    Account::_totalAmount += initial_deposit;
    Account::_nbAccounts++;
    Account::_displayTimestamp();
    std::cout
        << " index:" << this->_accountIndex
        << ";amount:" << initial_deposit
        << ";created"
        << std::endl;
}

Account::~Account( void ) {
    Account::_displayTimestamp();
    std::cout
        << " index:" << this->_accountIndex
        << ";amount:" << this->checkAmount()
        << ";closed"
        << std::endl;
}

void Account::makeDeposit( int deposit ) {
    Account::_displayTimestamp();
    std::cout
        << " index:" << this->_accountIndex
        << ";p_amount:" << this->checkAmount();
    this->_amount += deposit;
    Account::_totalAmount += deposit;
    this->_nbDeposits++;
    Account::_totalNbDeposits++;
    std::cout
        << ";deposit:" << deposit
        << ";amount:" << this->checkAmount()
        << ";nb_deposits:" << this->_nbDeposits
        << std::endl;
}

bool Account::makeWithdrawal( int withdrawal ) {
    Account::_displayTimestamp();
    std::cout
        << " index:" << this->_accountIndex
        << ";p_amount:" << this->checkAmount()
        << ";withdrawal:";
    if (withdrawal > this->checkAmount()) {
        std::cout << "refused" << std::endl;
        return false;
    }
    this->_amount -= withdrawal;
    Account::_totalAmount -= withdrawal;
    this->_nbWithdrawals++;
    Account::_totalNbWithdrawals++;
    std::cout
        << withdrawal
        << ";amount:" << this->checkAmount()
        << ";nb_withdrawals:" << this->_nbWithdrawals
        << std::endl;
    return true;
}

int	 Account::checkAmount( void ) const {
    return this->_amount;
}

void Account::displayStatus( void ) const {
    Account::_displayTimestamp();
    std::cout
        << " index:" << this->_accountIndex
        << ";amount:" << this->checkAmount()
        << ";deposits:" << this->_nbDeposits
        << ";withdrawals:" << this->_nbWithdrawals
        << std::endl;
}