#include "PhoneBook.hpp"

PhoneBook::PhoneBook(void) : _pointer(0) {}

static int isEmptyToUser(const std::string &str) {
    if (str.empty())
        return true;
    for (int i = 0; i < str.length(); i++) {
        if (str[i] != 32 && (str[i] < 9 || str[i] > 13))
            return false;
    }
    return true;
}

static std::string ensureNonEmpty() {
    std::string input;

    while (true) {
        std::getline(std::cin, input);
        if (std::cin.eof())
            exit(0);
        if (isEmptyToUser(input))
            std::cout << "No empty fields are allowed. Kindly try again:" << std::endl;
        else
            break;
    }
    return input;
}

static void incrementPointer(int &ptr) {
    ptr++;
    if (ptr == SIZE)
        ptr = 0;
}

void PhoneBook::addContact(void) {
    Contact &contact = this->_contacts[this->_pointer];
    
    std::cout << "Enter first name:" << std::endl;
    contact.setFirstName(ensureNonEmpty());
    std::cout << "Enter last name:" << std::endl;
    contact.setLastName(ensureNonEmpty());
    std::cout << "Enter nickname:" << std::endl;
    contact.setNickname(ensureNonEmpty());
    std::cout << "Enter phone:" << std::endl;
    contact.setPhone(ensureNonEmpty());
    std::cout << "Enter secret:" << std::endl;
    contact.setSecret(ensureNonEmpty());

    incrementPointer(this->_pointer);

    std::cout << "Okay, added contact!" << std::endl;
}

static void showHeader(void) {
    std::cout << "|     Index|First Name| Last Name|  Nickname|" << std::endl;
}

static void tableDisplaySpaces(int count) {
    while (count > 0) {
        std::cout << " ";
        count--;
    }
}

static void tableDisplayPipe() {
    std::cout << "|";
}

static void tableDisplayField(std::string field) {
    tableDisplaySpaces(COL - field.size());
    if (field.size() > COL) {
        field.resize(COL);
        field[COL - 1] = '.';
    }
    std::cout << field;
    tableDisplayPipe();
}

static void tableDisplayContact(Contact &c, int index) {
    tableDisplayPipe();
    tableDisplaySpaces(COL-1);
    std::cout << index + 1;
    tableDisplayPipe();
    tableDisplayField(c.getFirstName());
    tableDisplayField(c.getLastName());
    tableDisplayField(c.getNickname());
    std::cout << std::endl;
}

static int isValidIndex(std::string s) {
    return s.size() == 1 && s[0] >= '1' && s[0] <= '8';
}

static void displayField(std::string key, std::string value) {
    std::cout << key << ": " << value << std::endl;
}

static void displayInfo(Contact &c) {
    if (c.getFirstName().empty()) {
        std::cout << "Empty contact" << std::endl;
        return;
    }
    std::cout << "-----" << std::endl;
    displayField("First name", c.getFirstName());
    displayField("Last name", c.getLastName());
    displayField("Nickname", c.getNickname());
    displayField("Phone", c.getPhone());
    displayField("Secret", c.getSecret());
    std::cout << "-----" << std::endl;
}

void PhoneBook::search(void) {
    std::string input;
    showHeader();
    int i = 0;
    while (i < SIZE) {
        tableDisplayContact(this->_contacts[i], i);
        i++;
    }
    std::cout << "Show a contact by input of its index (1-8):" << std::endl;
    std::getline(std::cin, input);
    while (!(isValidIndex(input))) {
        std::cout << "Invalid input: " << input << ". Try again:" << std::endl;
        std::getline(std::cin, input);
    }
    displayInfo(this->_contacts[input[0] - '0' - 1]);
}

// void PhoneBook::populate(void) {
//     this->_contacts[0].setContact("Apple", "fruit", "theapple", "12345678", "oooh");
//     this->_contacts[1].setContact("Boat", "transport", "boating", "12345679", "ooohla");
//     this->_contacts[2].setContact("Cat", "feline", "kitty", "32345679", "catie");
//     this->_contacts[3].setContact("Degree", "uni", "university", "34345679", "mmmm");
//     this->_contacts[4].setContact("Egg", "food", "chickenpie", "34545679", "tastying");
//     this->_contacts[5].setContact("Freak", "weirdos", "eyebleachpls", "34565679", "ohno");
//     this->_contacts[6].setContact("Gentleman", "niceguy", "handsome", "34567679", "yespls");
//     this->_contacts[7].setContact("Helicopter", "flyingthing", "tourgroup", "34567879", "expensive");
// }