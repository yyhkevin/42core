#pragma once
#include <iostream>

class Contact {
    public:
        Contact(void);
        void setFirstName(const std::string &fName);
        void setLastName(const std::string &lName);
        void setNickname(const std::string &nName);
        void setPhone(const std::string &phone);
        void setSecret(const std::string &secret);
        const std::string &getFirstName(void);
        const std::string &getLastName(void);
        const std::string &getNickname(void);
        const std::string &getPhone(void);
        const std::string &getSecret(void);
        void setContact(const std::string &fName, const std::string &lName, const std::string &nName, const std::string &phone, const std::string &secret);
    private:
        std::string _firstName;
        std::string _lastName;
        std::string _nickname;
        std::string _phone;
        std::string _secret;
};