#include "PhoneBook.hpp"
#include <iostream>

static void showMenu() {
    std::cout << "Usage: " << ADD << " / " << SEARCH << " / " << EXIT << std::endl;
}

static void showUnknownCommand(std::string cmd) {
    std::cout << "Unknown command \"" << cmd << "\". Kindly enter one of the commands (case-sensitive)." << std::endl;
}

int main(int argc, char **argv) {
    PhoneBook phoneBook;
    std::string input;

    while (true) {
        showMenu();
        std::getline(std::cin, input);
        if (std::cin.eof())
            break;
        if (input.compare(ADD) == 0) {
            phoneBook.addContact();
        } else if (input.compare(SEARCH) == 0) {
            phoneBook.search();
        } else if (input.compare(EXIT) == 0)
            break;
        else
            showUnknownCommand(input);
    }
    return 0;
}