#pragma once
#include "Contact.hpp"

#define ADD "ADD"
#define SEARCH "SEARCH"
#define EXIT "EXIT"
#define SIZE 8
#define COL 10

class PhoneBook {
    public:
        PhoneBook(void);
        void addContact();
        void search();
        // void populate();
    private:
        Contact _contacts[SIZE];
        int _pointer;
};
