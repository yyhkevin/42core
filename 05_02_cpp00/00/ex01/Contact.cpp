#include "Contact.hpp"

Contact::Contact(void)
    : _firstName(""), _lastName(""), _nickname(""), _phone(""), _secret("") {}

void Contact::setFirstName(const std::string &fName) {
    this->_firstName = fName;
}

void Contact::setLastName(const std::string &lName) {
    this->_lastName = lName;
}

void Contact::setNickname(const std::string &nName) {
    this->_nickname = nName;
}

void Contact::setPhone(const std::string &phone) {
    this->_phone = phone;
}

void Contact::setSecret(const std::string &secret) {
    this->_secret = secret;
}

const std::string &Contact::getFirstName(void) {
    return this->_firstName;
}

const std::string &Contact::getLastName(void) {
    return this->_lastName;
}

const std::string &Contact::getNickname(void) {
    return this->_nickname;
}

const std::string &Contact::getPhone(void) {
    return this->_phone;
}

const std::string &Contact::getSecret(void) {
    return this->_secret;
}

void Contact::setContact(const std::string &fName, const std::string &lName, const std::string &nName, const std::string &phone, const std::string &secret) {
    this->setFirstName(fName);
    this->setLastName(lName);
    this->setNickname(nName);
    this->setPhone(phone);
    this->setSecret(secret);
}