#include <iostream>

static void p_char(char c) {
	std::cout << (char) std::toupper(c);
}

static void p_arg(char *arg) {
	while (*arg)
		p_char(*arg++);
}

int main(int argc, char **argv) {
	if (argc == 1)
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *";
	int i = 1;
	while (i < argc)
		p_arg(argv[i++]);
	std::cout << std::endl;
	return 0;
}
