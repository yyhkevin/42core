/* result OK */
/* libraries */
#include <stdlib.h> // for malloc, free, exit
#include <sys/types.h> // for pid_t
#include <unistd.h> // for write, pipe, fork, dup2, chdir, execve
#include <sys/wait.h> // for waitpid
#include <string.h> // for strcmp

/* libft */
size_t ft_strlen(const char *s)
{
 size_t i;

 i = 0;
 while (s[i] != '\0')
  i++;
 return (i);
}

/* utilities */
int error_info(char *title, char *message, char *input)
{
 write(STDERR_FILENO, "error: ", 7);
 if (title != NULL)
 {
  write(STDERR_FILENO, title, ft_strlen(title));
  write(STDERR_FILENO, ": ", 2);
 }
 if (message != NULL)
  write(STDERR_FILENO, message, ft_strlen(message));
 if (input != NULL)
 {
  write(STDERR_FILENO, " ", 1);
  write(STDERR_FILENO, input, ft_strlen(input));
 }
 write(STDERR_FILENO, "\n", 1);
 return (EXIT_FAILURE);
}

/* helper functions */
int ft_cd(char **str, int i)
{
 if (i != 2)
  return (error_info("cd", "bad arguments", NULL));
 if (chdir(str[1]) == -1)
  return (error_info("cd", "cannot change directory to", str[1]));
 return (EXIT_SUCCESS);
}

int execute_cmd(char **str, int i, char **envp)
{
 if (str[0] == NULL)
  return (error_info(NULL, "fatal", NULL));
 else
 {
  if (strcmp(str[0], "cd") == 0)
   return (ft_cd(str, i));
  if (execve(str[0], str, envp) == -1)
   return (error_info(NULL, "cannot execute", str[0]));
 }
 return (EXIT_SUCCESS);
}

int process_fds(int fd_to_dup, int std_fd, int fd_to_close)
{
 if (dup2(fd_to_dup, std_fd) == -1)
  return (error_info(NULL, "fatal", NULL));
 if (close(fd_to_dup) == -1)
  return (error_info(NULL, "fatal", NULL));
 if (close(fd_to_close) == -1)
  return (error_info(NULL, "fatal", NULL));
 return (EXIT_SUCCESS);
}

int execution(char **str, int i, char **envp)
{
 int  status;
 int  pipe_flag;
 int  pipe_fd[2];
 pid_t child_pid;

 status = 0;
 pipe_flag = 0;
 if (str[i] != NULL && strcmp(str[i], "|") == 0)
  pipe_flag = 1;
 if (pipe_flag == 0 && strcmp(str[0], "cd") == 0)
  return (ft_cd(str, i));
 if (pipe_flag == 1 && pipe(pipe_fd) == -1)
  return (error_info(NULL, "fatal", NULL));
 child_pid = fork();
 if (child_pid == 0)
 {
  str[i] = NULL;
  if (pipe_flag == 1)
   status = process_fds(pipe_fd[1], STDOUT_FILENO, pipe_fd[0]);
  status = execute_cmd(str, i, envp);
  return (status);
 }
 waitpid(child_pid, &status, 0);
 if (pipe_flag == 1)
  status = process_fds(pipe_fd[0], STDIN_FILENO, pipe_fd[1]);
 if (WIFEXITED(status))
  return (WEXITSTATUS(status));
 return (status);
}

/* main */
int main(int argc, char *argv[], char **envp)
{
 int  status;
 int  i;
 char **cmd_start;
 int  cmd_len;
 
 status = 0;
 i = 1;
 if (argc > 1)
 {
  while (argv[i] != NULL)
  {
   cmd_start = &argv[i];
   cmd_len = 0;
   while (argv[i] != NULL && strcmp(argv[i], "|") != 0 && \
    strcmp(argv[i], ";") != 0)
   {
    i++;
    cmd_len++;
   }
   if (cmd_len > 0)
    status = execution(cmd_start, cmd_len, envp);
   while (argv[i] != NULL && (strcmp(argv[i], "|") == 0 || \
    strcmp(argv[i], ";") == 0))
    i++;
  }
 }
 return (status);
}