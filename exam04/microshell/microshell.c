#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>

int ft_strlen(char *s)
{
    int i;

    i = 0;
    while (s[i])
        i++;
    return i;
}

int error_info(char *title, char *msg, char *input)
{
    write(STDERR_FILENO, "error: ", 7);
    if (title)
    {
        write(STDERR_FILENO, title, ft_strlen(title));
        write(STDERR_FILENO, ": ", 2);
    }
    if (msg)
        write(STDERR_FILENO, msg, ft_strlen(msg));
    if (input)
    {
        write(STDERR_FILENO, " ", 1);
        write(STDERR_FILENO, input, ft_strlen(input));
    }
    write(STDERR_FILENO, "\n", 1);
    return EXIT_FAILURE;
}

int process_fds(int fd_to_dup, int std_fd, int fd_to_close)
{
    if (dup2(fd_to_dup, std_fd) == -1)
        return error_info(0, "fatal", 0);
    if (close(fd_to_dup) == -1)
        return error_info(0, "fatal", 0);
    if (close(fd_to_close) == -1)
        return error_info(0, "fatal", 0);
    return EXIT_SUCCESS;
}

int ft_cd(char **str, int i)
{
    if (i != 2)
        return error_info("cd", "bad arguments", 0);
    if (chdir(str[1]) == -1)
        return error_info("cd", "cannot change directory to", str[1]);
    return EXIT_SUCCESS;
}

int execute_cmd(char **str, int i, char **envp)
{
    if (!str[0])
        return error_info(0, "fatal", 0);
    if (!strcmp(str[0], "cd"))
        return ft_cd(str, i);
    if (execve(str[0], str, envp) == -1)
        return error_info(0, "cannot execute", str[0]);
    return EXIT_SUCCESS;
}

int execution(char **str, int i, char **envp)
{
    int pipe_flag;
    int pipe_fd[2];
    int child_pid;
    int status;

    status = 0;
    pipe_flag = str[i] && !strcmp(str[i], "|");
    if (!pipe_flag && !strcmp(str[0], "cd"))
        return ft_cd(str, i);
    if (pipe_flag && pipe(pipe_fd) == -1)
        return error_info(0, "fatal", 0);
    child_pid = fork();
    if (!child_pid)
    {
        str[i] = 0;
        if (pipe_flag)
            status = process_fds(pipe_fd[1], STDOUT_FILENO, pipe_fd[0]);
        return execute_cmd(str, i, envp);
    }
    waitpid(child_pid, &status, 0);
    if (pipe_flag)
        status = process_fds(pipe_fd[0], STDIN_FILENO, pipe_fd[1]);
    if (WIFEXITED(status))
        return WEXITSTATUS(status);
    return status;
}

int main(int argc, char **argv, char **envp)
{
    int status;
    int i;
    char **cmd_start;
    int cmd_len;

    if (argc <= 1)
        return (0);
    status = 0;
    i = 1;
    while (argv[i])
    {
        cmd_start = argv + i;
        cmd_len = 0;
        while (argv[i] && strcmp(argv[i], "|") && strcmp(argv[i], ";"))
        {
            cmd_len++;
            i++;
        }
        if (cmd_len > 0)
            status = execution(cmd_start, cmd_len, envp);
        while (argv[i] && (!strcmp(argv[i], "|") || !(strcmp(argv[i], ";"))))
            i++;
    }
    return status;
}