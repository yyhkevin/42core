/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/06 17:23:31 by keyu              #+#    #+#             */
/*   Updated: 2023/09/20 13:02:13 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	char	*p_d;
	char	*p_s;

	if (!dst && !src)
		return (dst);
	p_d = dst;
	p_s = (char *)src;
	while (n > 0)
	{
		*p_d++ = *p_s++;
		n--;
	}
	return (dst);
}
