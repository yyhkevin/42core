/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/06 17:23:31 by keyu              #+#    #+#             */
/*   Updated: 2023/09/06 17:28:38 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcpy(char *dst, char *src, size_t dst_size)
{
	int	len;

	len = 0;
	if (dst_size == 0)
	{
		while (*(src + len))
			len++;
		return (len);
	}
	while (*src && --dst_size)
	{
		*dst++ = *src++;
		len++;
	}
	*dst = 0;
	while (*src)
	{
		src++;
		len++;
	}
	return (len);
}
