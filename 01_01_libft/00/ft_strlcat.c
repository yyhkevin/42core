/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/06 17:23:31 by keyu              #+#    #+#             */
/*   Updated: 2023/09/20 10:11:05 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t dst_size)
{
	size_t	dstlen;
	size_t	srclen;

	dstlen = ft_strlen(dst);
	srclen = ft_strlen(src);
	if (dstlen > dst_size)
		dstlen = dst_size;
	if (!dst_size || dstlen >= dst_size)
		return (dstlen + srclen);
	dst += dstlen;
	dst_size = dst_size - dstlen - 1;
	while (dst_size > 0 && *src)
	{
		*dst++ = *src++;
		dst_size--;
	}
	*dst = 0;
	return (dstlen + srclen);
}
