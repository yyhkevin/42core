/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/06 17:23:31 by keyu              #+#    #+#             */
/*   Updated: 2023/09/20 12:37:18 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	size_t	i;
	size_t	size;
	char	*buf;

	i = 0;
	while (*(s + i))
		i++;
	if (i > start + len)
		i = start + len;
	size = i - start + 1;
	if (start >= ft_strlen(s) || size <= 0)
		return (ft_calloc(1, 1));
	buf = malloc(size * sizeof(char));
	if (!buf)
		return (0);
	i = 0;
	while (i < size - 1)
	{
		*(buf + i) = *(s + start + i);
		i++;
	}
	*(buf + i) = 0;
	return (buf);
}
