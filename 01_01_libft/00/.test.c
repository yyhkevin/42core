#include <stdio.h>
#include "libft.h"
#include <stdlib.h>

void	print_char_array(char *arr, int len);
void    print_splitted(char **splitted);
char	upshift(unsigned int i, char c);
void	print_char(unsigned int i, char *c);
void	upshift_2(unsigned int i, char *c);

int main() {
	printf("ft_isalpha");
	printf("%d", ft_isalpha('c'));
	printf("%d", ft_isalpha('C'));
	printf("%d", !ft_isalpha('0'));
	printf("%d", !ft_isalpha('/'));
	printf("%d", !ft_isalpha(' '));
	printf("\n");

	printf("ft_isdigit");
	printf("%d", ft_isdigit('0'));
	printf("%d", ft_isdigit('9'));
	printf("%d", !ft_isdigit(0));
	printf("%d", !ft_isdigit(1));
	printf("%d", !ft_isdigit('A'));
	printf("\n");

	printf("ft_isalnum");
	printf("%d", ft_isalnum('0'));
	printf("%d", ft_isalnum('a'));
	printf("%d", ft_isalnum('A'));
	printf("%d", !ft_isalnum(1));
	printf("%d", !ft_isalnum(';'));
	printf("\n");

	printf("ft_isascii");
	printf("%d", ft_isascii('0'));
	printf("%d", ft_isascii(0));
	printf("%d", ft_isascii(127));
	printf("%d", !ft_isascii(128));
	printf("%d", ft_isascii(60));
	printf("\n");

	printf("ft_isprint");
	printf("%d", ft_isprint(' '));
	printf("%d", !ft_isprint(127));
	printf("%d", ft_isprint('a'));
	printf("%d", !ft_isprint(31));
	printf("%d", ft_isprint(126));
	printf("\n");

	printf("ft_strlen");
	printf("%d", ft_strlen("hello") == 5);
	printf("%d", ft_strlen("") == 0);
	printf("\n");

	char *p_c;
	printf("ft_memset - supposed to be 999999: ");
	char test[5] = {9,9,9,9,9};
	p_c = ft_memset(test, 2, 0);
	print_char_array(test, 5);
	printf("%d", *p_c);
	printf("\n");
	printf("ft_memset - supposed to be 222992: ");
	p_c = ft_memset(test, 2, 3);
	print_char_array(test, 5);
	printf("%d", *p_c);
	printf("\n");

	printf("ft_bzero - supposed to be 99999: ");
	char testbzero[5] = {9, 9, 9, 9, 9};
	ft_bzero(testbzero, 0);
	print_char_array(testbzero, 5);
	printf("\n");
	printf("ft_bzero - supposed to be 00099: ");
	ft_bzero(testbzero, 3);
	print_char_array(testbzero, 5);
	printf("\n");

	printf("ft_memcpy - supposed to be 12345671: ");
	char memcpy_src[5] = {9, 9, 9, 9, 9};
	char memcpy_dst[7] = {1, 2, 3, 4, 5, 6, 7};
	p_c = ft_memcpy(memcpy_dst, memcpy_src, 0);
	print_char_array(memcpy_dst, 7);
	printf("%d", *p_c);
	printf("\n");
	printf("ft_memcpy - supposed to be 99995679: ");
	p_c = ft_memcpy(memcpy_dst, memcpy_src, 4);
	print_char_array(memcpy_dst, 7);
	printf("%d", *p_c);
	printf("\n");
	printf("ft_memcpy - supposed to be 8889: ");
	memcpy_src[0] = 8;
	ft_memcpy(memcpy_src + 1, memcpy_src, 3);
	print_char_array(memcpy_src + 1, 4);
	printf("\n");

	printf("ft_memmove - supposed to be 23445: ");
	char mm[5] = {1, 2, 3, 4, 5};
	ft_memmove(mm, mm + 1, 3);
	print_char_array(mm, 5);
	printf("\n");
	printf("ft_memmove - supposed to be 22345: ");
	ft_memmove(mm + 1, mm, 3);
	print_char_array(mm, 5);
	printf("\n");
	printf("ft_memmove - supposed to be 22345: ");
	ft_memmove(mm, mm, 3);
	print_char_array(mm, 5);
	printf("\n");

	printf("ft_strlcpy - supposed to be AB3: ");
	char *buf = malloc(3);
	int res = ft_strlcpy(buf, "ABC", 3);
	printf("%s%d\n", buf, res);
	free(buf);
	printf("ft_strlcpy - supposed to be DEF3: ");
	buf = malloc(5);
	res = ft_strlcpy(buf, "DEF", 5);
	printf("%s%d\n", buf, res);
	free(buf);

	printf("ft_strlcat - supposed to be ABC3: ");
	buf = malloc(5);
	buf[0] = 0;
	res = ft_strlcat(buf, "ABC", 4);
	printf("%s%d\n", buf, res);
	printf("ft_strlcat - supposed to be ABCD7: ");
	res = ft_strlcat(buf, "DEFG", 5);
	printf("%s%d\n", buf, res);
	free(buf);

	printf("ft_toupper: ");
	printf("%d", 'A' == ft_toupper('a'));
	printf("%d", 'B' == ft_toupper('B'));
	printf("%d", 'Z' == ft_toupper('z'));
	printf("%d", '`' == ft_toupper('`'));
	printf("%d", '{' == ft_toupper('{'));
	printf("\n");

	printf("ft_tolower: ");
	printf("%d", 'a' == ft_tolower('A'));
	printf("%d", 'b' == ft_tolower('b'));
	printf("%d", 'z' == ft_tolower('Z'));
	printf("%d", '@' == ft_tolower('@'));
	printf("%d", '[' == ft_tolower('['));
	printf("\n");

	printf("ft_strchr - supposed to be CBC0x0: ");
	char *p = ft_strchr("ABCRC", 'C');
	printf("%c%c", *p, *(p-1));
	p = ft_strchr("ABCRC", '\0');
	printf("%c", *(p-1));
	printf("%p", ft_strchr("ABCRC", 'Z'));
	printf("\n");

	printf("ft_strrchr - supposed to be CRC0x0: ");
	p = ft_strrchr("ABCRC", 'C');
	printf("%c%c", *p, *(p-1));
	p = ft_strrchr("ABCRC", '\0');
	printf("%c", *(p-1));
	printf("%p", ft_strrchr("ABCRC", 'Z'));
	printf("\n");

	printf("ft_strncmp: ");
	buf = malloc(5);
	buf[0] = 'A';
	buf[1] = 'B';
	buf[2] = 'C';
	buf[3] = 0;
	buf[4] = 'D';
	printf("%d", 0 == ft_strncmp(buf, "ABC", 1000));
	free(buf);
	printf("%d", 1 == ft_strncmp("ABD", "ABC", 1000));
	printf("%d", -1 == ft_strncmp("ABC", "ABD", 1000));
	printf("%d", 67 == ft_strncmp("ABC", "AB", 1000));
	printf("%d", -67 == ft_strncmp("AB", "ABC", 1000));
	printf("%d", -1 == ft_strncmp("ABCD", "ABCE", 4));
	printf("%d", 0 == ft_strncmp("ABCD", "ABCE", 3));
	printf("%d", 0 == ft_strncmp("A", "B", 0));
	printf("%d", -1 == ft_strncmp("A", "B", 1));
	printf("\n");

	printf("ft_memchr - supposed to be A0x0: ");
	buf = malloc(3);
	buf[0] = 0xFF;
	buf[1] = 'A';
	p = ft_memchr(buf, 0xFF, 3);
	printf("%c", *(p + 1));
	p = ft_memchr(buf, 0xFE, 3);
	printf("%p", p);
	printf("\n");

	printf("ft_memcmp: ");
	char *buf2 = malloc(3);
	buf2[0] = 0xFF;
	buf2[1] = 'A';
	printf("%d", 0 == ft_memcmp(buf, buf2, 2));
	buf[2] = 'B';
	buf2[2] = 0;
	printf("%d", 0 == ft_memcmp(buf, buf2, 2));
	printf("%d", 66 == ft_memcmp(buf, buf2, 3));
	free(buf);
	free(buf2);
	printf("\n");

	printf("ft_strnstr - supposed to be ABC: %s\n", ft_strnstr("ABC", "", 3));
	printf("ft_strnstr - supposed to be 0x0: %p\n", ft_strnstr("", "A", 3));
	printf("ft_strnstr - supposed to be ABCADEF: %s\n", ft_strnstr("ABCADEF", "A", 3));
	printf("ft_strnstr - supposed to be BCDEF: %s\n", ft_strnstr("ABCDEF", "BC", 3));
	printf("ft_strnstr - supposed to be (null): %s\n", ft_strnstr("ABCDEF", "BC", 2));

	printf("ft_atoi: ");
	printf("%d", 2147483647 == ft_atoi("2147483647"));
	printf("%d", -2147483648 == ft_atoi("-2147483648"));
	printf("%d", 123 == ft_atoi("  \n\t+123"));
	printf("%d", -123 == ft_atoi("  \n\t-123"));
	printf("%d", 0 == ft_atoi("-0"));
	printf("%d", 123 == ft_atoi("  123abc456"));
	printf("%d", -123 == ft_atoi("  -123abc456"));
	printf("%d", 0 == ft_atoi("  abc456"));
	printf("%d", 456 == ft_atoi("  456  "));
	printf("\n");

	printf("ft_calloc - 3 zeros: ");
	int *p_int = ft_calloc(3, 4);
	for (int i = 0; i < 3; i++)
		printf("%d", *(p_int + i));
	free(p_int);
	printf("\n");

	printf("ft_strdup - supposed to be ABCDEF: ");
	p = ft_strdup("ABCDEF");
	printf("%s\n", p);
	free(p);

	printf("ft_substr - supposed to be CDEF: ");
	p = ft_substr("ABCDEF", 2, 10);
	printf("%s\n", p);
	free(p);
	printf("ft_substr - supposed to be CD: ");
	p = ft_substr("ABCDEF", 2, 2);
	printf("%s\n", p);
	free(p);
	printf("ft_substr - supposed to be ABCDEF: ");
	p = ft_substr("ABCDEF", 0, 6);
	printf("%s\n", p);
	free(p);
	printf("ft_substr - supposed to be ABCDEF: ");
	p = ft_substr("ABCDEF", 0, 10);
	printf("%s\n", p);
	free(p);
	printf("ft_substr - supposed to be BCDEF: ");
	p = ft_substr("ABCDEF", 1, 5);
	printf("%s\n", p);
	free(p);

	printf("ft_strjoin - supposed to be ABCDEF: ");
	p = ft_strjoin("AB", "CDEF");
	printf("%s\n", p);
	free(p);
	printf("ft_strjoin - supposed to be ABCDEF: ");
	p = ft_strjoin("", "ABCDEF");
	printf("%s\n", p);
	free(p);
	printf("ft_strjoin - supposed to be ABCDEF: ");
	p = ft_strjoin("ABCDEF", "");
	printf("%s\n", p);
	free(p);
	printf("ft_strjoin - supposed to be ^$: ");
	p = ft_strjoin("", "");
	printf("^%s$\n", p);
	free(p);

	printf("ft_strtrim - supposed to be BBCCDDEEFF: ");
	p = ft_strtrim("AABBCCAADDEEFF", "A");
	printf("%s\n", p);
	free(p);
	printf("ft_strtrim - supposed to be CCDDEEFF: ");
	p = ft_strtrim("AABBCCAADDEEFF", "AB");
	printf("%s\n", p);
	free(p);
	printf("ft_strtrim - supposed to be AABBCCAADDEEFF: ");
	p = ft_strtrim("AABBCCAADDEEFF", "");
	printf("%s\n", p);
	free(p);
	printf("ft_strtrim - supposed to be ^$: ");
	p = ft_strtrim("AABBCCAADDEEFF", "ABCDEF");
	printf("^%s$\n", p);
	free(p);
	printf("ft_strtrim - supposed to be BDF: ");
	p = ft_strtrim("ABCDEF", "ACE");
	printf("%s\n", p);
	free(p);
	printf("ft_strtrim - supposed to be ^$: ");
	p = ft_strtrim("", "ABC");
	printf("^%s$\n", p);
	free(p);

	printf("ft_itoa - supposed to be 2147483647: ");
	p = ft_itoa(2147483647);
	printf("%s\n", p);
	free(p);
	printf("ft_itoa - supposed to be -2147483648: ");
	p = ft_itoa(-2147483648);
	printf("%s\n", p);
	free(p);
	printf("ft_itoa - supposed to be 7: ");
	p = ft_itoa(7);
	printf("%s\n", p);
	free(p);
	printf("ft_itoa - supposed to be -7: ");
	p = ft_itoa(-7);
	printf("%s\n", p);
	free(p);
	printf("ft_itoa - supposed to be 0: ");
	p = ft_itoa(-0);
	printf("%s\n", p);
	free(p);
	printf("ft_itoa - supposed to be 11: ");
	p = ft_itoa(11);
	printf("%s\n", p);
	free(p);

	printf("ft_split - supposed to be he,o,: ");
	char **splitted = ft_split("hello", 'l');
	print_splitted(splitted);
	printf("ft_split - supposed to be A,: ");
	splitted = ft_split("A", 'l');
	print_splitted(splitted);
	printf("ft_split - supposed to be ABCDE,: ");
	splitted = ft_split("ABCDE", 'Z');
	print_splitted(splitted);
	printf("ft_split - supposed to be (empty): ");
	splitted = ft_split("AAA", 'A');
	print_splitted(splitted);
	printf("ft_split - supposed to be B,: ");
	splitted = ft_split("ABA", 'A');
	print_splitted(splitted);
	printf("ft_split - supposed to be A,A,: ");
	splitted = ft_split("ABA", 'B');
	print_splitted(splitted);
	printf("ft_split - supposed to be (empty): ");
	splitted = ft_split("", 0);
	print_splitted(splitted);
	printf("ft_split - supposed to be hello,world,: ");
	splitted = ft_split("  hello world  ", ' ');
	print_splitted(splitted);

	printf("ft_strmapi - supposed to be hfnos:");
	p = ft_strmapi("hello", upshift);
	printf("%s\n", p);
	free(p);

	printf("ft_striteri - supposed to be 0.H 1.e :");
	p = malloc(3);
	p[0] = 'H';
	p[1] = 'e';
	p[2] = 0;
	ft_striteri(p, print_char);
	printf("\n");
	printf("ft_striteri - supposed to be Hf: ");
	ft_striteri(p, upshift_2);
	printf("%s\n", p);
	free(p);

	printf("<< ft_putchar_fd - for stdout to be H");
	ft_putchar_fd('H', 1);
	printf("\n");

	printf("<< ft_putstr_fd - for stdout to be Hello World");
	ft_putstr_fd("Hello World", 1);
	printf("\n");

	printf("^^ ft_putendl_fd - for stdout to be Bonjour Earth with newline\n");
	ft_putendl_fd("Bonjour Earth", 1);

	printf("<<ft_putnbr_fd supposed to be 2147483647");
	ft_putnbr_fd(2147483647, 1);
	printf("\n");
	printf("<<ft_putnbr_fd supposed to be -2147483648");
	ft_putnbr_fd(-2147483648, 1);
	printf("\n");
	printf("<<ft_putnbr_fd supposed to be 0");
	ft_putnbr_fd(-0, 1);
	printf("\n");
}

void	upshift_2(unsigned int i, char *c)
{
	*c += i;
}

void	print_char(unsigned int i, char *c)
{
	printf("%i.%c ", i, *c);
}

char	upshift(unsigned int i, char c)
{
	return c + i;
}

void	print_char_array(char *arr, int len) {
	int i = 0;
	while (i < len)
		printf("%d", *(arr + i++));
}

void	print_splitted(char **splitted) {
	char	**p;
	p = splitted;
	while (*p)
	{
		printf("%s,", *p);
		free(*p++);
	}
	free(splitted);
	printf("\n");
}
