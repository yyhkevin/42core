/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/06 17:23:31 by keyu              #+#    #+#             */
/*   Updated: 2023/09/20 11:36:36 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int	istrim(char const *set, char c)
{
	while (*set)
	{
		if (*set == c)
			return (1);
		set++;
	}
	return (0);
}

static	void	cpy(char *dest, char *src, int start, int end)
{
	while (start < end)
	{
		*dest++ = *(src + start);
		start++;
	}
}

char	*ft_strtrim(char const *s1, char const *set)
{
	int		start;
	int		end;
	char	*buf;
	char	*p;

	p = (char *)s1;
	start = 0;
	while (*(p + start) && istrim(set, *(p + start)))
		start++;
	end = ft_strlen(s1);
	while (istrim(set, *(p + end - 1)))
		end--;
	if (end - start < 0)
		return (ft_calloc(1, 1));
	buf = malloc((end - start + 1) * sizeof(char));
	if (!buf)
		return (0);
	cpy(buf, (char *)s1, start, end);
	*(buf + end - start) = 0;
	return (buf);
}
