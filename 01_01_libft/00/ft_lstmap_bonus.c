/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/20 15:47:03 by keyu              #+#    #+#             */
/*   Updated: 2023/09/20 16:23:58 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	t_list	*recur(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list	*node;
	t_list	*next;

	node = malloc(sizeof(t_list));
	if (!node)
		return (0);
	node -> content = f(lst -> content);
	node -> next = 0;
	if (!(lst -> next))
		return (node);
	next = recur(lst -> next, f, del);
	if (!next)
	{
		del(node -> content);
		free(node);
		return (0);
	}
	node -> next = next;
	return (node);
}

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	if (!lst)
		return (0);
	return (recur(lst, f, del));
}
