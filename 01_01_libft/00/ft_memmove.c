/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/06 17:23:31 by keyu              #+#    #+#             */
/*   Updated: 2023/09/06 17:28:38 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	void	move_back(char *p_d, char *p_s, size_t len)
{
	while (len > 0)
	{
		*(p_d + len - 1) = *(p_s + len - 1);
		len--;
	}
}

static	void	move_forward(char *p_d, char *p_s, size_t len)
{
	size_t	i;

	i = 0;
	while (i < len)
	{
		*(p_d + i) = *(p_s + i);
		i++;
	}
}

void	*ft_memmove(void *dst, void *src, size_t len)
{
	char	*p_d;
	char	*p_s;

	if (dst == src)
		return (dst);
	p_d = dst;
	p_s = src;
	if (p_s < p_d)
		move_back(p_d, p_s, len);
	else
		move_forward(p_d, p_s, len);
	return (dst);
}
