#include "ClapTrap.hpp"
#include "ScavTrap.hpp"
#include "FragTrap.hpp"

int main(int argc, char **argv) {
    (void) argc;
    (void) argv;

    FragTrap f1("F1");
    FragTrap f2("F2");

    FragTrap f_cp(f1);
    FragTrap f_cp_assign = f_cp;

    f1.stats();
    f2.stats();
    f_cp.stats();
    f_cp_assign.stats();

    f1.highFivesGuys();

    for (int i = 0; i < 101; i++) {
        f1.attack("rando");
    }

    f1.takeDamage(99);
    f1.beRepaired(1);
    f1.takeDamage(1);
    f1.takeDamage(1);
}