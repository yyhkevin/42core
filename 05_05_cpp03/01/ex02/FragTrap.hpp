#pragma once

#include "ClapTrap.hpp"
#include <iostream>

class FragTrap : public ClapTrap {
    protected:
        FragTrap();
    public:
        static int const hp;
        static int const ep;
        static int const ad;
        FragTrap(std::string name);
        FragTrap(FragTrap const &ft);
        FragTrap & operator = (const FragTrap &ft);
        ~FragTrap();
        void highFivesGuys(void);
};