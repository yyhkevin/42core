#include "ClapTrap.hpp"

int main(int argc, char **argv) {
    (void) argc;
    (void) argv;

    ClapTrap c("Clappy");
    ClapTrap t("Trappy");

    ClapTrap c_cp(c);
    ClapTrap c_cp_assign = c_cp;

    c.stats();
    t.stats();
    c_cp.stats();
    c_cp_assign.stats();

    c.attack("Trappy");
    t.takeDamage(5);

    t.attack("Clappy");
    c.takeDamage(9);
    c.beRepaired(1);

    c.attack("Trappy");
    t.takeDamage(5);

    t.attack("Clappy");
    c.takeDamage(1);
    c.beRepaired(1);

    c.beRepaired(1);
    c.takeDamage(1);
}