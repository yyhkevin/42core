#include "ClapTrap.hpp"

int const ClapTrap::hp = 10;
int const ClapTrap::ep = 10;
int const ClapTrap::ad = 0;

ClapTrap::ClapTrap() : _name(""), _hp(ClapTrap::hp), _ep(ClapTrap::ep), _ad(ClapTrap::ad) {
    std::cout << "ClapTrap default constructor called" << std::endl;
}

ClapTrap::ClapTrap(std::string name) : _name(name), _hp(ClapTrap::hp), _ep(ClapTrap::ep), _ad(ClapTrap::ad) {
    std::cout << "ClapTrap name constructor called" << std::endl;
}

ClapTrap::ClapTrap(const ClapTrap &ct) {
    std::cout << "ClapTrap copy constructor called" << std::endl;
    *this = ct;
}

ClapTrap &ClapTrap::operator = (const ClapTrap &ct) {
    std::cout << "ClapTrap copy assignment called" << std::endl;
    if (this != &ct) {
        this->_name = ct._name;
        this->_hp = ct._hp;
        this->_ep = ct._ep;
        this->_ad = ct._ad;
    }
    return *this;
}

ClapTrap::~ClapTrap() {
    std::cout << "ClapTrap " << _name << " destructor called" << std::endl;
}

bool ClapTrap::_validate(std::string const &action) {
    if (_hp <= 0)
    {
        std::cout << "ClapTrap " << _name << " has no HP left for " << action << "!" << std::endl;
        return false;
    }
    if (_ep <= 0)
    {
        std::cout << "ClapTrap " << _name << " has no EP left for " << action << "!" << std::endl;
        return false;
    }
    return true;
}

void ClapTrap::attack(const std::string& target) {
    if (!_validate("attack"))
        return;
    std::cout << "ClapTrap " << _name << " attacks " << target << ", causing " << _ad << " points of damage!" << std::endl;
    _ep--;
}

void ClapTrap::takeDamage(unsigned int amount) {
    if (_hp <= 0)
    {
        std::cout << "ClapTrap " << _name << " already has 0 HP!" << std::endl;
        return;
    }
    if (amount >= (unsigned int) _hp) {
        std::cout << "ClapTrap " << _name << " takes " << amount << " points of damage and dies!" << std::endl;
        _hp = 0;
        return;
    }
    _hp -= amount;
    std::cout << "ClapTrap " << _name << " takes " << amount << " points of damage" << std::endl;
}

void ClapTrap::beRepaired(unsigned int amount) {
    if (!_validate("repair"))
        return;
    std::cout << "ClapTrap " << _name << " recovers " << amount << " health points!" << std::endl;
    _hp += amount;
    _ep--;
}

void ClapTrap::stats(void) {
    std::cout << "HP: " << this->_hp << std::endl;
    std::cout << "EP: " << this->_ep << std::endl;
    std::cout << "AD: " << this->_ad << std::endl;
    std::cout << "Name: " << this->_name << std::endl;
}