#include "ClapTrap.hpp"
#include "ScavTrap.hpp"

int main(int argc, char **argv) {
    (void) argc;
    (void) argv;

    ScavTrap st1("Firsty");
    ScavTrap st2("Secondy");

    st1.guardGate();

    for (int i = 0; i < 51; i++)
    {
        st1.attack("Secondy");
        st2.takeDamage(20);
    }
    st2.beRepaired(500);
}