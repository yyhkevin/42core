#include "ScavTrap.hpp"

ScavTrap::ScavTrap() {
    std::cout << "ScavTrap default constructor called" << std::endl;
}

ScavTrap::ScavTrap(std::string name) : ClapTrap(name) {
    std::cout << "ScavTrap name constructor called" << std::endl;
    this->_hp = 100;
    this->_ep = 50;
    this->_ad = 20;
}

ScavTrap::ScavTrap(const ScavTrap &st) {
    std::cout << "ScavTrap copy constructor called" << std::endl;
    *this = st;
}

ScavTrap & ScavTrap::operator = (const ScavTrap &st) {
    std::cout << "ScavTrap copy assignment called" << std::endl;
    if (this != &st) {
        this->_hp = st._hp;
        this->_ep = st._ep;
        this->_ad = st._ad;
        this->_name = st._name;
    }
    return *this;
}

ScavTrap::~ScavTrap() {
    std::cout << "ScavTrap " << _name << " destructor called" << std::endl;
}

void ScavTrap::attack(const std::string& target) {
    if (!_validate("attack"))
        return;
    std::cout << "ScavTrap " << _name << " attacks " << target << ", causing " << _ad << " points of damage!" << std::endl;
    _ep--;
}

void ScavTrap::guardGate() {
    std::cout << "ScavTrap " << _name << " in Gate Keeper mode" << std::endl;
}