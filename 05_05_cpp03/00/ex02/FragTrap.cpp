#include "FragTrap.hpp"

FragTrap::FragTrap() {
    std::cout << "FragTrap default constructor called" << std::endl;
}

FragTrap::FragTrap(std::string name) {
    std::cout << "FragTrap name constructor called" << std::endl;
    this->_name = name;
    this->_hp = 100;
    this->_ep = 100;
    this->_ad = 30;
}

FragTrap::FragTrap(FragTrap const &ft) {
    std::cout << "FragTrap copy constructor called" << std::endl;
    *this = ft;
}

FragTrap & FragTrap::operator = (const FragTrap &ft) {
    std::cout << "FragTrap copy assignment called" << std::endl;
    if (this != &ft) {
        this->_hp = ft._hp;
        this->_ep = ft._ep;
        this->_ad = ft._ad;
        this->_name = ft._name;
    }
    return *this;
}

FragTrap::~FragTrap() {
    std::cout << "FragTrap " << this->_name << " destructor called" << std::endl;
}

void FragTrap::highFivesGuys(void) {
    std::cout << "FragTrap " << this->_name << " HIGH FIVES EVERYONE~~~" << std::endl;
}
