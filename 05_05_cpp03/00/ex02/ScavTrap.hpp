#pragma once

#include <iostream>
#include "ClapTrap.hpp"

class ScavTrap : public ClapTrap
{
    protected:
        ScavTrap();
    public:
        ScavTrap(std::string name);
        ScavTrap(const ScavTrap &st);
        ScavTrap & operator = (const ScavTrap &st);
        ~ScavTrap();
        void attack(const std::string& target);
        void guardGate();
};