#pragma once

#include "ClapTrap.hpp"
#include <iostream>

class FragTrap : public ClapTrap {
    protected:
        FragTrap();
    public:
        FragTrap(std::string name);
        FragTrap(FragTrap const &ft);
        FragTrap & operator = (const FragTrap &ft);
        ~FragTrap();
        void highFivesGuys(void);
};