#include "ClapTrap.hpp"

int main(int argc, char **argv) {
    (void) argc;
    (void) argv;

    ClapTrap c("Clappy");
    ClapTrap t("Trappy");

    c.attack("Trappy");
    t.takeDamage(5);

    t.attack("Clappy");
    c.takeDamage(9);
    c.beRepaired(1);

    c.attack("Trappy");
    t.takeDamage(5);

    t.attack("Clappy");
    c.takeDamage(1);
    c.beRepaired(1);

    c.beRepaired(4294967295);

    c.takeDamage(4294967295);
    // c.takeDamage(4294967295);
}