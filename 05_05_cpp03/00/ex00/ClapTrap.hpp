#pragma once

#include <iostream>

class ClapTrap
{
protected:
    bool _validate(std::string const &action);
    std::string _name;
    int _hp;
    int _ep;
    int _ad;
    ClapTrap();
public:
    ClapTrap(std::string name);
    ClapTrap(const ClapTrap &ct);
    ClapTrap &operator = (const ClapTrap &ct);
    ~ClapTrap();
    void attack(const std::string& target);
    void takeDamage(unsigned int amount);
    void beRepaired(unsigned int amount);
};