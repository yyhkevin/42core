#include "Bureaucrat.hpp"

int main(void)
{
    Bureaucrat mike("Mike", 100);
    std::cout << mike << std::endl;
    mike.incrementGrade();
    std::cout << "Promoted: " << mike << std::endl;
    mike.decrementGrade();
    std::cout << "Demoted: " << mike << std::endl;

    try
    {
        Bureaucrat john("John", 151);
    }
    catch (std::exception &e)
    {
        std::cout << "Exception in instantializing john: " << e.what() << std::endl;
    }

    Bureaucrat john("John", 150);
    std::cout << john << std::endl;
    try
    {
        john.decrementGrade();
    }
    catch (std::exception &e)
    {
        std::cout << "Exception in decrementing grade for john: " << e.what() << std::endl;
    }

    try
    {
        Bureaucrat betty("Betty", 0);
    }
    catch (std::exception &e)
    {
        std::cout << "Exception in instantializing betty: " << e.what() << std::endl;
    }
    Bureaucrat betty("Betty", 1);
    std::cout << betty << std::endl;
    try
    {
        betty.incrementGrade();
    }
    catch (std::exception &e)
    {
        std::cout << "Exception in incrementing grade for betty: " << e.what() << std::endl;
    }
}