#include "Form.hpp"

Form::Form() : _name("default"), _signed(false), _grade_sign(1), _grade_execute(1)
{
    std::cout << "Form default constructor called" << std::endl;
}

Form::Form(std::string name, int grade_sign, int grade_execute) : _name(name), _signed(false), _grade_sign(grade_sign), _grade_execute(grade_execute)
{
    std::cout << "Form name/req constructor called" << std::endl;
}

Form::Form(Form const &f) : _name(f._name), _grade_sign(f._grade_sign), _grade_execute(f._grade_execute)
{
    std::cout << "Form copy constructor called" << std::endl;
    *this = f;
}

Form & Form::operator = (Form const &f)
{
    std::cout << "Form copy assignment operator called" << std::endl;
    if (this == &f)
        return *this;
    this->_signed = f._signed;
    return *this;
}

Form::~Form()
{
    std::cout << this->_name << ": Form destructor called" << std::endl;
}

std::string const Form::getName() const 
{
    return this->_name;
}

bool Form::getSigned() const 
{
    return this->_signed;
}

int Form::getGradeToSign() const 
{
    return this->_grade_sign;
}

int Form::getGradeToExecute() const 
{
    return this->_grade_execute;
}

void Form::beSigned(Bureaucrat &b)
{
    if (this->_grade_sign >= b.getGrade())
    {
        this->_signed = true;
        std::cout << b.getName() << " signed form: " << this->_name << std::endl;
    }
    else
        throw Form::GradeTooLowException();
}

const char* Form::GradeTooHighException::what() const throw()
{
    return "Grade too High";
}

const char* Form::GradeTooLowException::what() const throw()
{
    return "Grade too Low";
}

std::ostream & operator<<(std::ostream &o, Form const &f) {
    o << "[" << f.getName() << "] ";
    o << "signed: " << f.getSigned() << ", ";
    o << "grade to sign: " << f.getGradeToSign() << ", ";
    o << "grade to execute: " << f.getGradeToExecute();
    return o;
}