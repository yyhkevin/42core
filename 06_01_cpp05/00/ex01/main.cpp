#include "Bureaucrat.hpp"
#include "Form.hpp"

int main(void)
{
    Form g3f("Grade 3 Form", 3, 1);
    std::cout << g3f << std::endl;
    Form g3f_clone = g3f;
    std::cout << "Clone: " << g3f_clone << std::endl;

    Bureaucrat g3b("Grade 3 Bureacrat", 3);
    g3f.beSigned(g3b);
    std::cout << g3f << std::endl;

    Bureaucrat g4b("Grade 4 Bureacrat", 4);
    try
    {
        g3f.beSigned(g4b);
    }
    catch (std::exception &e)
    {
        std::cout << "Failed to sign because: " << e.what() << std::endl;
    }

    Bureaucrat g2b("Grade 2 Bureacrat", 2);
    Form g1f("Grade 1 Form", 1, 1);
    g2b.signForm(g1f);
    std::cout << g1f << std::endl;
}