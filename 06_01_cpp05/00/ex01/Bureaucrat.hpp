#pragma once

#include <iostream>
#include "Form.hpp"

#define GRADE_HIGHEST 1
#define GRADE_LOWEST 150

class Form;

class Bureaucrat
{
    private:
        Bureaucrat();
        std::string const _name;
        int _grade;
        void _setGrade(int grade);
    public:
        Bureaucrat(std::string name, int grade);
        Bureaucrat(Bureaucrat const &b);
        Bureaucrat & operator = (Bureaucrat const &b);
        ~Bureaucrat();
        int getGrade() const;
        std::string const getName() const;
        void incrementGrade(); // decrease _grade
        void decrementGrade(); // increase _grade

        void signForm(Form &f);

        class GradeTooHighException : public std::exception {
            virtual const char* what() const throw();
        };
        class GradeTooLowException : public std::exception {
            virtual const char* what() const throw();
        };
};

std::ostream & operator<<(std::ostream &o, Bureaucrat const &b);