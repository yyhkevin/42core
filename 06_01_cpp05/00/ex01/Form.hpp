#pragma once

#include <iostream>
#include "Bureaucrat.hpp"

// for circular dependency headers
class Bureaucrat;

class Form
{
    private:
        std::string const _name;
        bool _signed;
        int const _grade_sign;
        int const _grade_execute;
        Form();
    public:
        Form(std::string name, int grade_sign, int grade_execute);
        Form(Form const &f);
        Form & operator = (Form const &f);
        ~Form();

        std::string const getName() const;
        bool getSigned() const;
        int getGradeToSign() const;
        int getGradeToExecute() const;

        void beSigned(Bureaucrat &b);

        class GradeTooHighException : public std::exception {
            virtual const char* what() const throw();
        };
        class GradeTooLowException : public std::exception {
            virtual const char* what() const throw();
        };
};

std::ostream & operator<<(std::ostream &o, Form const &f);