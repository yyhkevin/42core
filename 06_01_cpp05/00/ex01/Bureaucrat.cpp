#include "Bureaucrat.hpp"
#include "Form.hpp"

Bureaucrat::Bureaucrat() : _name("default"), _grade(150)
{
    std::cout << "Bureaucrat default constructor called" << std::endl;
}

void Bureaucrat::_setGrade(int grade)
{
    if (grade > GRADE_LOWEST)
        throw Bureaucrat::GradeTooLowException();
    else if (grade < GRADE_HIGHEST)
        throw Bureaucrat::GradeTooHighException();
    this->_grade = grade;
}

Bureaucrat::Bureaucrat(std::string name, int grade) : _name(name)
{
    std::cout << "Bureaucrat name/grade constructor called" << std::endl;
    this->_setGrade(grade);
}

Bureaucrat::Bureaucrat(Bureaucrat const &b) : _name(b._name)
{
    std::cout << "Bureaucrat copy constructor called" << std::endl;
    *this = b;
}

Bureaucrat & Bureaucrat::operator = (Bureaucrat const &b)
{
    std::cout << "Bureaucrat copy assignment operator called" << std::endl;
    if (this == &b)
        return *this;
    this->_setGrade(b._grade);
    return *this;
}

Bureaucrat::~Bureaucrat()
{
    std::cout << this->_name << ": Bureaucrat destructor called" << std::endl;
}

int Bureaucrat::getGrade() const
{
    return this->_grade;
}

std::string const Bureaucrat::getName() const
{
    return this->_name;
}

void Bureaucrat::incrementGrade()
{
    this->_setGrade(this->_grade - 1);
}

void Bureaucrat::decrementGrade()
{
    this->_setGrade(this->_grade + 1);
}

void Bureaucrat::signForm(Form &f)
{
    try
    {
        f.beSigned(*this);
    }
    catch (std::exception &e)
    {
        std::cout << this->_name << " couldn't sign form because of: " << e.what() << std::endl;
    }
}

const char* Bureaucrat::GradeTooHighException::what() const throw()
{
    return "Grade too High";
}

const char* Bureaucrat::GradeTooLowException::what() const throw()
{
    return "Grade too Low";
}

std::ostream & operator<<(std::ostream &o, Bureaucrat const &b) {
    o << b.getName() << ", bureaucrat grade " << b.getGrade() << ".";
    return o;
}