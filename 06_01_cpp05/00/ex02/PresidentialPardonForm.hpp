#pragma once

#include <iostream>
#include "AForm.hpp"

class PresidentialPardonForm : public AForm
{
    private:
        std::string const _target;
        static int const GRADE_TO_SIGN;
        static int const GRADE_TO_EXEC;
        PresidentialPardonForm();
    public:
        PresidentialPardonForm(std::string target);
        PresidentialPardonForm(PresidentialPardonForm const &scf);
        PresidentialPardonForm & operator = (PresidentialPardonForm const &scf);
        ~PresidentialPardonForm();
        void execute(Bureaucrat const & executor) const;
};