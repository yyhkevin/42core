#pragma once

#include <iostream>
#include "AForm.hpp"
#include <cstdlib>

class RobotomyRequestForm : public AForm
{
    private:
        std::string const _target;
        static int const GRADE_TO_SIGN;
        static int const GRADE_TO_EXEC;
        RobotomyRequestForm();
    public:
        RobotomyRequestForm(std::string target);
        RobotomyRequestForm(RobotomyRequestForm const &scf);
        RobotomyRequestForm & operator = (RobotomyRequestForm const &scf);
        ~RobotomyRequestForm();
        void execute(Bureaucrat const & executor) const;
};