#include "PresidentialPardonForm.hpp"

int const PresidentialPardonForm::GRADE_TO_SIGN = 25;
int const PresidentialPardonForm::GRADE_TO_EXEC = 5;

PresidentialPardonForm::PresidentialPardonForm() : AForm("PresidentialPardonForm", PresidentialPardonForm::GRADE_TO_SIGN, PresidentialPardonForm::GRADE_TO_EXEC), _target("default target")
{
    std::cout << "PresidentialPardonForm default constructor called" << std::endl;
}

PresidentialPardonForm::PresidentialPardonForm(std::string target) : AForm("PresidentialPardonForm", PresidentialPardonForm::GRADE_TO_SIGN, PresidentialPardonForm::GRADE_TO_EXEC), _target(target)
{
    std::cout << "PresidentialPardonForm target constructor called" << std::endl;
}

PresidentialPardonForm::PresidentialPardonForm(PresidentialPardonForm const &scf) : AForm("PresidentialPardonForm", PresidentialPardonForm::GRADE_TO_SIGN, PresidentialPardonForm::GRADE_TO_EXEC), _target(scf._target)
{
    std::cout << "PresidentialPardonForm copy constructor called" << std::endl;
    *this = scf;
}

PresidentialPardonForm & PresidentialPardonForm::operator = (PresidentialPardonForm const &scf)
{
    if (this == &scf)
        return *this;
    return *this;
}

PresidentialPardonForm::~PresidentialPardonForm()
{
    std::cout << "PresidentialPardonForm destructor called" << std::endl;
}

void PresidentialPardonForm::execute(Bureaucrat const & executor) const
{
    if (this->checkIfExecutable(executor))
    {
      std::cout << this->_target << " has been pardoned by Zaphod Beeblebrox." << std::endl;
    }
}