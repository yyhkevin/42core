#include "RobotomyRequestForm.hpp"

int const RobotomyRequestForm::GRADE_TO_SIGN = 72;
int const RobotomyRequestForm::GRADE_TO_EXEC = 45;

RobotomyRequestForm::RobotomyRequestForm() : AForm("RobotomyRequestForm", RobotomyRequestForm::GRADE_TO_SIGN, RobotomyRequestForm::GRADE_TO_EXEC), _target("default target")
{
    std::cout << "RobotomyRequestForm default constructor called" << std::endl;
}

RobotomyRequestForm::RobotomyRequestForm(std::string target) : AForm("RobotomyRequestForm", RobotomyRequestForm::GRADE_TO_SIGN, RobotomyRequestForm::GRADE_TO_EXEC), _target(target)
{
    std::cout << "RobotomyRequestForm target constructor called" << std::endl;
}

RobotomyRequestForm::RobotomyRequestForm(RobotomyRequestForm const &scf) : AForm("RobotomyRequestForm", RobotomyRequestForm::GRADE_TO_SIGN, RobotomyRequestForm::GRADE_TO_EXEC), _target(scf._target)
{
    std::cout << "RobotomyRequestForm copy constructor called" << std::endl;
    *this = scf;
}

RobotomyRequestForm & RobotomyRequestForm::operator = (RobotomyRequestForm const &scf)
{
    if (this == &scf)
        return *this;
    return *this;
}

RobotomyRequestForm::~RobotomyRequestForm()
{
    std::cout << "RobotomyRequestForm destructor called" << std::endl;
}

void RobotomyRequestForm::execute(Bureaucrat const & executor) const
{
    if (this->checkIfExecutable(executor))
    {
        srand(time(0));
        int randomNum = rand() % 2;
        if (randomNum)
            std::cout << "Robotomy successful for " << this->_target << "!" << std::endl;
        else
            std::cout << "Robotomy failed for " << this->_target << "." << std::endl;
    }
}