#include "AForm.hpp"

AForm::AForm() : _name("default"), _signed(false), _grade_sign(1), _grade_execute(1)
{
    std::cout << "AForm default constructor called" << std::endl;
}

AForm::AForm(std::string name, int grade_sign, int grade_execute) : _name(name), _signed(false), _grade_sign(grade_sign), _grade_execute(grade_execute)
{
    std::cout << "AForm name/req constructor called" << std::endl;
}

AForm::AForm(AForm const &f) : _name(f._name), _grade_sign(f._grade_sign), _grade_execute(f._grade_execute)
{
    std::cout << "AForm copy constructor called" << std::endl;
    *this = f;
}

AForm & AForm::operator = (AForm const &f)
{
    std::cout << "AForm copy assignment operator called" << std::endl;
    if (this == &f)
        return *this;
    this->_signed = f._signed;
    return *this;
}

AForm::~AForm()
{
    std::cout << this->_name << ": AForm destructor called" << std::endl;
}

std::string const AForm::getName() const 
{
    return this->_name;
}

bool AForm::getSigned() const 
{
    return this->_signed;
}

int AForm::getGradeToSign() const 
{
    return this->_grade_sign;
}

int AForm::getGradeToExecute() const 
{
    return this->_grade_execute;
}

void AForm::beSigned(Bureaucrat &b)
{
    if (this->_grade_sign >= b.getGrade())
    {
        this->_signed = true;
        std::cout << b.getName() << " signed AForm: " << this->_name << std::endl;
    }
    else
        throw AForm::GradeTooLowException();
}

bool AForm::checkIfExecutable(Bureaucrat const &executor) const
{
    if (!(this->_signed))
        throw AForm::FormNotSignedException();
    if (this->_grade_execute < executor.getGrade())
        throw AForm::GradeTooLowException();
    return true;
}

const char* AForm::GradeTooHighException::what() const throw()
{
    return "Grade too High";
}

const char* AForm::GradeTooLowException::what() const throw()
{
    return "Grade too Low";
}

const char* AForm::FormNotSignedException::what() const throw()
{
    return "Form not Signed";
}

std::ostream & operator<<(std::ostream &o, AForm const &f) {
    o << "[" << f.getName() << "] ";
    o << "signed: " << f.getSigned() << ", ";
    o << "grade to sign: " << f.getGradeToSign() << ", ";
    o << "grade to execute: " << f.getGradeToExecute();
    return o;
}