#include "ShrubberyCreationForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"

void pseparator()
{
    std::cout << "---------------" << std::endl;
}

void pseparator(std::string header)
{
    std::cout << "---------------" << header << std::endl;
}

int main(void)
{
    Bureaucrat b1("Grade 1 Bureaucrat", 1);
    Bureaucrat b150("Grade 150 Bureaucrat", 150);

    {
        pseparator();
        ShrubberyCreationForm scf("home");
        scf.beSigned(b1);
        scf.execute(b1);

        pseparator();
        PresidentialPardonForm ppf("Some Person");
        ppf.beSigned(b1);
        ppf.execute(b1);

        pseparator();
        RobotomyRequestForm rrf("VaccumBot");
        rrf.beSigned(b1);
        rrf.execute(b1);

        pseparator();
    }

    pseparator("Not signed yet");
    {
        pseparator();
        ShrubberyCreationForm scf("home");
        try {
            scf.execute(b1);
        } catch(std::exception &e) {
            std::cout << "Failed to execute ShrubberyCreationForm because: " << e.what() << std::endl;
        }

        pseparator();
        PresidentialPardonForm ppf("Some Person");
        try {
            ppf.execute(b1);
        } catch(std::exception &e) {
            std::cout << "Failed to execute PresidentialPardonForm because: " << e.what() << std::endl;
        }

        pseparator();
        RobotomyRequestForm rrf("VacuumBot");
        try {
            rrf.execute(b1);
        } catch(std::exception &e) {
            std::cout << "Failed to execute RobotomyRequestForm because: " << e.what() << std::endl;
        }

        pseparator();
    }

    pseparator("Grade too low");
    {
        pseparator();
        ShrubberyCreationForm scf("home");
        try {
            scf.beSigned(b150);
        } catch(std::exception &e) {
            std::cout << "Failed to sign ShrubberyCreationForm because: " << e.what() << std::endl;
        }

        pseparator();
        PresidentialPardonForm ppf("Some Person");
        try {
            ppf.beSigned(b150);
        } catch(std::exception &e) {
            std::cout << "Failed to sign PresidentialPardonForm because: " << e.what() << std::endl;
        }

        pseparator();
        RobotomyRequestForm rrf("VacuumBot");
        try {
            rrf.beSigned(b150);
        } catch(std::exception &e) {
            std::cout << "Failed to sign RobotomyRequestForm because: " << e.what() << std::endl;
        }

        pseparator();
    }

    pseparator("Bureaucrat::executeForm");
    {
        pseparator();
        ShrubberyCreationForm scf("home");
        b1.executeForm(scf);
        b1.signForm(scf);
        b1.executeForm(scf);

        pseparator();
        PresidentialPardonForm ppf("Some Person");
        b1.signForm(ppf);
        b150.executeForm(ppf);

        pseparator();
        RobotomyRequestForm rrf("VacuumBot");
        b150.executeForm(rrf);
        b1.signForm(rrf);
        b150.executeForm(rrf);
        b1.executeForm(rrf);

        pseparator();
    }
}