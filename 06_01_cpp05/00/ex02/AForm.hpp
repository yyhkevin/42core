#pragma once

#include <iostream>
#include "Bureaucrat.hpp"

// for circular dependency headers
class Bureaucrat;

class AForm
{
    private:
        std::string const _name;
        bool _signed;
        int const _grade_sign;
        int const _grade_execute;
        AForm();
    public:
        AForm(std::string name, int grade_sign, int grade_execute);
        AForm(AForm const &f);
        AForm & operator = (AForm const &f);
        ~AForm();

        std::string const getName() const;
        bool getSigned() const;
        int getGradeToSign() const;
        int getGradeToExecute() const;

        void beSigned(Bureaucrat &b);

        virtual void execute(Bureaucrat const & executor) const = 0;

        bool checkIfExecutable(Bureaucrat const &executor) const;

        class GradeTooHighException : public std::exception {
            virtual const char* what() const throw();
        };
        class GradeTooLowException : public std::exception {
            virtual const char* what() const throw();
        };
        class FormNotSignedException : public std::exception {
            virtual const char* what() const throw();
        };
};

std::ostream & operator<<(std::ostream &o, AForm const &f);