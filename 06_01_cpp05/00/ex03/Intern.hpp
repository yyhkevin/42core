#pragma once

#include <iostream>
#include "ShrubberyCreationForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"

#define REQUEST_COUNT 3
#define REQUEST_SHRUBBERY "shrubbery"
#define REQUEST_ROBOTOMY "robotomy"
#define REQUEST_PARDON "pardon"

class Intern
{
    private:
        static AForm *makeShrubbery(std::string target);
        static AForm *makeRobotomy(std::string target);
        static AForm *makePardon(std::string target);
    public:
        Intern();
        Intern(Intern const &i);
        Intern & operator = (Intern const &i);
        ~Intern();
        AForm *makeForm(std::string request, std::string target);
};