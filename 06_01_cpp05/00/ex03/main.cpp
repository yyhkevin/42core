#include "Intern.hpp"

void pseparator()
{
    std::cout << "---------------" << std::endl;
}

void pseparator(std::string header)
{
    std::cout << "---------------" << header << std::endl;
}

int main(void)
{
    Intern rando;

    {
        pseparator();
        AForm *f1 = rando.makeForm(REQUEST_SHRUBBERY, "newHome");
        std::cout << *f1 << std::endl;

        pseparator();
        AForm *f2 = rando.makeForm(REQUEST_ROBOTOMY, "newVaccum");
        std::cout << *f2 << std::endl;

        pseparator();
        AForm *f3 = rando.makeForm(REQUEST_PARDON, "newPerson");
        std::cout << *f3 << std::endl;

        pseparator();
        AForm *invalid = rando.makeForm("holiday", "z");
        if (!invalid)
            std::cout << "Form not created as invalid request" << std::endl;

        pseparator();
        delete f1;
        delete f2;
        delete f3;
    }
}