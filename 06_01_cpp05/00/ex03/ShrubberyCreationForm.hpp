#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include "AForm.hpp"

class ShrubberyCreationForm : public AForm
{
    private:
        std::string const _target;
        static int const GRADE_TO_SIGN;
        static int const GRADE_TO_EXEC;
        ShrubberyCreationForm();
    public:
        ShrubberyCreationForm(std::string target);
        ShrubberyCreationForm(ShrubberyCreationForm const &scf);
        ShrubberyCreationForm & operator = (ShrubberyCreationForm const &scf);
        ~ShrubberyCreationForm();
        void execute(Bureaucrat const & executor) const;
};