#include "ShrubberyCreationForm.hpp"

int const ShrubberyCreationForm::GRADE_TO_SIGN = 145;
int const ShrubberyCreationForm::GRADE_TO_EXEC = 137;

ShrubberyCreationForm::ShrubberyCreationForm() : AForm("ShrubberyCreationForm", ShrubberyCreationForm::GRADE_TO_SIGN, ShrubberyCreationForm::GRADE_TO_EXEC), _target("default target")
{
    std::cout << "ShrubberyCreationForm default constructor called" << std::endl;
}

ShrubberyCreationForm::ShrubberyCreationForm(std::string target) : AForm("ShrubberyCreationForm", ShrubberyCreationForm::GRADE_TO_SIGN, ShrubberyCreationForm::GRADE_TO_EXEC), _target(target)
{
    std::cout << "ShrubberyCreationForm target constructor called" << std::endl;
}

ShrubberyCreationForm::ShrubberyCreationForm(ShrubberyCreationForm const &scf) : AForm("ShrubberyCreationForm", ShrubberyCreationForm::GRADE_TO_SIGN, ShrubberyCreationForm::GRADE_TO_EXEC), _target(scf._target)
{
    std::cout << "ShrubberyCreationForm copy constructor called" << std::endl;
    *this = scf;
}

ShrubberyCreationForm & ShrubberyCreationForm::operator = (ShrubberyCreationForm const &scf)
{
    if (this == &scf)
        return *this;
    return *this;
}

ShrubberyCreationForm::~ShrubberyCreationForm()
{
    std::cout << "ShrubberyCreationForm destructor called" << std::endl;
}

void ShrubberyCreationForm::execute(Bureaucrat const & executor) const
{
    if (this->checkIfExecutable(executor))
    {
        std::string name = this->_target + "_shrubbery";
        std::ofstream outfile(name.c_str());

        for (int i = 0; i < 5; i++)
        {
        outfile 
            <<
"       _-_\n"
"    /~~   ~~\\\n"
" /~~         ~~\\\n"
"{               }\n"
" \\  _-     -_  /\n"
"   ~  \\ //  ~\n"
"_- -   | | _- _\n"
"  _ -  | |   -_\n"
"      // \\\n\n" << std::endl;
        }

        outfile.close();
    }
}