#include "Intern.hpp"

Intern::Intern()
{
    std::cout << "Intern constructor called" << std::endl;
}

Intern::Intern(Intern const &i)
{
    std::cout << "Intern copy constructor called" << std::endl;
    *this = i;
}

Intern & Intern::operator = (Intern const &i)
{
    std::cout << "Intern copy assignment operator called" << std::endl;
    if (this == &i)
        return *this;
    return *this;
}

Intern::~Intern()
{
    std::cout << "Intern destructor called" << std::endl;
}

AForm *Intern::makeShrubbery(std::string target)
{
    return new ShrubberyCreationForm(target);
}

AForm *Intern::makeRobotomy(std::string target)
{
    return new RobotomyRequestForm(target);
}

AForm *Intern::makePardon(std::string target)
{
    return new PresidentialPardonForm(target);
}

AForm *Intern::makeForm(std::string request, std::string target)
{
    AForm *(*allForms[])(const std::string target) = {&makeShrubbery, &makeRobotomy, &makePardon};
    std::string requestTypes[] = { REQUEST_SHRUBBERY, REQUEST_ROBOTOMY, REQUEST_PARDON };

    for (int i = 0; i < REQUEST_COUNT; i++)
    {
        if (requestTypes[i] == request)
            return allForms[i](target);
    }

    return 0;
}