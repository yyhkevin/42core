/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/05 12:48:09 by keyu              #+#    #+#             */
/*   Updated: 2023/10/12 16:37:14 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static char	*ft_append_str(char *dst, char *src, int bytes)
{
	char	*temp;

	if (!dst)
	{
		dst = ft_calloc(BUFFER_SIZE + 1, sizeof(char));
		if (!dst)
			return (0);
	}
	else
	{
		temp = dst;
		dst = ft_calloc(ft_strlen(dst) + BUFFER_SIZE + 1, sizeof(char));
		if (!dst)
		{
			free(temp);
			return (0);
		}
		ft_strlcat(dst, temp, ft_strlen(temp) + BUFFER_SIZE + 1);
		free(temp);
	}
	ft_strlcat(dst, src, ft_strlen(dst) + bytes + 1);
	return (dst);
}

static char	*ft_find_nl(char *buf, struct s_pos *pos, char **line, int b_r)
{
	int	index_of_nl;

	index_of_nl = ft_strchr(buf + pos -> pos, '\n');
	if (index_of_nl != -1)
	{
		*line = ft_append_str(*line, buf + pos -> pos, index_of_nl + 1);
		pos -> pos = index_of_nl + pos -> pos + 1;
		return (*line);
	}
	else
	{
		*line = ft_append_str(*line, buf + pos -> pos, b_r);
		pos -> pos = BUFFER_SIZE;
		return (0);
	}
}

static void	ft_setup_stuff(char *buf, struct s_pos *pos, int bytes_read)
{
	buf[bytes_read] = 0;
	pos -> pos = 0;
	ft_cpy(pos -> buf, buf, bytes_read);
}

static char	*ft_read_till_nl(int fd, char *buf, char *line, int *bytes_read)
{
	static struct s_pos	pos;
	char				*tmp;

	while (*bytes_read > 0)
	{
		ft_cpy(buf, pos.buf, BUFFER_SIZE);
		if (pos.pos == BUFFER_SIZE || pos.buf[pos.pos] == 0)
		{
			*bytes_read = read(fd, buf, BUFFER_SIZE);
			if (*bytes_read == -1)
			{
				if (!line)
					free(line);
				return (0);
			}
			if (*bytes_read == 0)
				return (line);
			ft_setup_stuff(buf, &pos, *bytes_read);
		}
		tmp = ft_find_nl(buf, &pos, &line, *bytes_read);
		if (tmp)
			return (tmp);
	}
	return (line);
}

char	*get_next_line(int fd)
{
	char	*line;
	char	*buf;
	int		bytes_read;

	line = 0;
	bytes_read = BUFFER_SIZE;
	if (fd < 0 || BUFFER_SIZE <= 0)
		return (0);
	buf = malloc(BUFFER_SIZE + 1);
	line = ft_read_till_nl(fd, buf, line, &bytes_read);
	free(buf);
	return (line);
}
