#!/bin/bash

# Compile the C code with different BUFFER_SIZE values
for BUFFER_SIZE in {1..150}; do
    echo "Compiling with BUFFER_SIZE=${BUFFER_SIZE}..."
    
    # Run the program with valgrind and redirect the output to a file
    valgrind -s ./a.out read_error.txt > valgrind_output_${BUFFER_SIZE}.txt 2>&1
    
    # Check for memory leaks using valgrind
    if grep -q "no leaks are possible" "valgrind_output_${BUFFER_SIZE}.txt"; then
        echo "No memory leaks for BUFFER_SIZE=${BUFFER_SIZE}."
    else
        echo "Memory leaks detected for BUFFER_SIZE=${BUFFER_SIZE}."
        exit 1
    fi

    # Run the program without valgrind and redirect the output to a file
    ./a.out read_error.txt > output_${BUFFER_SIZE}.txt
done

# Check if all the outputs are the same at the binary level
first_output="output_1.txt"
for BUFFER_SIZE in {2..150}; do
    current_output="output_${BUFFER_SIZE}.txt"

    # Compare files at the binary level
    if cmp -s "$first_output" "$current_output"; then
        echo "Outputs are the same for BUFFER_SIZE=${BUFFER_SIZE}."
    else
        echo "Outputs are different for BUFFER_SIZE=${BUFFER_SIZE}."
        exit 1
    fi
done

echo "All outputs are the same at the binary level, and no memory leaks detected."

