/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/05 14:37:42 by keyu              #+#    #+#             */
/*   Updated: 2023/10/12 16:03:11 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

void	*ft_calloc(size_t count, size_t size)
{
	void	*buf;
	size_t	i;

	buf = malloc(count * size);
	if (!buf)
		return (0);
	i = 0;
	while (i < count * size)
		*((char *)buf + i++) = 0;
	return (buf);
}

size_t	ft_strlen(const char *s)
{
	int	len;

	len = 0;
	while (*s)
	{
		len++;
		s++;
	}
	return (len);
}

size_t	ft_strlcat(char *dst, const char *src, size_t dst_size)
{
	size_t	dstlen;
	size_t	srclen;

	dstlen = ft_strlen(dst);
	srclen = ft_strlen(src);
	if (dstlen > dst_size)
		dstlen = dst_size;
	if (!dst_size || dstlen >= dst_size)
		return (dstlen + srclen);
	dst += dstlen;
	dst_size = dst_size - dstlen - 1;
	while (dst_size > 0 && *src)
	{
		*dst++ = *src++;
		dst_size--;
	}
	*dst = 0;
	return (dstlen + srclen);
}

int	ft_strchr(const char *s, int c)
{
	int	offset;

	offset = 0;
	while (*(s + offset))
	{
		if (*(s + offset) == (char)c)
			return (offset);
		offset++;
	}
	if (*(s + offset) == (char)c)
		return (offset);
	return (-1);
}

void	ft_cpy(char *dst, char *src, int bytes)
{
	int	i;

	i = 0;
	while (i < bytes)
	{
		dst[i] = src[i];
		i++;
	}
	dst[i] = 0;
}
