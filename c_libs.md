| C Header | Functions / Defines |
| --- | --- |
| unistd | write, read |
| stdlib | malloc, free, atoi |
| stdio | size_t, printf |
| string | size_t |
| stdarg | va_start, va_arg, va_end |

Using variadic functions:
```c
some_function(f, ...)

// create a va_list
va_list ptr;

// initialize va by pointing to the last named argument
va_start(ptr, f);

// get variable by passing in the type to be typecasted into
va_arg(ptr, type);

// end va_list
va_end(ptr);
```
