#pragma once

#include <iostream>

#define DEBUG "DEBUG"
#define INFO "INFO"
#define WARN "WARN"
#define ERROR "ERROR"

class Harl {
private:
    void debug(void);
    void info(void);
    void warning(void);
    void error(void);
    void (Harl::*_fptr[4])();
    std::string _values[4];
public:
    Harl();
    ~Harl();
    void complain(std::string level);
};