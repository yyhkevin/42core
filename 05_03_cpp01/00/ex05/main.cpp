#include "Harl.hpp"

int main(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    Harl h;
    h.complain(DEBUG);
    h.complain(INFO);
    h.complain(WARN);
    h.complain(ERROR);
    h.complain("unknown");
}