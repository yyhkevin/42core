#include "Harl.hpp"

void Harl::debug(void) {
    std::cout << "Debug: x is 1" << std::endl;
}

void Harl::info(void) {
    std::cout << "Info: system operational" << std::endl;
}

void Harl::warning(void) {
    std::cout << "Warning: unsupported value passed" << std::endl;
}

void Harl::error(void) {
    std::cout << "Error: null pointer exception encountered" << std::endl;
}

Harl::Harl() {
    _values[0] = DEBUG;
    _values[1] = INFO;
    _values[2] = WARN;
    _values[3] = ERROR;
    _fptr[0] = &Harl::debug;
    _fptr[1] = &Harl::info;
    _fptr[2] = &Harl::warning;
    _fptr[3] = &Harl::error;
}

Harl::~Harl() {

}

void Harl::complain(std::string level) {
    for (int i = 0; i < 4; i++) {
        if (_values[i] == level) {
            (this->*_fptr[i])();
            return;
        }
    }
    std::cout << "Not a valid complain level: " << level << std::endl;
}