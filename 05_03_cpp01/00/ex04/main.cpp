#include "sed.hpp"

int main(int argc, char **argv) {
    if (argc != 4)
    {
        p_line("Error: do not have enough arguments");
        return 1;
    }

    std::string search(argv[2]);
    std::string replace(argv[3]);

    if (search.empty()) {
        p_line("Your second argument is empty.");
        return 1;
    }

    std::string filename(argv[1]);

    std::ifstream infile(filename.c_str());
    if (!infile) {
        std::cout << "Error opening infile: " << filename << std::endl;
        return 1;
    }

    std::string outfilename(filename + ".replace");

    std::ofstream outfile(outfilename.c_str());
    if (!outfile) {
        std::cout << "Error opening outfile: " << outfilename << std::endl;
        infile.close();
        return 2;
    }

    std::string line;
    size_t pos;
    
    // \n should be considered esoteric and not to be handled
    // otherwise, create a buffer and search string again
    while (std::getline(infile, line)) {
        pos = 0;
        while (1) {
            pos = line.find(search, pos);
            if (pos == std::string::npos)
                break;
            line.erase(pos, search.length());
            line.insert(pos, replace);
            pos += replace.length();
        }

        outfile << line << std::endl;
    }
    infile.close();
    outfile.close();
}