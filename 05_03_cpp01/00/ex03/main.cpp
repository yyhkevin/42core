#include "HumanA.hpp"
#include "HumanB.hpp"
#include <iostream>

int main(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    {
        Weapon club = Weapon("crude spiked club");
        HumanA bob("Bob", club);
        bob.attack();
        club.setType("some other type of club");
        bob.attack();
    }
    {
        Weapon club = Weapon("crude spiked club");
        HumanB jim("Jim");
        jim.setWeapon(club);
        jim.attack();
        club.setType("some other type of club");
        jim.attack();
    }
}

    // Weapon w("crude spiked club");
    // HumanA ha("humanA", w);
    // ha.attack();
    // w.setType("non spike");
    // ha.attack();

    // HumanB hb("humanB");
    // hb.attack();
    // hb.setWeapon(w);
    // hb.attack();