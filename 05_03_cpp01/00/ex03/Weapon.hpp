#pragma once

#include <iostream>

class Weapon {
public:
    Weapon(std::string type);
    ~Weapon();
    std::string const &getType();
    void setType(std::string type);
private:
    std::string _type;
};