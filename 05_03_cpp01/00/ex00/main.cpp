#include "Zombie.hpp"

int main(int argc, char **argv) {
    (void) argc;
    (void) argv;
    Zombie *heapZombie = newZombie("HeapZombie");
    heapZombie->announce();
    delete heapZombie;
    randomChump("StackZombie");
}