#include "Zombie.hpp"

int main(int argc, char **argv) {
    (void) argc;
    (void) argv;
    int count = 5;
    Zombie *horde = zombieHorde(count, "HeapZombie");

    if (horde) {
        for (int i = 0; i < count; i++)
            horde[i].announce();
        delete[] horde;
    }
}