/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/12/04 22:05:07 by keyu              #+#    #+#             */
/*   Updated: 2024/12/04 22:05:08 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

int	apply_darkness_factor(int color, t_data *data)
{
	int	r;
	int	g;
	int	b;
	int	darkness_applied;

	r = ((color >> 16) & 0xFF) * data->ray.darkness_factor;
	g = ((color >> 8) & 0xFF) * data->ray.darkness_factor;
	b = ((color & 0xFF) & 0xFF) * data->ray.darkness_factor;
	darkness_applied = (r << 16) | (g << 8) | b;
	return (darkness_applied);
}
