/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_casting.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/12/04 13:30:46 by ysng              #+#    #+#             */
/*   Updated: 2024/12/04 13:30:50 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

static void	init_ray(t_ray *ray, t_player *player, double start_x, t_data *data)
{
	int	r;
	int	g;
	int	b;

	ray->ray_x = player->x;
	ray->ray_y = player->y;
	ray->cos_angle = cos(start_x) * 0.04;
	ray->sin_angle = sin(start_x) * 0.04;
	while (touch_wall(ray->ray_x, ray->ray_y, data) != TRUE)
	{
		ray->ray_x += ray->cos_angle;
		ray->ray_y += ray->sin_angle;
	}
	ray->dist = pythagoras(ray->ray_x - player->x, ray->ray_y - player->y);
	ray->darkness_factor = 1.0 / (1.0 + ray->dist * 0.002);
	if (ray->darkness_factor < 0.2)
		ray->darkness_factor = 0.2;
	r = ((LIGHT_GRAY >> 16) & 0xFF) * ray->darkness_factor;
	g = ((LIGHT_GRAY >> 8) & 0xFF) * ray->darkness_factor;
	b = (LIGHT_GRAY & 0xFF) * ray->darkness_factor;
	ray->ray_color = (r << 16) | (g << 8) | b;
}

static int	init_step_x_dir(double start_x)
{
	if (cos(start_x) > 0)
		return (1);
	return (-1);
}

static int	init_step_y_dir(double start_x)
{
	if (sin(start_x) > 0)
		return (1);
	return (-1);
}

static int	get_wall_side(double start_x, t_data *data)
{
	int	step_x;
	int	step_y;
	int	wall_side;

	wall_side = -1;
	step_x = init_step_x_dir(start_x);
	step_y = init_step_y_dir(start_x);
	if (touch_wall(data->ray.ray_x - step_x, data->ray.ray_y, data)
		|| touch_wall(data->ray.ray_x - step_x, data->ray.ray_y - step_y, data))
	{
		wall_side = S;
		if (step_y == 1)
			wall_side = N;
	}
	else if (touch_wall(data->ray.ray_x, data->ray.ray_y - step_y, data)
		|| touch_wall(data->ray.ray_x, data->ray.ray_y, data))
	{
		wall_side = E;
		if (step_x == 1)
			wall_side = W;
	}
	return (wall_side);
}

void	ray_casting_3d(t_player *player, t_data *data, double start_x, int ray)
{
	double	start_y;
	double	end;
	double	height;
	int		wall_side;

	init_ray(&(data->ray), player, start_x, data);
	wall_side = get_wall_side(start_x, data);
	height = (BOX / data->ray.dist) * (LENGTH / 2);
	start_y = (WIDTH - height) / 2;
	end = start_y + height;
	paint_celling(start_y, ray, data);
	paint_wall(wall_side, data, ray, height);
	paint_floor(end, ray, data);
}

/*
static int	get_wall_side(double start_x, t_data *data)
{
	int	step_x;
	int	step_y;
	int	wall_side;

	wall_side = -1;
	step_x = -1;
	step_y = -1;
	if (cos(start_x) > 0)
		step_x = 1;
	if (sin(start_x) > 0)
		step_y = 1;
	if (touch_wall(data->ray.ray_x - step_x, data->ray.ray_y, data)
		|| touch_wall(data->ray.ray_x - step_x, data->ray.ray_y - step_y, data))
	{
		wall_side = S;
		if (step_y == 1)
			wall_side = N;
	}
	else if (touch_wall(data->ray.ray_x, data->ray.ray_y - step_y, data)
		|| touch_wall(data->ray.ray_x, data->ray.ray_y, data))
	{
		wall_side = E;
		if (step_x == 1)
			wall_side = W;
	}
	return (wall_side);
}
*/