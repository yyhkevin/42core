/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game_init.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/12/03 12:00:56 by ysng              #+#    #+#             */
/*   Updated: 2024/12/04 13:06:48 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

int	if_avatar(int x, int y, char **map)
{
	if (map[y][x] == 'N' || map[y][x] == 'S' || map[y][x] == 'E'
				|| map[y][x] == 'W')
	{
		return (TRUE);
	}
	return (FALSE);
}

void	ft_init_player_coordinate(t_player *player, t_data *data)
{
	int		x;
	int		y;
	int		flag;
	char	**map;

	map = data->map;
	y = -1;
	flag = -1;
	while (map[++y] && flag == -1)
	{
		x = -1;
		while (map[y][++x] && flag == -1)
		{
			if (if_avatar(x, y, map) == TRUE)
			{
				player->x = x * BOX + (BOX / 2);
				player->y = y * BOX + (BOX / 2);
				player->x_direction = x;
				player->y_direction = y;
				flag = 0;
			}
		}
	}
	if (flag == -1)
		error_wrapper(AVATAR_POSITION_MISSING);
}

void	ft_init_player(t_player *player, t_data *data)
{
	int		x;
	int		y;
	char	**map;

	player->x = LENGTH / 2;
	player->y = WIDTH / 2;
	ft_init_player_coordinate(player, data);
	x = player->x_direction;
	y = player->y_direction;
	map = data->map;
	player->angle = 0;
	if (map[y][x] == 'N')
		player->angle = 1.5 * PI;
	else if (map[y][x] == 'S')
		player->angle = PI / 2;
	else if (map[y][x] == 'W')
		player->angle = PI;
	player->up = FALSE;
	player->down = FALSE;
	player->left = FALSE;
	player->right = FALSE;
	player->left_arrow_rotate = FALSE;
	player->right_arrow_rotate = FALSE;
	player->tab = FALSE;
	player->wall_side = -1;
}

void	init_game(t_data *data)
{
	data->mlx_connection = mlx_init();
	if (data->mlx_connection == NULL)
		error_wrapper(MLX_MALLOC_ERROR);
	ft_init_window(data);
	ft_init_img(data);
	ft_init_texture(data);
	ft_init_pixels(data);
	data->map = data->config.map;
	if (!(data->map))
		error_wrapper(MAP_IS_NULL);
	ft_init_player(&data->player, data);
	mlx_put_image_to_window(data->mlx_connection, data->mlx_window,
		data->img.img_ptr, 0, 0);
}

/*
void	ft_init_player_coordinate(t_player *player, t_data *data)
{
	int		x;
	int		y;
	int		flag;
	char	**map;

	map = data->map;
	y = -1;
	flag = -1;
	while (map[++y] && flag == -1)
	{
		x = -1;
		while (map[y][++x] && flag == -1)
		{
			if (if_avatar(x, y, map) == TRUE)
//			if (map[y][x] == 'N' || map[y][x] == 'S' || map[y][x] == 'E'
//				|| map[y][x] == 'W')
			{
				player->x = x * BOX + (BOX / 2);
				player->y = y * BOX + (BOX / 2);
				player->x_direction = x;
				player->y_direction = y;
				flag = 0;
			}
		}
	}
	if (flag == -1)
		error_wrapper(AVATAR_POSITION_MISSING);
}
*/
