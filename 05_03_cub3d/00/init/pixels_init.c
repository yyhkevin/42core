/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/12/03 11:43:51 by ysng              #+#    #+#             */
/*   Updated: 2024/12/03 11:44:03 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

void	ft_init_img(t_data *data)
{
	data->img.img_ptr = mlx_new_image(data->mlx_connection, LENGTH, WIDTH);
	if (data->img.img_ptr == NULL)
	{
		mlx_destroy_window(data->mlx_connection, data->mlx_window);
		free(data->mlx_connection);
		error_wrapper(IMG_MALLOC_ERROR);
	}
}

void	ft_init_pixels(t_data *data)
{
	data->img.pixels_ptr = mlx_get_data_addr(data->img.img_ptr,
			&data->img.bits_per_pixel, &data->img.linelen, &data->img.endian);
	if (data->img.pixels_ptr == NULL)
	{
		mlx_destroy_image(data->mlx_connection, data->img.img_ptr);
		mlx_destroy_window(data->mlx_connection, data->mlx_window);
		free(data->mlx_connection);
		error_wrapper(ADDR_MALLOC_ERROR);
	}
}

void	ft_init_window(t_data *data)
{
	data->mlx_window = mlx_new_window(data->mlx_connection, LENGTH, WIDTH,
			"cub3d");
	if (data->mlx_window == NULL)
	{
		mlx_destroy_display(data->mlx_connection);
		free(data->mlx_connection);
		error_wrapper(WINDOW_MALLOC_ERROR);
	}
}

void	load_textures(t_texture *texture, char *path, void *mlx, t_data *data)
{
	texture->img_ptr = mlx_xpm_file_to_image(mlx, path, &texture->width,
			&texture->height);
	if (!texture->img_ptr)
		error_wrapper(TEXTURE_LOAD_FAIL);
	texture->pixels = (int *)mlx_get_data_addr(texture->img_ptr,
			&data->img.bits_per_pixel, &data->img.linelen, &data->img.endian);
}

void	ft_init_texture(t_data *data)
{
	load_textures(&data->textures[N], data->config.texture_n,
		data->mlx_connection, data);
	load_textures(&data->textures[E], data->config.texture_e,
		data->mlx_connection, data);
	load_textures(&data->textures[S], data->config.texture_s,
		data->mlx_connection, data);
	load_textures(&data->textures[W], data->config.texture_w,
		data->mlx_connection, data);
}
