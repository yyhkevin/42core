/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_player.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/12/03 12:54:17 by ysng              #+#    #+#             */
/*   Updated: 2024/12/03 12:54:21 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

static void	move_up(t_player *player, double sin_angle, double cos_angle)
{
	int	pace;

	pace = 5;
	player->x += cos_angle * pace;
	player->y += sin_angle * pace;
}

static void	move_down(t_player *player, double sin_angle, double cos_angle)
{
	int	pace;

	pace = 5;
	player->x -= cos_angle * pace;
	player->y -= sin_angle * pace;
}

static void	move_left(t_player *player, double sin_angle, double cos_angle)
{
	int	pace;

	pace = 5;
	player->x += sin_angle * pace;
	player->y -= cos_angle * pace;
}

static void	move_right(t_player *player, double sin_angle, double cos_angle)
{
	int	pace;

	pace = 5;
	player->x -= sin_angle * pace;
	player->y += cos_angle * pace;
}

void	move_player(t_player *player, t_data *data)
{
	double	angle_speed;
	double	cos_angle;
	double	sin_angle;

	angle_speed = 0.02;
	sin_angle = sin(player->angle);
	cos_angle = cos(player->angle);
	if (player->left_arrow_rotate == TRUE)
		player->angle -= angle_speed;
	if (player->right_arrow_rotate == TRUE)
		player->angle += angle_speed;
	if (player->angle > 2 * PI)
		player->angle = 0;
	if (player->angle < 0)
		player->angle = 2 * PI;
	if (player->up == TRUE && collision_detection(player, data) == FALSE)
		move_up(player, sin_angle, cos_angle);
	if (player->down == TRUE && collision_detection(player, data) == FALSE)
		move_down(player, sin_angle, cos_angle);
	if (player->left == TRUE && collision_detection(player, data) == FALSE)
		move_left(player, sin_angle, cos_angle);
	if (player->right == TRUE && collision_detection(player, data) == FALSE)
		move_right(player, sin_angle, cos_angle);
}
