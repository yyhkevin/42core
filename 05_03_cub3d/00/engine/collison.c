/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   collison.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/12/04 11:39:45 by ysng              #+#    #+#             */
/*   Updated: 2024/12/04 11:39:54 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

int	check_collision_up(double test_x, double test_y, t_data *data)
{
	test_x += cos(data->player.angle) * 5;
	test_y += sin(data->player.angle) * 5;
	if (touch_wall(test_x + PLAYER_RADIUS, test_y, data) == TRUE
		|| touch_wall(test_x - PLAYER_RADIUS, test_y, data) == TRUE
		|| touch_wall(test_x, test_y + PLAYER_RADIUS, data) == TRUE
		|| touch_wall(test_x, test_y - PLAYER_RADIUS, data) == TRUE)
	{
		return (TRUE);
	}
	return (FALSE);
}

int	check_collision_down(double test_x, double test_y, t_data *data)
{
	test_x -= cos(data->player.angle) * 5;
	test_y -= sin(data->player.angle) * 5;
	if (touch_wall(test_x + PLAYER_RADIUS, test_y, data) == TRUE
		|| touch_wall(test_x - PLAYER_RADIUS, test_y, data) == TRUE
		|| touch_wall(test_x, test_y + PLAYER_RADIUS, data) == TRUE
		|| touch_wall(test_x, test_y - PLAYER_RADIUS, data) == TRUE)
	{
		return (TRUE);
	}
	return (FALSE);
}

int	check_collision_left(double test_x, double test_y, t_data *data)
{
	test_x += sin(data->player.angle) * 5;
	test_y -= cos(data->player.angle) * 5;
	if (touch_wall(test_x + PLAYER_RADIUS, test_y, data) == TRUE
		|| touch_wall(test_x - PLAYER_RADIUS, test_y, data) == TRUE
		|| touch_wall(test_x, test_y + PLAYER_RADIUS, data) == TRUE
		|| touch_wall(test_x, test_y - PLAYER_RADIUS, data) == TRUE)
	{
		return (TRUE);
	}
	return (FALSE);
}

int	check_collision_right(double test_x, double test_y, t_data *data)
{
	test_x -= sin(data->player.angle) * 5;
	test_y += cos(data->player.angle) * 5;
	if (touch_wall(test_x + PLAYER_RADIUS, test_y, data) == TRUE
		|| touch_wall(test_x - PLAYER_RADIUS, test_y, data) == TRUE
		|| touch_wall(test_x, test_y + PLAYER_RADIUS, data) == TRUE
		|| touch_wall(test_x, test_y - PLAYER_RADIUS, data) == TRUE)
	{
		return (TRUE);
	}
	return (FALSE);
}

int	collision_detection(t_player *player, t_data *data)
{
	int	result;

	if (player->up == TRUE)
		result = check_collision_up(player->x, player->y, data);
	if (player->down == TRUE)
		result = check_collision_down(player->x, player->y, data);
	if (player->left == TRUE)
		result = check_collision_left(player->x, player->y, data);
	if (player->right == TRUE)
		result = check_collision_right(player->x, player->y, data);
	return (result);
}

/*
int	collision_detection(t_player *player, t_data* data)
{
	double	cos_angle;
	double	sin_angle;
	double	test_x;
	double	test_y;

	sin_angle = sin(player->angle);
	cos_angle = cos(player->angle);
	test_x = player->x;
	test_y = player->y;
	if(player->up == TRUE)
	{
		test_x = player->x + cos_angle * 5;
		test_y = player->y + sin_angle * 5;
	}
	if (player->down == TRUE)
	{
		test_x = player->x -cos_angle * 5;
		test_y = player->y - sin_angle * 5;
	}
	if (player->left == TRUE)
	{
		test_x = player->x + sin_angle * 5;
		test_y = player-> y - cos_angle * 5;
	}
	if(player->right == TRUE)
	{
		test_x = player->x - sin_angle * 5;
		test_y = player->y + cos_angle * 5;
	}
	if (touch_wall(test_x + PLAYER_RADIUS, test_y, data) == TRUE ||
		touch_wall(test_x - PLAYER_RADIUS, test_y, data) == TRUE ||
		touch_wall(test_x, test_y + PLAYER_RADIUS, data) == TRUE ||
		touch_wall(test_x, test_y - PLAYER_RADIUS, data) == TRUE)
	{
		return (TRUE);
	}
	return (FALSE);
}

*/
