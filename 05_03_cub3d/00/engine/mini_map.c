/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mini_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/12/04 12:17:36 by ysng              #+#    #+#             */
/*   Updated: 2024/12/04 12:17:39 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

void	fov_map(t_player *player, t_data *data, double start_x, int ray)
{
	double	ray_x;
	double	ray_y;
	double	cos_angle;
	double	sin_angle;
	int		color;

	color = LIGHT_GRAY;
	cos_angle = cos(start_x) * 0.0185;
	sin_angle = sin(start_x) * 0.0185;
	ray_x = player->x;
	ray_y = player->y;
	while (touch_wall(ray_x, ray_y, data) != TRUE)
	{
		if (ray == LENGTH / 2)
			color = TRUE_NORTH;
		ft_put_pixels(ray_x, ray_y, color, data);
		ray_x += cos_angle;
		ray_y += sin_angle;
	}
}

void	draw_player(int x, int y, int color, t_data *data)
{
	int	i;
	int	size;

	size = 10;
	i = -1;
	while (++i < size)
		ft_put_pixels(x + i, y, color, data);
	i = -1;
	while (++i < size)
		ft_put_pixels(x, y + i, color, data);
	i = -1;
	while (++i < size)
		ft_put_pixels(x + size, y + i, color, data);
	i = -1;
	while (++i < size)
		ft_put_pixels(x + i, y + size, color, data);
}

void	draw_map(t_data *data)
{
	int		color;
	char	**map;
	int		y;
	int		x;

	map = data->map;
	color = TEAL;
	y = -1;
	while (map[++y])
	{
		x = -1;
		while (map[y][++x])
		{
			if (map[y][x] == '1')
				draw_square(x * BOX, y * BOX, color, data);
			else if (map[y][x] == '0' || map[y][x] == 'p' || map[y][x] == 'N'
				|| map[y][x] == 'S' || map[y][x] == 'E' || map[y][x] == 'W')
				draw_square(x * BOX, y * BOX, NAVY, data);
		}
	}
}

void	mini_map(t_data *data)
{
	t_player	*player;

	player = &data->player;
	draw_map(data);
	draw_player(player->x, player->y, BROWN, data);
}

/*
int	draw_loop(t_data *data)
{
	t_player	*player;
	int			ray;
	double		fraction;
	double		start_x;

	player = &data->player;
	ray = -1;
	fraction = (11.0 / 30.0 * PI) / LENGTH;
	start_x = player->angle - (11.0 / 60.0 * PI);
	move_player(player, data);
	clear_image(data);
	if (data->player.tab == TRUE)
	{
		draw_player(player->x, player->y, GREEN, data);
		draw_map(data);
	}
	while (++ray < LENGTH)
	{
		if (data->player.tab == TRUE)
			fov_map(player, data, start_x, ray);
		else
			ray_casting_3d(player, data, start_x, ray);
		start_x += fraction;
	}
	mlx_put_image_to_window(data->mlx_connection, data->mlx_window,
		data->img.img_ptr, 0, 0);
	return (0);
}
*/