/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/10/07 10:20:13 by ysng              #+#    #+#             */
/*   Updated: 2024/10/23 12:40:55 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_put_pixels(int x, int y, int color, t_data *data)
{
	int	index;

	if (x >= LENGTH || y >= WIDTH)
		return ;
	if (x < 0 || y < 0)
		return ;
	index = (x * data->img.bits_per_pixel / 8) + (y * data->img.linelen);
	data->img.pixels_ptr[index] = color & 0xFF;
	data->img.pixels_ptr[index + 1] = (color >> 8) & 0xFF;
	data->img.pixels_ptr[index + 2] = (color >> 16) & 0xFF;
}

void	draw_square(int x, int y, int color, t_data *data)
{
	int	i;
	int	j;
	int	size;

	size = BOX;
	i = -1;
	while (++i < size)
	{
		j = -1;
		while (++j < size)
			ft_put_pixels(x + j, y + i, color, data);
	}
}

int	touch_wall(double pixel_x, double pixel_y, t_data *data)
{
	int	x;
	int	y;

	x = (int)(pixel_x / BOX);
	y = (int)(pixel_y / BOX);
	if (y < 0 || y >= 720 || x < 0 || x >= 1280)
		return (FALSE);
	if (data->map[y][x] == '1' || data->map[y][x] == 'p')
	{
		return (TRUE);
	}
	return (FALSE);
}

double	pythagoras(double x, double y)
{
	double	x_sq;
	double	y_sq;

	x_sq = x * x;
	y_sq = y * y;
	return (sqrt(x_sq + y_sq));
}

int	main(int argc, char **argv)
{
	t_data	data;

	(void)argv;
	(void)argc;
	try_parse(argc, argv, &data);
	init_game(&data);
	mlx_hook(data.mlx_window, KeyPress, KeyPressMask, key_handler_press, &data);
	mlx_hook(data.mlx_window, KeyRelease, KeyReleaseMask, key_handler_release,
		&data.player);
	mlx_hook(data.mlx_window, DestroyNotify, NoEventMask, close_handler, &data);
	mlx_loop_hook(data.mlx_connection, draw_loop, &data);
	mlx_loop(data.mlx_connection);
	return (0);
}

/*
//DDA is still WIPS

double	dda(double pix_x, double pix_y, double start_x, t_data *data)
{
	int x = (int)(pix_x / BOX);
	int y = (int)(pix_y / BOX);

	// double x = (pix_x / BOX);
	// double y = (pix_y / BOX);


	double	dir_step_x = cos(start_x);
	double	dir_step_y = sin(start_x);

	//understand this: Ray Vector=(cos(start_x),sin(start_x))
	//Ray Vector=(cos(start_x),sin(start_x))
	double delta_x = BOX / fabs(dir_step_x);
	double delta_y = BOX / fabs(dir_step_y);  

	double scale_x =  sqrt((1*1) + (delta_y / BOX) * (delta_y / BOX)); // kiv
	double scale_y =  sqrt((1*1) + (delta_x / BOX) * (delta_x / BOX)); // kiv

	int step_x;
	int step_y;

	step_y = 1;
	step_x = 1;
	if (dir_step_x < 0)
		step_x = -1;
	if (dir_step_y < 0)
		step_y = -1;
	
	double side_dist_x;
	double side_dist_y;

	if (dir_step_x < 0)
		side_dist_x = (pix_x - (x * BOX)) * scale_x;
	else if (dir_step_x >= 0)
		side_dist_x = ((x + 1) * BOX - pix_x) * scale_x;

	if(dir_step_y < 0)
		side_dist_y = (pix_y - (y * BOX)) * scale_y;
	else if (dir_step_y >= 0)
		side_dist_y = ((y + 1) * BOX - pix_y) * scale_y;

	 double hit_x, hit_y;  // To store the precise hit coordinates
	while (1)
	{
		if (side_dist_x < side_dist_y)
		{
			side_dist_x += delta_x * scale_x;
			x += step_x;
			
			if (x < 0 || x >= LENGTH || data->map[y][x] == '1')
			{
				// Calculate precise hit coordinates
				hit_x = pix_x + dir_step_x * side_dist_x;
				hit_y = pix_y + dir_step_y * side_dist_x;
				
				// Calculate exact distance using Pythagorean theorem
				return sqrt(pow(hit_x - pix_x, 2) + pow(hit_y - pix_y, 2));
			}
		}
		else
		{
			side_dist_y += delta_y * scale_y;
			y += step_y;
			
			if (y < 0 || y >= WIDTH || data->map[y][x] == '1')
			{
				// Calculate precise hit coordinates
				hit_x = pix_x + dir_step_x * side_dist_y;
				hit_y = pix_y + dir_step_y * side_dist_y;
				
				// Calculate exact distance using Pythagorean theorem
				return sqrt(pow(hit_x - pix_x, 2) + pow(hit_y - pix_y, 2));
			}
		}
	}
	return 1000;
}


//this is for my refernce as i code

void	fov_map(t_player *player, t_data *data, double start_x, int ray)
{
	double	ray_x;
	double	ray_y;
	double	cos_angle;
	double	sin_angle;
	int		color;

	color = LIGHT_GRAY;
	cos_angle = cos(start_x);
	sin_angle = sin(start_x);
	ray_x = player->x;
	ray_y = player->y;
	while (touch_wall(ray_x, ray_y, data) != TRUE)
	{
		if (ray == LENGTH / 2)
			color = TRUE_NORTH;
		ft_put_pixels(ray_x, ray_y, color, data);
		ray_x += cos_angle;
		ray_y += sin_angle;
	}
}


//this is for my reference as i code

int	draw_loop(t_data *data)
{
	t_player	*player;
	int			ray;
	double		fraction;
	double		start_x;

	player = &data->player;
	ray = -1;
	fraction = (11.0 / 30.0 * PI) / LENGTH;
	start_x = player->angle - (11.0 / 60.0 * PI);
	printf("this is the initial angle: %f\n", start_x);
	move_player(player, data);
	clear_image(data);
	if (data->player.tab == TRUE)
		mini_map(data);
	while (++ray < LENGTH)
	{
		if (data->player.tab == TRUE)
			fov_map(player, data, start_x, ray);
		else
			ray_casting_3d(player, data, start_x, ray);
		start_x += fraction;
	}
	mlx_put_image_to_window(data->mlx_connection, data->mlx_window,
		data->img.img_ptr, 0, 0);
	return (0);
}
*/
