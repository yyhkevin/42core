/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puthex_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/27 10:43:37 by ysng              #+#    #+#             */
/*   Updated: 2023/10/27 10:53:10 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_puthex_fd(unsigned int pointer, char specifier)
{
	int		counter;
	char	*hexadec;

	counter = 0;
	if (specifier == 'x')
		hexadec = "0123456789abcdef";
	else if (specifier == 'X')
		hexadec = "0123456789ABCDEF";
	if (pointer < 16)
	{
		ft_putchar(hexadec[pointer]);
		counter++;
	}
	else
	{
		counter += ft_puthex_fd(pointer / 16, specifier);
		counter += ft_puthex_fd(pointer % 16, specifier);
	}
	return (counter);
}
