/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsize.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/04 14:45:06 by ysng              #+#    #+#             */
/*   Updated: 2023/10/04 15:15:51 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
/*
typedef struct s_list
{
	char			*content;
	struct s_list	*next;
}					t_list;*/
int	ft_lstsize(t_list *lst)
{
	int	count;

	count = 0;
	while (lst)
	{
		lst = lst->next;
		count++;
	}
	return (count);
}
/*
int main()
// create a sample linked list
{
	t_list *head = NULL;
	t_list *current = NULL;

	// create and initiallize nodes
	t_list *node1 = (t_list *)malloc(sizeof(t_list));
	node1->content = "Node 1";
	node1->next = NULL;

	t_list *node2 = (t_list *)malloc(sizeof(t_list));
	node2->content = "Node 2";
	node2->next = NULL;

	t_list *node3 = (t_list *)malloc(sizeof(t_list));
	node3->content = "Node 3";
	node3->next = NULL;

	// set the head and link nodes
	head = node1;
	node1->next = node2;
	node2->next = node3;

	// call ft_lstsize to count the number of nodes
	int count = ft_lstsize(head);

	// print the result
	printf("Number of nodes in the linked list: %d\n", count);

	// Free the allocated memory
	current = head;
	while (current)
	{
		t_list *temp = current;
		current = current->next;
		free(temp);
	}
	return (0);
}*/
