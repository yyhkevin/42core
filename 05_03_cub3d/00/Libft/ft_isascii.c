/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isascii.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/29 13:57:02 by ysng              #+#    #+#             */
/*   Updated: 2023/08/29 15:14:22 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*isascii()
checks whether c is a 7-bit unsigned char value that fits into the ASCII 
character set.

RETURN VALUE
The  values  returned  are nonzero if the character c falls into the tested 
class, and zero if not.
*/

#include "libft.h"

int	ft_isascii(int c)
{
	if (c >= 0 && c <= 127)
		return (1);
	return (0);
}
