/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalpha.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/29 15:15:44 by ysng              #+#    #+#             */
/*   Updated: 2023/08/29 15:31:29 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*isalpha()
checks  for  an  alphabetic  character; in the standard "C" locale, it is 
equivalent to(isupper(c) || islower(c)).  In some locales, there may be  
additional  characters for which isalpha() is true—letters which are neither 
uppercase nor lowercase.

RETURN VALUE
The values  returned  are nonzero if the character c falls into the tested 
class, and zero if not.

*/

#include "libft.h"

int	ft_isalpha(int c)
{
	if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
		return (1);
	return (0);
}
/*
int	main()
{
	printf("%d", ft_isalpha(65));
}*/
