/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/11 21:27:01 by ysng              #+#    #+#             */
/*   Updated: 2023/09/11 23:26:49 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*DESCRIPTION
The  memchr()  function scans the initial n bytes of the memory area pointed 
to by s for the first instance of c.  Both c and the bytes of the memory area 
pointed to  by  s  are  interpreted as unsigned char.

RETURN VALUE
The memchr() and memrchr() functions return a pointer to the matching byte or  
NULL  if  the character does not occur in the given memory area.
*/

void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t	i;

	i = 0;
	while (i < n)
	{
		if (((unsigned char *)s)[i] == (unsigned char)c)
			return (((unsigned char *)s) + i);
		i++;
	}
	return (0);
}
