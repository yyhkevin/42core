/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/25 12:03:49 by ysng              #+#    #+#             */
/*   Updated: 2023/09/25 15:34:53 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
 *Parameters s: 
 The string from which to create the substring.
start: The start index of the substring in the
string ’s’.
len: The maximum length of the substring.
Return value 
The substring.
NULL if the allocation fails.
External functs. malloc

Description 
Allocates (with malloc(3)) and returns a substring
from the string ’s’.
The substring begins at index ’start’ and is of
maximum size ’len’.
 */

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*new_string;
	size_t	string_length;
	size_t	finish;

	if (!s)
		return (0);
	string_length = ft_strlen(s);
	if (start >= string_length)
	{
		new_string = (char *)malloc(1);
		if (!new_string)
			return (0);
		new_string[0] = '\0';
		return (new_string);
	}
	finish = 0;
	if (start < string_length)
		finish = string_length - start;
	if (finish > len)
		finish = len;
	new_string = (char *)malloc(sizeof(char) * (finish + 1));
	if (!new_string)
		return (0);
	ft_strlcpy(new_string, s + start, finish + 1);
	return (new_string);
}
/*
int	main()
{
	char input[] = "Hello, World!";// strlen is 12 excluding \0 
	unsigned int start = 7; // starting at W
	// as length is set to 5, world will be printed. ! will be negated
	size_t	length = 29; //ending at d 

	// call ft_substr to extract a subsrtring
	char *result = ft_substr(input, start, length);

	if (result)
	{
		printf("original string: %s\n", input);// %s - string 
		printf("original string: %s\n", result);// %s - string
		free(result);// free memory allocated bt ft_substr
	}
	else
		printf("memory allocation failed or input string us NULL");
	return (0);
}
*/
