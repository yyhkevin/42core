/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/28 11:07:48 by ysng              #+#    #+#             */
/*   Updated: 2023/10/09 13:28:57 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
Parameters s1: The string to be trimmed.
set: The reference set of characters to trim.
Return value The trimmed string.
NULL if the allocation fails.
External functs. malloc
Description Allocates (with malloc(3)) and returns a copy of
’s1’ with the characters specified in ’set’ removed
from the beginning and the end of the string.
*/
/*
 *count_words -> flag = 0 signifies the start of new word
		count counter is to calculate word count
 * */
/*
 *index set to -1 helps you keep track of word boundaries while iterating
 through the string, allowing you to split the string into words accurately.
 When index is -1, it means you are not inside a word, and when index is
 non-negative, it points to the start of a word.
 */

static void	free_array(char **strs, int counter)
{
	int	strs_index;

	strs_index = 0;
	while (strs_index < counter)
	{
		free(strs[strs_index]);
		strs_index++;
	}
	free(strs);
}

static char	*get_words(const char **s, char d)
{
	int		i;
	char	*str;

	i = 0;
	while ((*s)[i] != '\0' && (*s)[i] == d)
		(*s)++;
	while ((*s)[i] != '\0' && (*s)[i] != d)
		i++;
	str = (char *)malloc((i + 1) * sizeof(char));
	if (!str)
		return (NULL);
	i = 0;
	while ((**s) != '\0' && (**s) != d)
	{
		str[i] = (**s);
		(*s)++;
		i++;
	}
	str[i] = '\0';
	return (str);
}

static int	count_words(char const *str, char c)
{
	int	counter;
	int	flag;

	counter = 0;
	flag = 0;
	while (*str)
	{
		if (*str != c && flag == 0)
		{
			counter++;
			flag = 1;
		}
		else if (*str == c)
			flag = 0;
		str++;
	}
	return (counter);
}

char	**ft_split(char const *s, char d)
{
	int		counter;
	char	**strs;
	int		strs_index;

	counter = count_words(s, d);
	strs = (char **)malloc((counter + 1) * sizeof(char *));
	if (!strs)
		return (NULL);
	strs_index = 0;
	while (strs_index < counter)
	{
		strs[strs_index] = get_words(&s, d);
		if (!strs[strs_index])
		{
			free_array(strs, strs_index);
			return (NULL);
		}
		strs_index++;
	}
	strs[strs_index] = NULL;
	return (strs);
}
/*
int	main(void)
{
	const	char* input_str = "This is a sample string";
	char delimiter = ' ';

	char	**result = ft_split(input_str, delimiter);

	if (result)
	{
		int	i = 0;
		while (result[i] != NULL)
		{
			printf("Word %d: %s\n", i + 1, result[i]);
			free(result[i]);
			i++;
		}
		free(result);
	}
	else
	{
		printf("Split failed\n");
	}
	return (0);
}*/
