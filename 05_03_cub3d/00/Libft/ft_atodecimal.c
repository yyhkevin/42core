/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atodecimal.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/03 14:29:47 by ysng              #+#    #+#             */
/*   Updated: 2024/04/03 14:29:50 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

double	atodecimal(char *str)
{
	int		sign;
	long	integer_num;
	double	decimal;
	double	decimal_multiplier;

	sign = 1;
	integer_num = 0;
	decimal = 0;
	decimal_multiplier = 1;
	while (*str == ' ' || (*str >= '\t' && *str <= '\r'))
		++str;
	while (*str == '-' || *str == '+')
		if ('-' == *str++)
			sign = -1;
	while (*str != '.' && *str)
		integer_num = (integer_num * 10) + (*str++ - '0');
	if ('.' == *str)
		++str;
	while (*str)
	{
		decimal_multiplier /= 10;
		decimal = ((*str++ - '0') * decimal_multiplier) + decimal;
	}
	return ((integer_num + decimal) * sign);
}
