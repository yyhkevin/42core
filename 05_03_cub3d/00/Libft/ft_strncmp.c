/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/11 19:29:45 by ysng              #+#    #+#             */
/*   Updated: 2023/09/11 20:48:29 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
/*
DESCRIPTION
The  strcmp() function compares the two strings s1 and s2.
The locale is not taken into account (for a locale-aware comparison, 
see strcoll(3)).  The comparison is done using  unsigned
characters.

strcmp() returns an integer indicating the result of the comparison, 
as follows:
• 0, if the s1 and s2 are equal;
• a negative value if s1 is less than s2;
• a positive value if s1 is greater than s2.

The  strncmp()  function is similar, except it compares only the first 
(at most) n bytes of s1and s2.

RETURN VALUE
The strcmp() and strncmp() functions return an integer less than, equal to,  
or  greater  than zero  if  s1 (or the first n bytes thereof) is found, 
respectively, to be less than, to match, or be greater than s2.
 

EXAMPLES
The program below can be used to demonstrate the operation of strcmp() 
(when given  two  arguments) and strncmp() (when given three arguments).  
First, some examples using strcmp():

           $ ./string_comp ABC ABC
           <str1> and <str2> are equal
           $ ./string_comp ABC AB      # 'C' is ASCII 67; 'C' - ' ' = 67
           <str1> is greater than <str2> (67)
           $ ./string_comp ABA ABZ     # 'A' is ASCII 65; 'Z' is ASCII 90
           <str1> is less than <str2> (-25)
           $ ./string_comp ABJ ABC
           <str1> is greater than <str2> (7)
           $ ./string_comp $'\201' A   # 0201 - 0101 = 0100 (or 64 decimal)
           <str1> is greater than <str2> (64)

The  last  example  uses bash(1)-specific syntax to produce a string 
containing 
an 8-bit ASCIIcode; the result demonstrates that the string comparison 
uses unsigned characters.

And then some examples using strncmp():

           $ ./string_comp ABC AB 3
           <str1> is greater than <str2> (67)
           $ ./string_comp ABC AB 2
           <str1> and <str2> are equal in the first 2 bytes

 */
#include "libft.h"

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t	i;

	if (n == 0)
		return (0);
	i = 0;
	while (s1[i] == s2[i] && s1[i] != '\0')
	{
		if (i < (n - 1))
			i++;
		else
			return (0);
	}
	return ((unsigned char)(s1[i]) - (unsigned char)(s2[i]));
}
