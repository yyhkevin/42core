/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalnum.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/06 13:50:12 by ysng              #+#    #+#             */
/*   Updated: 2023/09/06 15:17:01 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*  isalnum()checks for an alphanumeric character; 
it is equivalent to (isalpha(c) || isdigit(c)).

isalnum:

isalnum stands for "is alphanumeric."
It checks whether a given character is either an alphabetic 
character (a through z or A through Z) or a decimal digit (0 through 9).
Returns a nonzero value (true) if the character is alphanumeric, 
and 0 (false) otherwise.
*/

int	ft_isalnum(int c)
{
	if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')
		|| (c >= '0' && c <= '9'))
		return (1);
	return (0);
}
