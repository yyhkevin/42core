/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/05 11:34:34 by ysng              #+#    #+#             */
/*   Updated: 2023/10/05 12:29:57 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
/*
typedef struct s_list
{
	char	*content;
	struct s_list	*next;
}	t_list;
*/
void	ft_lstdelone(t_list *lst, void (*del)(void*))
{
	if (!lst)
		return ;
	del (lst->content);
	free (lst);
}
/*
int main()
{
    // Create a t_list node
    t_list *node = malloc(sizeof(t_list));
    
    // Allocate memory for content and copy the string
    node->content = malloc(strlen("hello world") + 1);
    strcpy(node->content, "hello world");

    // Define a function to free the content
    void del(void *content)
    {
        free(content);
    }

    // Call ft_lstdelone to delete the node
    ft_lstdelone(node, del);
    
    return (0);
}
*/
