/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstlast.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/04 15:28:32 by ysng              #+#    #+#             */
/*   Updated: 2023/10/04 16:58:35 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
typedef struct s_list
{
void			*content;
struct s_list	*next;
} t_list;*/

t_list	*ft_lstlast(t_list *lst)
{
	if (!lst)
		return (0);
	while (lst->next)
		lst = lst->next;
	return (lst);
}
/*
int	main(void)
{
	t_list	*head;
	t_list	*node1;
	t_list	*node2;
	t_list	*node3;
	t_list	*lastNode;

	head = NULL;
	// creating some sample nodes to test my function
	node1 = malloc(sizeof(t_list));
	node1->content = "node 1";
	node1->next = NULL;
	node2 = malloc(sizeof(t_list));
	node2->content = "node2";
	node2->next = NULL;
	node3 = malloc(sizeof(t_list));
	node3->content = "node3";
	node3->next = NULL;
	//set the head and the linked nodes
	head = node1;
	node1->next = node2;
	node2->next = node3;
	// call function to find the last node
	lastNode = ft_lstlast(head);
	//print the content of the last node
	if (lastNode)
	{
		printf("The last element of the linked list: %s\n",
			(char *)lastNode->content);
	}
	else
	{
		printf("The linked list is empty./n");
	}
	//clean up the memory by freeing up the nodes
	free(node1);
	free(node2);
	free(node3);
	return (0);
}
*/