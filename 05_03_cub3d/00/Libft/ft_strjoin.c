/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/26 10:58:53 by ysng              #+#    #+#             */
/*   Updated: 2023/09/26 10:59:00 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*new_string;
	int		index;
	int		counter;

	index = 0;
	counter = 0;
	if (!s1 || !s2)
		return (NULL);
	new_string = (char *)malloc(sizeof(char) * ft_strlen(s1) + ft_strlen(s2)
			+ 1);
	if (!new_string)
		return (NULL);
	while (s1[index] != '\0')
	{
		new_string[index] = s1[index];
		index++;
	}
	while (s2[counter] != '\0')
	{
		new_string[index] = s2[counter];
		index++;
		counter++;
	}
	new_string[index] = '\0';
	return (new_string);
}
/*
int	main(void)
{
	char const *str1 = "Hello,";
	char const *str2 = "world!";
	char *join;

	join = ft_strjoin(str1, str2);

	printf("string 1: %s\n", str1);
	printf("string 2: %s\n", str2);
	printf("string joined: %s\n", new_string);
	free(result);
}
*/
