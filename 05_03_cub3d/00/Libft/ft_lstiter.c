/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstiter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/06 11:29:28 by ysng              #+#    #+#             */
/*   Updated: 2023/10/06 11:31:29 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
/**
 typedef struct s_list {
    char *content;
    struct s_list *next;
} t_list;

Iterates the list ’lst’ and applies the function
’f’ on the content of each node.
 

void    print_content (void *content)
{
        if (content)
        {
                printf("%s\n", (char *)content);
        }
}
*/
void	ft_lstiter(t_list *lst, void (*f)(void *))
{
	t_list	*list_ptr;

	if (!lst)
		return ;
	list_ptr = lst;
	while (list_ptr != NULL)
	{
		(*f)(list_ptr->content);
		list_ptr = list_ptr->next;
	}
}
/*
int	main(void)
{
	t_list	node1;
	t_list	node2;
	t_list	node3;

	node1 = {"node1", NULL};
	node2 = {"node2", NULL};
	node3 = {"node3", NULL};

	node1.next = &node2;
	node2.next = &node3;

	//call ft_lstiter to print the content of each node
	ft_lstiter(&node1, print_content);

	return (0);
}
*/
