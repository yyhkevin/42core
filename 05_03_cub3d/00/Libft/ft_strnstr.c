/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/15 15:23:06 by ysng              #+#    #+#             */
/*   Updated: 2023/09/15 22:15:17 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*DESCRIPTION

The strnstr() function locates the first occurrence of the null-terminated 
string needle in the string haystack, where not more than 
len characters are searched.  
Characters that appear after a ‘\0’ character are not searched.  
Since thes trnstr() function is a FreeBSD specific API, 
it should only be used when portability is not a concern.

RETURN VALUES
If needle is an empty string, haystack is returned; 
if needle occurs nowhere in haystack, NULL is returned; 
otherwise a pointer to the first character of the first occurrence 
of needle is returned.

EXAMPLES
The following sets the pointer ptr to the "Bar Baz" portion of largestring:
const char *largestring = "Foo Bar Baz";
const char *smallstring = "Bar";
char *ptr;

ptr = strstr(largestring, smallstring);

The following sets the pointer ptr to NULL, 
because only the first 4 characters of largestring are searched:
const char *largestring = "Foo Bar Baz";
const char *smallstring = "Bar";
char *ptr;

ptr = strnstr(largestring, smallstring, 4);

*/

#include "libft.h"

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t	h;
	size_t	n;

	h = 0;
	if (little[0] == '\0')
		return ((char *)big);
	while (big[h] != '\0')
	{
		n = 0;
		while (big[h + n] == little[n] && (h + n) < len)
		{
			if (big[h + n] == '\0' && little[n] == '\0')
				return ((char *)&big[h]);
			n++;
		}
		if (little[n] == '\0')
			return ((char *)&big[h]);
		h++;
	}
	return (0);
}
