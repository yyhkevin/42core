/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/27 12:19:15 by ysng              #+#    #+#             */
/*   Updated: 2023/09/27 14:16:08 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
 * integer to ASCII
 Allocates (with malloc(3)) and returns a string
representing the integer received as an argument.
Negative numbers must be handled.

ft_len(int n) is to calculate len of int passed

*/
static char	*ft_putascii(char *string, unsigned int number, long int length)
{
	while (number > 0)
	{
		string[length--] = (number % 10) + 48;
		number = number / 10;
	}
	return (string);
}

static long int	ft_length(int n)
{
	int	length;

	length = 0;
	if (n <= 0)
		length = 1;
	while (n != 0)
	{
		length++;
		n = n / 10;
	}
	return (length);
}

char	*ft_itoa(int n)
{
	char			*string;
	long int		length;
	unsigned int	number;
	int				sign;

	sign = 1;
	length = ft_length(n);
	string = (char *)malloc(sizeof(char) * (length + 1));
	if (!string)
		return (NULL);
	string[length--] = '\0';
	if (n == 0)
		string[0] = '0';
	if (n < 0)
	{
		sign *= -1;
		number = n * -1;
		string[0] = '-';
	}
	else
		number = n;
	string = ft_putascii(string, number, length);
	return (string);
}
