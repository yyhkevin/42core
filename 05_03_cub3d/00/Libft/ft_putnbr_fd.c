/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/27 10:43:06 by ysng              #+#    #+#             */
/*   Updated: 2023/11/02 13:57:26 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_putnbr_fd(int n, int fd)

{
	long	number;
	int		count;

	count = 0;
	number = n;
	if (number == -2147483648)
	{
		write(1, "-2147483648", 11);
		return (11);
	}
	count = ft_countdigits(number);
	if (number < 0)
	{
		ft_putchar('-');
		number *= -1;
	}
	if (number > 9)
	{
		ft_putnbr_fd(number / fd, fd);
		ft_putnbr_fd((number % 10), fd);
	}
	else
		ft_putchar(number + 48);
	return (count);
}
