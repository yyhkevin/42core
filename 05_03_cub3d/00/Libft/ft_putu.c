/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putu.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/02 11:26:26 by ysng              #+#    #+#             */
/*   Updated: 2023/11/02 12:34:20 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_countu(unsigned int number)
{
	int	count;

	count = 0;
	if (number == 0)
		count++;
	while (number > 0)
	{
		count++;
		number /= 10;
	}
	return (count);
}

int	ft_putu(unsigned int number)
{
	int	len;

	len = ft_countu(number);
	if (number >= 10)
		ft_putu(number / 10);
	ft_putchar(number % 10 + '0');
	return (len);
}
