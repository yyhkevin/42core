/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striteri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/25 10:54:30 by ysng              #+#    #+#             */
/*   Updated: 2023/09/25 11:16:54 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*Applies the function ’f’ on each character of
the string passed as argument, passing its index
as first argument. Each character is passed by
address to ’f’ to be modified if necessary.*/
void	ft_striteri(char *s, void (*f)(unsigned int, char *))
{
	int	i;

	if (s == NULL)
		return ;
	i = 0;
	while (s[i] != '\0')
	{
		(*f)(i, &s[i]);
		i++;
	}
	s[i] = '\0';
}
/*
//sample function to pass to ft_striteri
void	print_char_index(unsigned int index, char *c)
{
	printf("Character at inder %u: %c\n", index, *c);
}

int	main()
{
	char	test_string[] = "Hello World";

// call ft_steriteri with the test string & print_char_index function
	ft_striteri(test_string, print_char_index);
}
%u:

%u is used for formatting unsigned integers (whole numbers).
It expects an argument of type unsigned int when used with printf.
When you use %u in a printf statement,
it instructs printf to print the corresponding unsigned int argument as an 
unsigned integer without any sign (negative or positive). 
It will display only the magnitude of the integer.
Example: printf("Unsigned integer: %u\n", myUnsignedInteger);
%c:

%c is used for formatting characters.
It expects an argument of type char when used with printf.
When you use %c in a printf statement,
it instructs printf to print the corresponding char 
argument as a single character.
Example: printf("Character: %c\n", myChar);
Here are some examples of how you might use %u & %c in printf statements:

c
Copy code
unsigned int	num = 42;
char			letter = 'A';

printf("Unsigned integer: %u\n", num);
printf("Character: %c\n", letter);
In these examples,
%u is used to format and print the value of an unsigned integer (num), and
%c is used to format and print a character (letter).
*/
/*
void	print_char_index(unsigend int index, char *c)
{
	printf("Character at index %u: %c\n", index, *c);
}

int	main(void)
{
	char test_string[] = "Hello World";
	ft_striteri(test_string, print_char_index);
	return 0
}*/
