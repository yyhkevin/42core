/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/01 13:14:33 by ysng              #+#    #+#             */
/*   Updated: 2023/10/03 11:21:37 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
/*
 A node is like a container in code for storing some values.

 typedef struct s_list
{
        void                    *content;
        struct s_list   *next;
}               t_list;

 */
t_list	*ft_lstnew(void *content)
{
	t_list	*new_node;

	new_node = (t_list *)malloc(sizeof(t_list));
	if (!new_node)
		return (NULL);
	new_node->content = content;
	new_node->next = NULL;
	return (new_node);
}
/*
int	main(void)
{	//create a linked list with an integer value
	int	data = 42;
	t_list	*node = ft_lstnew(&data);

	//check if node creation was successful
	if (node)
	{
	// access the data in the node
		int*	value_ptr = (int *)node->content;

		if (value_ptr! = NULL)
		{
			int	value = *value_ptr;
			printf("Node's data" %d\n", value");		
		}
		else
			printf("Node's content is NULL\n");
		
	}	
	return (0);
}
*/
