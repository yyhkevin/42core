/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/10/07 10:20:26 by ysng              #+#    #+#             */
/*   Updated: 2024/12/04 15:23:13 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h> 
#include "Libft/libft.h"
#include "minilibx-linux/mlx.h"
#include <X11/X.h>
#include <X11/keysym.h>
#include <math.h>

#define TRUE 1
#define FALSE 0

#define FAIL -1
#define SUCCESS 0

#define EPSILON 0.0001
#define LENGTH 1280
#define WIDTH 720
#define PI 3.14159265359
#define GREEN 0x00FF00
#define RED 0xFF0000
#define TRUE_NORTH 0xFF0000
#define TEAL 0x008080
#define WHITE 0xFFFFFF
#define LIGHT_BLUE 0xB0E0E6  // RGB: (176, 224, 230)
#define LIGHT_GRAY 0xD3D3D3
#define NAVY 0x000080 
#define BROWN 0xA52A2A 
#define PLAYER_RADIUS 15 // Radius of the player's hitbox

#define BOX 64

#define NORTH "NO"
#define SOUTH "SO"
#define WEST "WE"
#define EAST "EA"
#define FLOOR "F"
#define CEILING "C"

#define MAP_N 'N'
#define MAP_S 'S'
#define MAP_E 'E'
#define MAP_W 'W'
#define MAP_0 '0'
#define MAP_1 '1'
#define MAP_DEFAULT MAP_1

// defines for texture rendering
#define N 0 //NORTH
#define E 1 //EAST
#define S 2	//SOUTH
#define W 3 //WEST

/*
#define W_KEY 119
#define A_KEY 97
#define S_KEY 115
#define D_KEY 100
*/

typedef struct s_player
{
	float	x;
	float	y;
	float	map_x;
	float	map_y;
	float	angle;
	int		up;
	int		down;
	int		left;
	int		right;
	int		tab;
	int		left_arrow_rotate;
	int		right_arrow_rotate;
	int		x_direction;
	int		y_direction;
	int		wall_side;
}	t_player;

typedef enum e_t_errorMsg
{
	WRONG_ARG = 0,
	IS_DIR,
	IS_NOT_DOT_CUB,
	OPEN_FILE_FAILED,
	MLX_MALLOC_ERROR,
	WINDOW_MALLOC_ERROR,
	IMG_MALLOC_ERROR,
	ADDR_MALLOC_ERROR,
	FD_OPEN_ERROR,
	NO_CONFIG_FILE,
	WRONG_CONFIG_EXT,
	MAP_IS_NULL,
	AVATAR_POSITION_MISSING,
	TEXTURE_LOAD_FAIL,
}							t_errorMsg;

typedef struct s_image
{
	void		*img_ptr;
	char		*pixels_ptr;
	int			bits_per_pixel;
	int			endian;
	int			linelen;
}				t_image;

typedef struct s_config
{
	int		floor_color[3];
	int		ceiling_color[3];
	char	*texture_n;
	char	*texture_s;
	char	*texture_e;
	char	*texture_w;
	char	**map;
}	t_config;

typedef struct s_ray
{
	double	ray_x;
	double	ray_y;
	double	cos_angle;
	double	sin_angle;
	double	dist;
	int		ray_color;
	int		wall_side;
	float	darkness_factor;
}	t_ray;

typedef struct s_texture
{
	void	*img_ptr;
	int		*pixels;
	int		width;
	int		height;
}	t_texture;

typedef struct s_step
{
	double	step_x;
	double	step_y;
}	t_step;

typedef struct s_wall_prop
{
	double		tex_y;
	int			end;
	int			start_y;
	int			tex_x;
	t_texture	*texture;
	double		step;
}	t_wall_prop;

/*below is a sample data*/
//	t_mapinfo	mapinfo; // check
//	int			**texture_pixels;
//	int			**textures;
//	t_texinfo	texinfo; //check
//	t_img		minimap; //check
//	t_step		step;
typedef struct s_data
{
	void		*mlx_connection;
	void		*mlx_window;
	int			win_height;
	int			win_width;
	t_image		img;
	t_player	player;
	char		*cub3d_data;
	t_ray		ray;
	char		**map;
	t_texture	textures[4];
	t_config	config;
}	t_data;

// main.c
int		error_wrapper(t_errorMsg err_msg);

//***event handler***//
//hanlder.c
int		key_handler_release(int keysym, t_player *player);
int		key_handler_press(int keysym, t_data *data);
int		close_handler(t_data *data);

//***init***//
//pixels.c
void	ft_init_img(t_data *data);
void	ft_init_pixels(t_data *data);
void	ft_init_window(t_data *data);
void	ft_init_texture(t_data *data);
void	load_textures(t_texture *texture, char *path, void *mlx, t_data *data);

//game_init.c
void	ft_init_player(t_player *player, t_data *data);
void	init_game(t_data *data);
void	ft_init_player_coordinate(t_player *player, t_data *data);

//***engine***//
//collison.c
int		collision_detection(t_player *player, t_data *data);

//draw_loop.c
void	clear_image(t_data *data);
int		draw_loop(t_data *data);

//mini_map.c
void	fov_map(t_player *player, t_data *data, double start_x, int ray);
void	draw_map(t_data *data);
void	draw_player(int x, int y, int color, t_data *data);
void	mini_map(t_data *data);

//move_player.c
void	move_player(t_player *player, t_data *data);
void	fov_map(t_player *player, t_data *data, double start_x, int ray);
void	draw_map(t_data *data);
void	draw_player(int x, int y, int color, t_data *data);
void	draw_square(int x, int y, int color, t_data *data);

//ray casting.c
void	ray_casting_3d(t_player *player, t_data *data, double start_x, int ray);

// render_utils.c
int		apply_darkness_factor(int color, t_data *data);

//render.c
void	paint_wall(int wall_side, t_data *data, int ray, double height);
void	paint_floor(double end, int ray, t_data *data);
void	paint_celling(double start_y, int ray, t_data *data);
int		apply_darkness_factor(int color, t_data *data);
int		pick_texture(t_data *data, double y, double start_y, double wall_x);

//maths utils
double	pythagoras(double x, double y);

// destroy_config.c
void	destroy_map(char **map);
void	destroy_config(t_config *conf);

// map_utils.c
int		count_rows(char **map);

// map_verifier_utils.c
void	p_map(char **map);
int		check_bounded_clone(int y, int x, char **map);

// map_verifier.c
int		verify_map(t_config *conf);

// parse_colors.c
void	color_error(char *k, char **splitted, char *nl, t_config *conf);
int		parse_colors(char *k, char *nl, int *array, t_config *conf);

// parse_map_utils.c
void	check_valid_chars(char *k, char **splitted, char *nl, t_config *conf);
int		map_line_is_valid(char *s);
void	error_map(char *nl, t_list *map, int fd, t_config *conf);
void	error_map_break(char *nl, t_list *map, int fd, t_config *conf);

// parse_map.c
char	**parse_map_section(char *nl, int fd, t_config *conf);

// parse.c
void	destroy_config(t_config *conf);
void	try_parse(int argc, char **argv, t_data *data);

// parsing_utils.c
int		p_is_space(char c);
char	*cp_text_endl(char *src);
int		p_is_endl(char c);
void	set_config_default(t_config *config);
void	check_config_ext(char *path);

// textures_verifier.c
int		verify_textures(t_config *conf);
void	ft_put_pixels(int x, int y, int color, t_data *data);
int		touch_wall(double pixel_x, double pixel_y, t_data *data);
double	dda(double pix_x, double pix_y, double start_x, t_data *data);
