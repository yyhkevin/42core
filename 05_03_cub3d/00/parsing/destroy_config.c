/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   destroy_config.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/11/15 17:12:06 by keyu              #+#    #+#             */
/*   Updated: 2024/11/15 17:12:06 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

void	destroy_map(char **map)
{
	int	i;

	i = 0;
	while (map[i])
		free(map[i++]);
	free(map);
}

static void	destroy_textures(t_config *conf)
{
	if (conf->texture_n)
		free(conf->texture_n);
	if (conf->texture_s)
		free(conf->texture_s);
	if (conf->texture_e)
		free(conf->texture_e);
	if (conf->texture_w)
		free(conf->texture_w);
}

void	destroy_config(t_config *conf)
{
	if (conf->map)
		destroy_map(conf->map);
	destroy_textures(conf);
}
