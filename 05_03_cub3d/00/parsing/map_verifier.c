/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_verifier.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/12/01 13:53:32 by keyu              #+#    #+#             */
/*   Updated: 2024/12/01 13:53:33 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

void	p_map(char **map)
{
	int	i;

	i = 0;
	while (map[i])
	{
		printf("%s\n", map[i]);
		i++;
	}
	printf("====\n");
}

static int	is_player(char c)
{
	return (c == MAP_N || c == MAP_S || c == MAP_E || c == MAP_W);
}

static int	count_players(char **map)
{
	int	r;
	int	c;
	int	count;

	r = 0;
	count = 0;
	while (map[r])
	{
		c = 0;
		while (map[r][c])
		{
			if (is_player(map[r][c]))
				count++;
			c++;
		}
		r++;
	}
	return (count);
}

static int	check_bounded(char **map)
{
	int	x;
	int	y;

	y = 0;
	while (map[y])
	{
		x = 0;
		while (map[y][x])
		{
			if (is_player(map[y][x]))
				return (check_bounded_clone(y, x, map));
			x++;
		}
		y++;
	}
	return (0);
}

int	verify_map(t_config *conf)
{
	int	players;

	players = count_players(conf->map);
	if (players != 1)
	{
		if (players == 0)
			ft_putstr_fd("Error\nNo player start point detected.\n",
				STDERR_FILENO);
		else
			ft_putstr_fd("Error\nMultiple player start points detected.\n",
				STDERR_FILENO);
		destroy_config(conf);
		exit(EXIT_FAILURE);
	}
	p_map(conf->map);
	if (!check_bounded(conf->map))
	{
		ft_putstr_fd("Error\nMap is not bounded.\n", STDERR_FILENO);
		destroy_config(conf);
		exit(EXIT_FAILURE);
	}
	return (1);
}
