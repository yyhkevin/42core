/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   textures_verifier.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/11/15 17:10:00 by keyu              #+#    #+#             */
/*   Updated: 2024/11/15 17:10:01 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

static int	verify_xpm(char *path)
{
	if ((int) ft_strlen(path) < 4)
		return (0);
	while (*path)
		path++;
	return (*(path - 4) == '.' && *(path - 3) == 'x'
		&& *(path - 2) == 'p' && *(path - 1) == 'm');
}

static int	verify_path(char *path)
{
	int	fd;

	fd = open(path, O_RDONLY);
	if (fd != -1)
		close(fd);
	else
	{
		ft_putstr_fd("Invalid texture: ", STDERR_FILENO);
		ft_putstr_fd(path, STDERR_FILENO);
		ft_putstr_fd("\n", STDERR_FILENO);
	}
	if (!verify_xpm(path))
		ft_putstr_fd("Textures must be xpm.\n", STDERR_FILENO);
	return (fd != -1 && verify_xpm(path));
}

int	verify_textures(t_config *conf)
{
	if (conf->floor_color[0] == -1 || conf->ceiling_color[0] == -1)
	{
		ft_putstr_fd("Failed to parse floor/ceiling colors: "
			"invalid config / unexistent\n", STDERR_FILENO);
		return (0);
	}
	if (conf->texture_n && conf->texture_s
		&& conf->texture_e && conf->texture_w)
		return (verify_path(conf->texture_n) && verify_path(conf->texture_s)
			&& verify_path(conf->texture_e) && verify_path(conf->texture_w));
	ft_putstr_fd("Texture for NO/SO/EA/WE is missing.\n", STDERR_FILENO);
	return (0);
}
