/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/11/15 15:18:06 by keyu              #+#    #+#             */
/*   Updated: 2024/11/15 15:18:07 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

// returns (1) on success
static int	put_texture_config(char **ctr, char *k, char *nl, t_config *conf)
{
	if (*ctr)
	{
		ft_putstr_fd("Error\nDuplicate key: ", STDERR_FILENO);
		write(STDERR_FILENO, k, 1);
		write(STDERR_FILENO, k + 1, 1);
		ft_putstr_fd("\n", STDERR_FILENO);
		free(nl);
		destroy_config(conf);
		exit(EXIT_FAILURE);
	}
	*ctr = cp_text_endl(k + 2);
	return (1);
}

// returns 0 if is map, 1 if config
static int	parse_first_section(char *nl, t_config *conf)
{
	char	*s;

	s = nl;
	if (*s == '\n')
		return (1);
	while (p_is_space(*s))
		s++;
	if (ft_strncmp(s, NORTH, 2) == 0 && p_is_endl(*(s + 2)))
		return (put_texture_config(&(conf->texture_n), s, nl, conf));
	else if (ft_strncmp(s, SOUTH, 2) == 0 && p_is_endl(*(s + 2)))
		return (put_texture_config(&(conf->texture_s), s, nl, conf));
	else if (ft_strncmp(s, EAST, 2) == 0 && p_is_endl(*(s + 2)))
		return (put_texture_config(&(conf->texture_e), s, nl, conf));
	else if (ft_strncmp(s, WEST, 2) == 0 && p_is_endl(*(s + 2)))
		return (put_texture_config(&(conf->texture_w), s, nl, conf));
	else if (ft_strncmp(s, FLOOR, 1) == 0 && p_is_endl(*(s + 1)))
		return (parse_colors(s, nl, conf->floor_color, conf));
	else if (ft_strncmp(s, CEILING, 1) == 0 && p_is_endl(*(s + 1)))
		return (parse_colors(s, nl, conf->ceiling_color, conf));
	return (0);
}

static void	config_error(t_config *conf, char *nl, int fd)
{
	free(nl);
	close(fd);
	destroy_config(conf);
	exit(EXIT_FAILURE);
}

static void	parse_config(int fd, t_config *conf)
{
	int		is_map;
	char	*nl;

	is_map = 0;
	nl = get_next_line(fd);
	while (nl && !is_map)
	{
		is_map = !parse_first_section(nl, conf);
		if (is_map)
			break ;
		free(nl);
		nl = get_next_line(fd);
	}
	if (!verify_textures(conf))
		config_error(conf, nl, fd);
	conf->map = parse_map_section(nl, fd, conf);
	close(fd);
}

void	try_parse(int argc, char **argv, t_data *data)
{
	int	config_fd;

	if (argc != 2)
		error_wrapper(NO_CONFIG_FILE);
	check_config_ext(argv[1]);
	config_fd = open(argv[1], O_RDONLY);
	set_config_default(&(data->config));
	if (config_fd != -1)
		parse_config(config_fd, &(data->config));
	else
		error_wrapper(NO_CONFIG_FILE);
}
