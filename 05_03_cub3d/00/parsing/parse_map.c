/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/12/02 20:43:20 by keyu              #+#    #+#             */
/*   Updated: 2024/12/02 20:43:20 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

static t_list	*add_map_ll(char *l, t_list *map)
{
	char	*cp;
	int		i;

	i = 0;
	while (l[i] && l[i] != '\n')
		i++;
	cp = malloc(sizeof(char) * (i + 1));
	cp[i--] = 0;
	while (i >= 0)
	{
		cp[i] = l[i];
		i--;
	}
	if (map == 0)
		return (ft_lstnew((void *)cp));
	ft_lstadd_back(&map, ft_lstnew((void *)cp));
	return (map);
}

static char	*cp_line(char *line, int max)
{
	char	*out;
	int		i;

	i = 0;
	out = malloc(sizeof(char) * (max + 1));
	while (i < max)
	{
		while (i < (int) ft_strlen(line))
		{
			if (line[i] == ' ')
				out[i] = MAP_DEFAULT;
			else
				out[i] = line[i];
			i++;
		}
		out[i] = MAP_0;
		i++;
	}
	out[max] = 0;
	free(line);
	return (out);
}

static void	make_equal(char **map)
{
	int	max;
	int	i;

	max = 0;
	i = 0;
	while (map[i])
	{
		if ((int) ft_strlen(map[i]) > max)
			max = ft_strlen(map[i]);
		i++;
	}
	i = 0;
	while (map[i])
	{
		map[i] = cp_line(map[i], max);
		i++;
	}
}

static char	**convert_ll(t_list *ll, t_config *conf)
{
	char	**output;
	int		i;
	t_list	*ori;
	int		size;

	i = 0;
	ori = ll;
	size = ft_lstsize(ori);
	output = malloc(sizeof(char *) * (size + 1));
	while (i < size)
	{
		output[i] = ft_strdup((char *) ll->content);
		ll = ll->next;
		i++;
	}
	output[i] = 0;
	ft_lstclear(&ori, free);
	make_equal(output);
	conf->map = output;
	if (verify_map(conf))
		return (output);
	return (0);
}

char	**parse_map_section(char *nl, int fd, t_config *conf)
{
	int		have_endl;
	t_list	*map;

	have_endl = 0;
	map = 0;
	while (nl)
	{
		if (*nl == '\n' && *(nl + 1) == 0)
			have_endl = 1;
		if (map_line_is_valid(nl))
		{
			if (have_endl && !(*nl == '\n' && *(nl + 1) == 0))
				error_map_break(nl, map, fd, conf);
			map = add_map_ll(nl, map);
		}
		else
			error_map(nl, map, fd, conf);
		free(nl);
		nl = get_next_line(fd);
	}
	return (convert_ll(map, conf));
}
