# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ysng <marvin@42.fr>                        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2024/04/03 13:12:40 by ysng              #+#    #+#              #
#    Updated: 2024/04/03 13:12:44 by ysng             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = cub3D

CC = gcc
RM = rm -rf

CFLAGS = -Wall -Wextra -Werror -g
MLXFLAGS = -L ./minilibx-linux -lmlx_Linux -lX11 -lXext 

LIBFT = ./Libft/libft.a

SRC = main.c \
		error/error.c \
		init/pixels_init.c \
		init/game_init.c \
		engine/draw_loop.c \
		engine/move_player.c \
		engine/mini_map.c \
		engine/collison.c \
		render/ray_casting.c \
		render/render_utils.c \
		render/render.c \
		event_handler/handler.c \
		parsing/destroy_config.c \
		parsing/map_utils.c \
		parsing/map_verifier_utils.c \
		parsing/map_verifier.c \
		parsing/parse_colors.c \
		parsing/parse_map_utils.c \
		parsing/parse_map.c \
		parsing/parse.c \
		parsing/parsing_utils.c \
		parsing/textures_verifier.c \

OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	$(MAKE) --no-print-directory -C ./Libft bonus CFLAGS="-Wall -Wextra -Werror -g -fPIC"
	$(MAKE) --no-print-directory -C ./minilibx-linux
	echo "\033[1m ✅ MiniLibX compiled \033[0m✅"
	$(CC) $(CFLAGS) $(OBJ) $(LIBFT) $(MLXFLAGS) -lm -o $(NAME)
	@echo "\033[1m ❌ no ft_printf \033[0m❌"
	@echo "\033[1m ✅ get_next_line \033[0m✅"
	@echo "\033[1m ✅ libft compiled \033[0m✅"
	@echo "\033[1m ✅ cub3d compiled \033[0m✅"
	@echo "                  "
	@echo "                        __              __         ____      "
	@echo "                       /\\ \\           /'__\`\\      /\\  _\`\\    "
	@echo "  ___       __  __     \\ \\ \\____     /\\_\\L\\ \\     \\ \\ \\/\\ \\  "
	@echo " /'___\\    /\\ \\/\\ \\     \\ \\ '__\`\\    \\/_/_\\_<_     \\ \\ \\ \\ \\ "
	@echo "/\\ \\__/    \\ \\ \\_\\ \\     \\ \\ \\L\\ \\     /\\ \\L\\ \\     \\ \\ \\_\\ \\"
	@echo "\\ \\____\\    \\ \\____/      \\ \\_,__/     \\ \\____/      \\ \\____/"
	@echo " \\/____/     \\/___/        \\/___/       \\/___/        \\/___/  "
	@echo "                                                                       "
	@echo "\033[1m ◽ cub3D by @jacksng and @keyu is ready ◽ \033[0m"
	@echo "                                                                       "

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(MAKE) clean -C ./Libft
	$(RM) $(OBJ)
	@echo "✨ OBJ deleted ✨"

fclean: clean
	$(MAKE) fclean -C ./Libft
	rm -rf $(NAME)
	@echo "✨ $(NAME) deleted✨"

re: fclean all

.PHONY: all, clean, fclean, re, bonus

.SILENT:
