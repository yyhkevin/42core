/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/20 10:40:06 by ysng              #+#    #+#             */
/*   Updated: 2023/09/20 14:11:46 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*The calloc() function contiguously allocates enough space for 
count objects that are size bytes of memory each and
returns a pointer to the allocated memory.  
The allocated memory is filled with bytes of value zero.*/
/*
nmeb: The number of elements to allocate memory for.
size: The size (in bytes) of each element.
*/
void	*ft_calloc(size_t nmemb, size_t size)
{
	void		*array;	

	array = malloc(nmemb * size);
	if (array == 0)
		return (0);
	ft_bzero(array, (nmemb * size));
	return (array);
}
