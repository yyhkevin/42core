/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/05 12:32:04 by ysng              #+#    #+#             */
/*   Updated: 2023/10/05 14:08:23 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstclear(t_list **lst, void (*del)(void *))
{
	t_list	*node;

	if (!lst)
		return ;
	while (*lst)
	{
		node = (*lst)->next;
		ft_lstdelone(*lst, del);
		*lst = node;
	}
}
/*
void	del_content(void *content)
{
	free(content);
}

void main()
{
	//create a simple linked list
	t_list	*first = malloc(sizeof(t_list));
	t_list	*second = malloc(sizeof(t_list));
	t_list	*third = malloc(sizeof(t_list));
	
	//filling content and linking nodes
	first->content = malloc(sizeof(int));
	*((int *)(first->content)) = 1;
	first->next = second;

	second->content = malloc(sizeof(int));
	*((int *)(second->content)) = 2;
	second->next = third;

	third->content = malloc(sizeof(int));
	*((int *)(third->content)) = 3;
	third->next = NULL;

	//print the original list
	t_list	*current = first;
	printf("Original list: ");
	
	while (current)
	{
		printf("%d -> ", *((int *)(current->content)));
		current = current->next;
	}
	printf("NULL\n");

	ft_lstclear(&first, del_content);
	
	if(first == NULL)
	{
		printf("List cleared successfully\n");
	}
	else
	{
		printf("List not cleared.\n");
	}
	return (0);
}*/
