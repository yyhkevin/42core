/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putpointer.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/27 10:42:37 by ysng              #+#    #+#             */
/*   Updated: 2023/11/02 11:03:44 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_pointertohex(unsigned long pointer)
{
	char	*hex;
	int		counter;

	hex = "0123456789abcdef";
	counter = 0;
	if (pointer < 16)
	{
		ft_putchar(hex[pointer]);
		counter++;
	}
	else
	{
		counter += ft_pointertohex(pointer / 16);
		counter += ft_pointertohex(pointer % 16);
	}
	return (counter);
}

int	ft_putpointer(unsigned long number)
{
	if (number == 0)
	{
		write(1, "(nil)", 5);
		return (5);
	}
	ft_putchar('0');
	ft_putchar('x');
	return (ft_pointertohex(number) + 2);
}
