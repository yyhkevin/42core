/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_front.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/03 10:59:30 by ysng              #+#    #+#             */
/*   Updated: 2023/10/05 10:56:31 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
/*
typedef struct s_list
{
    char *content;
    struct s_list *next;
} t_list;
*/
void	ft_lstadd_front(t_list **lst, t_list *new)
{
	if (!lst || !new)
		return ;
	new->next = *lst;
	*lst = new;
}
/*
int	main()
{
	//create a few nodes for the linked list
	t_list	*node1 = malloc(sizeof(t_list));
	t_list  *node2 = malloc(sizeof(t_list));
	t_list  *node3 = malloc(sizeof(t_list));	

	//assign some content to the nodes
	node1->content = "node 1";
	node2->content = "node 2";
	node3->content = "node 3";
	
	//initiallize the linked list pointer
	t_list	*list = NULL;

	//add the nodes to the front of the linked list
	ft_lstadd_front(&list, node1);
	ft_lstadd_front(&list, node2);
	ft_lstadd_front(&list, node3);	
	//Traverse and print the linked list
	t_list	*current = list;
	while (current)
	{
		printf("%s\n", (char *)current->content);
		current = current->next;
	}
	//free the memory allocated for the nodea
	free (node1);
	free (node2);
	free (node3);
	return (0);
}*/
