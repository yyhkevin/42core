/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/10 13:00:30 by ysng              #+#    #+#             */
/*   Updated: 2023/11/02 16:54:47 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	print_convert(char specifier, va_list argument_pointer)
{
	int	counter;

	counter = 0;
	if (specifier == 'c')
		counter += ft_putchar(va_arg(argument_pointer, int));
	else if (specifier == 's')
		counter += ft_putstr(va_arg(argument_pointer, char *));
	else if (specifier == '%')
		counter += ft_putchar('%');
	else if (specifier == 'x' || specifier == 'X')
		counter += ft_puthex_fd(va_arg(argument_pointer, unsigned int),
				specifier);
	else if (specifier == 'd' || specifier == 'i')
		counter += ft_putnbr_fd((va_arg(argument_pointer, int)), 10);
	else if (specifier == 'p')
		counter += ft_putpointer(va_arg(argument_pointer, unsigned long));
	else if (specifier == 'u')
		counter += ft_putu(va_arg(argument_pointer, unsigned int));
	else
	{
		write (1, "Unrecognized format specifier ", 30);
		return (30 + ft_putchar(specifier));
	}
	return (counter);
}

int	ft_printf(const char *format, ...)
{
	va_list	argument_pointer;
	int		count;

	va_start(argument_pointer, format);
	count = 0;
	if (!format)
		return (0);
	while (*format != '\0')
	{
		if (*format == '%')
			count += print_convert(*(++format), argument_pointer);
		else
			count += ft_putchar(*format);
		format++;
	}
	va_end(argument_pointer);
	return (count);
}
