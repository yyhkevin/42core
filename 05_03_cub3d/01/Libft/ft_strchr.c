/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/07 13:15:58 by ysng              #+#    #+#             */
/*   Updated: 2023/09/11 22:21:59 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*DESCRIPTION
The strchr() function returns a pointer to the first occurrence of the 
character c in the string s.

RETURN VALUE
The  strchr()  and  strrchr() functions return a pointer to the matched 
character or NULL if the character is not found.  The terminating null byte is 
considered part of the string, so that if c is specified as '\0', 
these functions return a pointer to the terminator.

 */

char	*ft_strchr(const char *s, int c)
{
	int	i;

	i = 0;
	while (s[i] != '\0' && s[i] != (char)c)
		i++;
	if (s[i] == (char)c)
		return ((char *)&s[i]);
	return (NULL);
}
