/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/05 12:47:25 by keyu              #+#    #+#             */
/*   Updated: 2023/10/12 16:05:50 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

// for read()
# include <unistd.h>
// for malloc
# include <stdlib.h>
// for printf
# include <stdio.h>
// for open, close
# include <fcntl.h>

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 100
# endif

struct	s_pos
{
	char	buf[BUFFER_SIZE + 1];
	int		pos;
};

char	*get_next_line(int fd);
void	*ft_calloc(size_t count, size_t size);
size_t	ft_strlen(const char *s);
size_t	ft_strlcat(char *dst, const char *src, size_t dst_size);
int		ft_strchr(const char *s, int c);
void	ft_cpy(char *dst, char *src, int bytes);
#endif
