/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/15 18:06:06 by ysng              #+#    #+#             */
/*   Updated: 2023/09/15 20:24:49 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
/*DESCRIPTION
The  bzero()  function  erases  the data in the n bytes of the memory 
starting at the location pointed to by s, by writing zeros 
(bytes containing '\0') to that area.

 The explicit_bzero() function performs the same task as bzero().  
 It differs from  bzero() in that it guarantees that compiler optimizations 
 will not remove the erase operation if the compiler deduces that 
 the operation is "unnecessary".

RETURN VALUE
None
*/
void	ft_bzero(void *s, size_t n)
{
	ft_memset(s, '\0', n);
}
