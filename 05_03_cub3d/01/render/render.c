/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/12/04 13:28:33 by ysng              #+#    #+#             */
/*   Updated: 2024/12/04 13:59:35 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

int	pick_pix(int tex_x, int tex_y, t_texture *texture)
{
	int	x;
	int	y;

	if (texture == NULL || texture->pixels == NULL)
		return (0);
	x = tex_x % texture->width;
	y = tex_y % texture->height;
	if (x >= 0 && x < texture->width && y >= 0 && y < texture->height)
		return (texture->pixels[y * texture->width + x]);
	return (0);
}

void	draw_wall_slice(double height, int start_x, t_data *data)
{
	t_wall_prop	p;

	p.tex_y = 0.0;
	p.texture = &data->textures[data->player.wall_side];
	p.step = p.texture->height / height;
	if (height > WIDTH)
	{
		p.tex_y = (height - WIDTH) * p.step / 2;
		height = WIDTH;
	}
	p.start_y = (WIDTH - height) / 2;
	p.end = p.start_y + height;
	p.tex_x = (int)data->ray.ray_y % BOX;
	if (data->player.wall_side == N || data->player.wall_side == S)
		p.tex_x = (int)data->ray.ray_x % BOX;
	while (p.start_y < p.end)
	{
		ft_put_pixels(start_x, p.start_y,
			pick_pix(p.tex_x, p.tex_y, p.texture), data);
		p.start_y++;
		p.tex_y += p.step;
	}
}

void	paint_wall(int wall_side, t_data *data, int ray, double height)
{
	data->player.wall_side = wall_side;
	draw_wall_slice(height, ray, data);
}

void	paint_celling(double start_y, int ray, t_data *data)
{
	double	ceiling_start;
	int		ceiling_color;

	ceiling_start = start_y;
	ceiling_color = (data->config.ceiling_color[0] << 16)
		| (data->config.ceiling_color[1] << 8)
		| (data->config.ceiling_color[2]);
	while (ceiling_start > 0)
	{
		ft_put_pixels(ray, ceiling_start, ceiling_color, data);
		ceiling_start--;
	}
}

void	paint_floor(double end, int ray, t_data *data)
{
	double	floor_start;
	int		floor_color;

	floor_start = end;
	floor_color = (data->config.floor_color[0] << 16)
		| (data->config.floor_color[1] << 8)
		| (data->config.floor_color[2]);
	while (floor_start < WIDTH)
	{
		ft_put_pixels(ray, floor_start, floor_color, data);
		floor_start++;
	}
}
