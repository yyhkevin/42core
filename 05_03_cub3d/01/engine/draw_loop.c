/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_loop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/12/03 12:40:26 by ysng              #+#    #+#             */
/*   Updated: 2024/12/03 12:40:29 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

void	clear_image(t_data *data)
{
	int	x;
	int	y;

	y = -1;
	while (++y < WIDTH)
	{
		x = -1;
		while (++x < LENGTH)
			ft_put_pixels(x, y, 0, data);
	}
}

// render view
int	draw_loop(t_data *data)
{
	t_player	*player;
	int			ray;
	double		fraction;
	double		start_x;

	player = &data->player;
	ray = -1;
	fraction = (11.0 / 30.0 * PI) / LENGTH;
	start_x = player->angle - (11.0 / 60.0 * PI);
	move_player(player, data);
	clear_image(data);
	if (data->player.tab == TRUE)
		mini_map(data);
	while (++ray < LENGTH)
	{
		if (data->player.tab == TRUE)
			fov_map(player, data, start_x, ray);
		else
			ray_casting_3d(player, data, start_x, ray);
		start_x += fraction;
	}
	mlx_put_image_to_window(data->mlx_connection, data->mlx_window,
		data->img.img_ptr, 0, 0);
	return (0);
}
