/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handler.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/12/04 12:39:41 by ysng              #+#    #+#             */
/*   Updated: 2024/12/04 12:39:43 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

int	close_handler(t_data *data)
{
	int	i;

	i = -1;
	while (++i < 4)
	{
		if (data->textures[i].img_ptr)
		{
			mlx_destroy_image(data->mlx_connection, data->textures[i].img_ptr);
			data->textures[i].img_ptr = NULL;
		}
	}
	if (data->img.img_ptr)
		mlx_destroy_image(data->mlx_connection, data->img.img_ptr);
	if (data->mlx_window)
		mlx_destroy_window(data->mlx_connection, data->mlx_window);
	if (data->mlx_connection)
	{
		mlx_destroy_display(data->mlx_connection);
		free(data->mlx_connection);
	}
	destroy_config(&data->config);
	exit(EXIT_SUCCESS);
}

int	key_handler_press(int keysym, t_data *data)
{
	t_player	*player;

	player = &(data->player);
	if (keysym == 65307)
		close_handler(data);
	if (keysym == 119)
		player->up = TRUE;
	if (keysym == 97)
		player->left = TRUE;
	if (keysym == 115)
		player->down = TRUE;
	if (keysym == 100)
		player->right = TRUE;
	if (keysym == 65361)
		player->left_arrow_rotate = TRUE;
	if (keysym == 65363)
		player->right_arrow_rotate = TRUE;
	if (keysym == 65289)
		player->tab = TRUE;
	return (0);
}

int	key_handler_release(int keysym, t_player *player)
{
	if (keysym == 119)
		player->up = FALSE;
	if (keysym == 97)
		player->left = FALSE;
	if (keysym == 115)
		player->down = FALSE;
	if (keysym == 100)
		player->right = FALSE;
	if (keysym == 65361)
		player->left_arrow_rotate = FALSE;
	if (keysym == 65363)
		player->right_arrow_rotate = FALSE;
	if (keysym == 65289)
		player->tab = FALSE;
	return (0);
}
