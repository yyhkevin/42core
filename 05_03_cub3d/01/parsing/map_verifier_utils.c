/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_verifier_utils.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/12/10 21:17:41 by keyu              #+#    #+#             */
/*   Updated: 2024/12/10 21:17:42 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

static int	flood_fill(int y, int x, char **map, int rows)
{
	if (x < 0 || y < 0 || y >= rows || x >= (int) ft_strlen(map[y]))
		return (0);
	if (map[y][x] == 'v')
		return (1);
	if (map[y][x] == MAP_1)
		return (1);
	map[y][x] = 'v';
	return (flood_fill(y - 1, x, map, rows) && flood_fill(y + 1, x, map, rows)
		&& flood_fill(y, x - 1, map, rows) && flood_fill(y, x + 1, map, rows));
}

static void	put_diagonal(int y, int x, char **map, int rows)
{
	if (x < 0 || y < 0 || y >= rows || x >= (int) ft_strlen(map[y]))
		return ;
	if (map[y][x] == MAP_0)
		map[y][x] = 'p';
}

static void	psuedo_wall(char **map, int rows)
{
	int	y;
	int	x;

	y = 0;
	while (y < rows)
	{
		x = 0;
		while (map[y][x])
		{
			if (map[y][x] == 'v')
			{
				put_diagonal(y - 1, x - 1, map, rows);
				put_diagonal(y - 1, x + 1, map, rows);
				put_diagonal(y + 1, x - 1, map, rows);
				put_diagonal(y + 1, x + 1, map, rows);
			}
			x++;
		}
		y++;
	}
}

static void	reset_map(char **map, int rows)
{
	int	y;
	int	x;

	y = 0;
	while (y < rows)
	{
		x = 0;
		while (map[y][x])
		{
			if (map[y][x] == 'v')
				map[y][x] = MAP_0;
			x++;
		}
		y++;
	}
}

int	check_bounded_clone(int y, int x, char **map)
{
	int		bounded;
	char	initial;
	int		rows;

	rows = count_rows(map);
	initial = map[y][x];
	bounded = flood_fill(y, x, map, rows);
	psuedo_wall(map, rows);
	reset_map(map, rows);
	map[y][x] = initial;
	return (bounded);
}

// static char	**clone_map(char **map)
// {
// 	char	**clone;
// 	int		i;
// 	int		rows;

// 	i = 0;
// 	rows = count_rows(map);
// 	clone = malloc(sizeof(char *) * (rows + 1));
// 	while (i < rows)
// 	{
// 		clone[i] = ft_strdup(map[i]);
// 		i++;
// 	}
// 	clone[i] = 0;
// 	return (clone);
// }