/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_colors.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/11/28 14:26:47 by keyu              #+#    #+#             */
/*   Updated: 2024/11/28 14:26:48 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

static void	free_splitted(char **splitted)
{
	int	i;

	i = 0;
	while (splitted[i])
		free(splitted[i++]);
	free(splitted);
}

static int	count_splitted(char **splitted)
{
	int	i;

	i = 0;
	while (splitted[i])
		i++;
	return (i);
}

void	color_error(char *k, char **splitted, char *nl, t_config *conf)
{
	ft_putstr_fd("Error\nColor is "
		"incomplete / invalid (<0 or >255) / has non-digit chars: ",
		STDERR_FILENO);
	ft_putstr_fd(k, STDERR_FILENO);
	free_splitted(splitted);
	free(nl);
	destroy_config(conf);
	exit(EXIT_FAILURE);
}

static void	assign_color(char *k, char *nl, t_config *conf, int *array)
{
	int		i;
	int		j;
	char	**splitted;

	splitted = ft_split(k + 1, ',');
	if (count_splitted(splitted) == 3)
	{
		i = 0;
		while (i < 3)
		{
			j = 0;
			while (p_is_space(splitted[i][j]))
				j++;
			if (!splitted[i][j])
				color_error(k, splitted, nl, conf);
			check_valid_chars(splitted[i] + j, splitted, nl, conf);
			array[i] = ft_atoi(splitted[i]);
			if (array[i] < 0 || array[i] > 255)
				color_error(k, splitted, nl, conf);
			i++;
		}
	}
	free_splitted(splitted);
}

// returns (1) on success
int	parse_colors(char *k, char *nl, int *array, t_config *conf)
{
	if (array[0] != -1)
	{
		ft_putstr_fd("Error\nDuplicate key: ", STDERR_FILENO);
		write(STDERR_FILENO, k, 1);
		ft_putstr_fd("\n", STDERR_FILENO);
		free(nl);
		destroy_config(conf);
		exit(EXIT_FAILURE);
	}
	assign_color(k, nl, conf, array);
	return (1);
}
