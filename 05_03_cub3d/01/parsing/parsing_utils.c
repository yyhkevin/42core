/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/12/02 20:37:10 by keyu              #+#    #+#             */
/*   Updated: 2024/12/02 20:37:11 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

int	p_is_space(char c)
{
	return (c == 32 || (c >= 9 && c <= 13));
}

char	*cp_text_endl(char *src)
{
	int		i;
	int		j;
	char	*dest;

	while (p_is_space(*src))
		src++;
	i = 0;
	while (src[i] && !p_is_space(src[i]))
		i++;
	j = 0;
	dest = malloc(sizeof(char) * (i + 1));
	while (j < i)
	{
		dest[j] = src[j];
		j++;
	}
	dest[j] = 0;
	return (dest);
}

int	p_is_endl(char c)
{
	return (c == 0 || p_is_space(c));
}

void	set_config_default(t_config *config)
{
	config->map = 0;
	config->texture_n = 0;
	config->texture_s = 0;
	config->texture_e = 0;
	config->texture_w = 0;
	config->floor_color[0] = -1;
	config->ceiling_color[0] = -1;
}

void	check_config_ext(char *path)
{
	if ((int) ft_strlen(path) < 4)
		error_wrapper(WRONG_CONFIG_EXT);
	while (*path)
		path++;
	if (*(path - 4) == '.' && *(path - 3) == 'c'
		&& *(path - 2) == 'u' && *(path - 1) == 'b')
		return ;
	error_wrapper(WRONG_CONFIG_EXT);
}
