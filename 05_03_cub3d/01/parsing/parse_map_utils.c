/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_map_utils.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/12/02 20:46:40 by keyu              #+#    #+#             */
/*   Updated: 2024/12/02 20:46:41 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

// i <= 4 to include '\n' and is safe due to (int) type in atoi
static int	check_valid_num(char *k)
{
	int	i;
	int	neg;

	neg = 0;
	if (*k == '-')
	{
		neg = 1;
		k++;
	}
	while (*k == '0')
		k++;
	if (ft_isdigit(*k) && neg)
		return (0);
	i = 0;
	while (k[i])
		i++;
	return (i <= 4);
}

void	check_valid_chars(char *k, char **splitted, char *nl, t_config *conf)
{
	int	i;

	if (!check_valid_num(k))
		color_error(k, splitted, nl, conf);
	i = 0;
	while (k[i])
	{
		if (!ft_isdigit(k[i]))
		{
			if (!(k[i] == '\n' && k[i + 1] == 0))
				color_error(k, splitted, nl, conf);
		}
		i++;
	}
}

int	map_line_is_valid(char *s)
{
	while (*s)
	{
		if (*s != MAP_N && *s != MAP_S && *s != MAP_E && *s != MAP_W
			&& *s != MAP_0 && *s != MAP_1 && *s != ' ')
		{
			if (!(*s == '\n' && *(s + 1) == 0))
				return (0);
		}
		s++;
	}
	return (1);
}

void	error_map(char *nl, t_list *map, int fd, t_config *conf)
{
	ft_putstr_fd("Error\nIn map: ", STDERR_FILENO);
	ft_putstr_fd(nl, STDERR_FILENO);
	free(nl);
	ft_lstclear(&map, free);
	close(fd);
	destroy_config(conf);
	exit(EXIT_FAILURE);
}

void	error_map_break(char *nl, t_list *map, int fd, t_config *conf)
{
	ft_putstr_fd("Error\nBreak in map.\n", STDERR_FILENO);
	free(nl);
	ft_lstclear(&map, free);
	close(fd);
	destroy_config(conf);
	exit(EXIT_FAILURE);
}
