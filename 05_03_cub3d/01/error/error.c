/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ysng <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/10/23 14:45:20 by ysng              #+#    #+#             */
/*   Updated: 2024/12/04 13:14:46 by ysng             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../cub3d.h"

void	error_wrapper_cont(t_errorMsg err_msg)
{
	if (err_msg == MAP_IS_NULL)
		ft_putstr_fd("Map is NULL.\n", STDERR_FILENO);
	else if (err_msg == AVATAR_POSITION_MISSING)
		ft_putstr_fd("No starting position (N/S/E/W) found in the map!\n",
			STDERR_FILENO);
	else if (err_msg == TEXTURE_LOAD_FAIL)
		ft_putstr_fd("Failed to load png texture\n", STDERR_FILENO);
	else if (err_msg == MLX_MALLOC_ERROR)
		ft_putstr_fd("MLX connection failed\n", STDERR_FILENO);
	else if (err_msg == FD_OPEN_ERROR)
		ft_putstr_fd("failed to open file.\n", STDERR_FILENO);
	else if (err_msg == NO_CONFIG_FILE)
		ft_putstr_fd("no config file found / too many arguments.\n",
			STDERR_FILENO);
	else if (err_msg == WRONG_CONFIG_EXT)
		ft_putstr_fd("Only .cub configs are accepted.\n", STDERR_FILENO);
}

int	error_wrapper(t_errorMsg err_msg)
{
	ft_putstr_fd("Error:\n", STDERR_FILENO);
	if (err_msg == WRONG_ARG)
		ft_putstr_fd("Check you command\n", STDERR_FILENO);
	else if (err_msg == IS_DIR)
		ft_putstr_fd("This is a directory\n", STDERR_FILENO);
	else if (err_msg == IS_NOT_DOT_CUB)
		ft_putstr_fd("map must be in *.cub format\n", STDERR_FILENO);
	else if (err_msg == OPEN_FILE_FAILED)
		ft_putstr_fd("failed to open file\n", STDERR_FILENO);
	else if (err_msg == WINDOW_MALLOC_ERROR)
		ft_putstr_fd("failed to malloc for window\n", STDERR_FILENO);
	else if (err_msg == IMG_MALLOC_ERROR)
		ft_putstr_fd("failed to malloc for image\n", STDERR_FILENO);
	else if (err_msg == ADDR_MALLOC_ERROR)
		ft_putstr_fd("failed to malloc get data addr\n", STDERR_FILENO);
	else
		error_wrapper_cont(err_msg);
	exit(EXIT_FAILURE);
}
