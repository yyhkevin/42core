#pragma once

#include <iostream>
#include <cmath>

// default
// copy constructor
// copy assignment operator
// destructor
class Fixed
{
private:
    int _num;
    static int const _fbits;
public:
    Fixed();
    Fixed(int const i);
    Fixed(float const f);
    Fixed(const Fixed &f);
    Fixed & operator = (const Fixed &f);
    ~Fixed();
    int getRawBits( void ) const;
    void setRawBits( int const raw );
    float toFloat(void) const;
    int toInt(void) const;

    bool operator > (Fixed const &f) const;
    bool operator < (Fixed const &f) const;
    bool operator >= (Fixed const &f) const;
    bool operator <= (Fixed const &f) const;
    bool operator == (Fixed const &f) const;
    bool operator != (Fixed const &f) const;

    Fixed & operator++();
    Fixed & operator--();
    Fixed operator++(int);
    Fixed operator--(int);

    Fixed operator + (Fixed const &f);
    Fixed operator - (Fixed const &f);
    Fixed operator * (Fixed const &f);
    Fixed operator / (Fixed const &f);

    static Fixed & min(Fixed &a, Fixed &b);
    static Fixed const & min(Fixed const &a, Fixed const &b);
    static Fixed & max(Fixed &a, Fixed &b);
    static Fixed const & max(Fixed const &a, Fixed const &b);
};

std::ostream & operator<<(std::ostream &o, Fixed const &f);