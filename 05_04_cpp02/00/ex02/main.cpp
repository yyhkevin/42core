#include "Fixed.hpp"

int main(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    // Fixed const a( Fixed( 5.05f ) + Fixed( 2 ) );
    // Fixed const s( Fixed( 5.05f ) - Fixed( 2 ) );
    // Fixed const m( Fixed( 5.05f ) * Fixed( 2 ) );
    // Fixed const d( Fixed( 5.05f ) / Fixed( 2 ) );
    // std::cout << a << std::endl;
    // std::cout << s << std::endl;
    // std::cout << m << std::endl;
    // std::cout << d << std::endl;

    // bool aGTs = (a > s);
    // bool aLTs = (a < s);
    // std::cout << aGTs << aLTs << std::endl;

    // Fixed a0(5.05f);
    // Fixed a1(2);
    // std::cout << (a0 * a1 == a1 * a0) << std::endl;

    Fixed a;
    Fixed const b( Fixed( 5.05f ) * Fixed( 2 ) );
    std::cout << a << std::endl;
    std::cout << ++a << std::endl;
    std::cout << a << std::endl;
    std::cout << a++ << std::endl;
    std::cout << a << std::endl;
    std::cout << b << std::endl;
    std::cout << Fixed::max( a, b ) << std::endl;
    return 0;
}