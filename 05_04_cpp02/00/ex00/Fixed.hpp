#pragma once

#include <iostream>

// default
// copy constructor
// copy assignment operator
// destructor
class Fixed
{
private:
    int _num;
    static int const _fbits;
public:
    Fixed();
    Fixed(const Fixed &f);
    Fixed & operator = (const Fixed &f);
    ~Fixed();
    int getRawBits( void ) const;
    void setRawBits( int const raw );
};