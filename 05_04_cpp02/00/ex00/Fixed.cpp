#include "Fixed.hpp"

int const Fixed::_fbits = 8;

Fixed::Fixed() : _num(0) {
    std::cout << "Default constructor called" << std::endl;
    Fixed::_num = 0;
}

Fixed::Fixed(const Fixed &f) {
    std::cout << "Copy constructor called" << std::endl;
    *this = f;
}

Fixed &Fixed::operator = (const Fixed &f) {
    std::cout << "Copy assignment operator called" << std::endl;
    setRawBits(f.getRawBits());
    return *this;
}

Fixed::~Fixed() {
    std::cout << "Destructor called" << std::endl;
}

int Fixed::getRawBits( void ) const {
    std::cout << "getRawBits member function called" << std::endl;
    return _num;
}

void Fixed::setRawBits( int const raw ) {
    _num = raw;
}
