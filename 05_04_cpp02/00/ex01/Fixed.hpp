#pragma once

#include <iostream>
#include <cmath>

// default
// copy constructor
// copy assignment operator
// destructor
class Fixed
{
private:
    int _num;
    static int const _fbits;
public:
    Fixed();
    Fixed(int const i);
    Fixed(float const f);
    Fixed(const Fixed &f);
    Fixed & operator = (const Fixed &f);
    ~Fixed();
    int getRawBits( void ) const;
    void setRawBits( int const raw );
    float toFloat(void) const;
    int toInt(void) const;
};

std::ostream & operator<<(std::ostream &o, Fixed const &f);