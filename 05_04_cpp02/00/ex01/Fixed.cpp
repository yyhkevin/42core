#include "Fixed.hpp"

int const Fixed::_fbits = 8;

Fixed::Fixed() : _num(0) {
    std::cout << "Default constructor called" << std::endl;
}

Fixed::Fixed(int const i) {
    std::cout << "Int constructor called" << std::endl;
    this->_num = (i << _fbits);
}

Fixed::Fixed(float const f) {
    std::cout << "Float constructor called" << std::endl;
    // the following works because:
    // take a floating number e.g. 2.75 -> representation is ...000010.1100000...
    // then shift left by 8 to get the fixed number representation: ...001011000000, where last 8 bits are the fractional part
    // ---
    // take original number, multiply by fractional bits to get a large number, then scrape its fractional part
    // its bits will be a close representation
    this->_num = roundf(f * (1 << _fbits));
}

Fixed::Fixed(const Fixed &f) {
    std::cout << "Copy constructor called" << std::endl;
    *this = f;
}

Fixed &Fixed::operator = (const Fixed &f) {
    std::cout << "Copy assignment operator called" << std::endl;
    setRawBits(f.getRawBits());
    return *this;
}

Fixed::~Fixed() {
    std::cout << "Destructor called" << std::endl;
}

int Fixed::getRawBits( void ) const {
    // std::cout << "getRawBits member function called" << std::endl;
    return _num;
}

void Fixed::setRawBits( int const raw ) {
    _num = raw;
}

float Fixed::toFloat(void) const {
    // do the reverse process of obtaining Fixed num from float
    return (float) _num / (1 << _fbits);
}

int Fixed::toInt(void) const {
    // ignore the least 8 significant bits as they are fractional component
    return (_num >> _fbits);
}

std::ostream & operator<<(std::ostream &o, Fixed const &f) {
    o << f.toFloat();
    return o;
}