#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int	ft_contains_before(char *s, int len)
{
	int	i;

	i = 0;
	while (i < len)
	{
		if (s[len] == s[i])
			return (1);
		i++;
	}
	return (0);
}

int	ft_contains(char c, char *str)
{
	while (*str)
	{
		if (c == *str)
			return (1);
		str++;
	}
	return (0);
}

int	main(int argc, char **argv)
{
	int	i;

	i = 0;
	if (argc == 3)
	{
		while (argv[1][i])
		{
			if (!ft_contains_before(argv[1], i))
			{
				if (ft_contains(argv[1][i], argv[2]))
					ft_putchar(argv[1][i]);
			}
			i++;
		}
	}
	ft_putchar('\n');
	return (0);
}

/*
int	main(int argc, char **argv)
{
	int	i;

	i = 0;
	if (argc == 3)
	{
		while (argv[1][i] && *argv[2])
		{
			if (ft_contains(argv[1], i))
				i++;
			else if (argv[1][i] == *argv[2])
			{
				ft_putchar(argv[1][i]);
				i++;
			}
			argv[2]++;
		}
	}
	ft_putchar('\n');
	return (0);
}
*/
