#include <stdio.h>

int	ft_atoi(const char *str)
{
	int	i;
	int	is_neg;

	i = 0;
	is_neg = 0;
	while (*str == 32 || (*str >= 9 && *str <= 13))
		str++;
	if (*str == '-' || *str == '+')
	{
		if (*str == '-')
			is_neg = 1;
		str++;
	}
	while (*str >= '0' && *str <= '9')
		i = 10 * i + *str++ - '0';
	if (is_neg)
		i *= -1;
	return (i);
}

int	main(int argc, char **argv)
{
	printf("%d\n", ft_atoi(argv[1]));
	return (0);
}
