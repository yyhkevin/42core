#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int	ft_contains_before(char *str, int len)
{
	int	i;

	i = 0;
	while (i < len)
	{
		if (str[i] == str[len])
			return (1);
		i++;
	}
	return (0);
}

int	ft_contains(char *str, char c)
{
	while (*str)
	{
		if (*str == c)
			return (1);
		str++;
	}
	return (0);
}

int	main(int argc, char **argv)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	if (argc == 3)
	{
		while (argv[1][i])
		{
			if (!ft_contains_before(argv[1], i))
				ft_putchar(argv[1][i]);
			i++;
		}
		while (argv[2][j])
		{
			if (!ft_contains(argv[1], argv[2][j]) && !ft_contains_before(argv[2], j))
				ft_putchar(argv[2][j]);
			j++;
		}
	}
	ft_putchar('\n');
	return (0);
}
