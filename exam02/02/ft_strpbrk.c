int	ft_is_in(char c, char *set)
{
	while (*set)
	{
		if (c == *set)
			return (1);
		set++;
	}
	return (0);
}

char	*ft_strpbrk(const char *s1, const char *s2)
{
	if (!s1 || !s2)
		return (0);
	while (*s1)
	{
		if (ft_is_in(*s1, (char *) s2))
			return ((char *) s1);
		s1++;
	}
	return (0);
}

#include <stdio.h>
int	main(int argc, char **argv)
{
	char *c = ft_strpbrk(0, 0);
	printf("%c\n", *c);
	return (0);
}
