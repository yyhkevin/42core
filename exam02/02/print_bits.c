#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	print_bits(unsigned char octet)
{
	int	i;

	i = 7;
	while (i >= 0)
	{
		ft_putchar('0' + ((octet >> i) & 1));
		i--;
	}
}

int	main()
{
	print_bits(255);
}
