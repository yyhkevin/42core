#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int	ft_is_space(char c)
{
	if ((c >= 9 && c <= 13) || c == 32)
		return (1);
	return (0);
}

int	ft_is_printable(char c)
{
	return (c >= 33 && c <= 126);
}

int	main(int argc, char **argv)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	if (argc == 2)
	{
		while (argv[1][i])
		{
			if (ft_is_space(argv[1][i]) && ft_is_printable(argv[1][i+1]))
				j = i + 1;
			i++;
		}
		while (ft_is_printable(argv[1][j]))
			ft_putchar(argv[1][j++]);
	}
	ft_putchar('\n');
	return (0);
}
