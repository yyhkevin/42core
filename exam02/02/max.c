int	max(int* tab, unsigned int len)
{
	int	m;

	if (len > 0)
	{
		len--;
		m = tab[len];
		while (len > 0)
		{
			len--;
			if (tab[len] > m)
				m = tab[len];
		}
		return (m);
	}
	return (0);
}

#include <stdio.h>
int	main()
{
	int	tab[5] = {5, 4, 10, -5, 0};
	printf("%d\n", max(tab, 5));
}
