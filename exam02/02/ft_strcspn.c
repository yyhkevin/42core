#include <stdio.h>

int	ft_is_in(char c, const char *reject)
{
	while (*reject)
	{
		if (*reject == c)
			return (1);
		reject++;
	}
	return (0);
}

size_t	ft_strcspn(const char *s, const char *reject)
{
	int	i;

	i = 0;
	while (s[i])
	{
		if (ft_is_in(s[i], reject))
			return (i);
		i++;
	}
	return (i);
}
