#include <string.h>

int	ft_is_in(char c, char *set)
{
	while (*set)
	{
		if (*set == c)
			return (1);
		set++;
	}
	return (0);
}

size_t	ft_strspn(const char *s, const char *accept)
{
	int	i;

	i = 0;
	while (s[i])
	{
		if (ft_is_in(s[i], (char *) accept))
			return (i);
		i++;
	}
	return (i);
}
