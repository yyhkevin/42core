unsigned char	reverse_bits(unsigned char octet)
{
	int	i;
	int	bit;

	i = 0;
	bit = 0;
	while (bit < 8)
	{
		i <<= 1;
		i += ((octet >> bit) & 1);
		bit++;
	}
	return (i);
}

#include <stdio.h>
int	main()
{
	printf("%d\n", reverse_bits(3));
}
