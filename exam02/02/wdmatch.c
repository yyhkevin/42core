#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		ft_len(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

int	main(int argc, char **argv)
{
	int	i;

	i = 0;
	if (argc == 3)
	{
		while (argv[1][i] && *argv[2])
		{
			if (argv[1][i] == *argv[2])
			{
				i++;
				argv[2]++;
			}
			else
				argv[2]++;
		}
		if (i == ft_len(argv[1]))
			ft_putstr(argv[1]);
	}
	ft_putchar('\n');
	return (0);
}
