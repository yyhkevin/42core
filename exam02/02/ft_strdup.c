#include <stdlib.h>

char	*ft_strdup(char *src)
{
	int		len;
	char	*dup;
	int		i;

	len = 0;
	i = 0;
	while (src[len])
		len++;
	dup = malloc(sizeof(char) * (len + 1));
	while (i < len)
	{
		dup[i] = src[i];
		i++;
	}
	dup[i] = 0;
	return (dup);
}

#include <stdio.h>

int	main(int argc, char **argv)
{
	char	*p = ft_strdup("hello");
	printf("%s\n", p);
	free(p);
}
