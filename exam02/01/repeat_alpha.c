#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_repeatchar(char c, int count)
{
	while (count)
	{
		ft_putchar(c);
		count--;
	}
}

int	main(int argc, char **argv)
{
	if (argc == 2)
	{
		while (*argv[1])
		{
			if (*argv[1] >= 'A' && *argv[1] <= 'Z')
				ft_repeatchar(*argv[1], *argv[1] - 'A' + 1);
			else if (*argv[1] >= 'a' && *argv[1] <= 'z')
				ft_repeatchar(*argv[1], *argv[1] - 'a' + 1);
			else
				ft_putchar(*argv[1]);
			argv[1]++;
		}
	}
	ft_putchar('\n');
	return (0);
}
