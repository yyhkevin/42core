#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *s)
{
	while(*s)
		ft_putchar(*s++);
}

void	ft_writenum(int c)
{
	if (c > 9)
		ft_writenum(c / 10);
	c %= 10;
	write(1, &"0123456789"[c], 1);
}

int	main(int argc, char **argv)
{
	int	i;

	i = 1;
	while (i < 101)
	{
		if (i % 3 == 0 && i % 5 == 0)
			ft_putstr("fizzbuzz");
		else if (i % 3 == 0)
			ft_putstr("fizz");
		else if (i % 5 == 0)
			ft_putstr("buzz");
		else
			ft_writenum(i);
		ft_putchar('\n');
		i++;
	}
	return (0);
}
