#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int	ft_atoi(char *str)
{
	int	i;

	i = 0;
	while (*str == 32 || (*str >= 9 && *str <= 13))
		str++;
	while (*str >= '0' && *str <= '9')
		i = i * 10 + *str++ - '0';
	return (i);
}

void	ft_putnbr(int n)
{
	if (n >= 10)
		ft_putnbr(n / 10);
	n %= 10;
	ft_putchar('0' + n);
}

void	ft_pline(int b, int m)
{
	ft_putnbr(b);
	write(1, " x ", 3);
	ft_putnbr(m);
	write(1, " = ", 3);
	ft_putnbr(b * m);
	ft_putchar('\n');
}

int	main(int argc, char **argv)
{
	int i;

	i = 1;
	if (argc == 2)
	{
		while (i < 10)
			ft_pline(i++, ft_atoi(argv[1]));
	}
	else
		ft_putchar('\n');
	return (0);
}
