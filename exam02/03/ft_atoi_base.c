int	pos_in_base(char c, const char *base)
{
	int	i;

	i = 0;
	if (c >= 'A' && c <= 'Z')
		c += 32;
	while (base[i])
	{
		if (base[i] == c)
			return (i);
		i++;
	}
	return (-1);
}

int	ft_atoi_base(const char *str, int str_base)
{
	int	i;
	int	is_neg;

	i = 0;
	is_neg = 0;
	while (*str == 32 || (*str > 9 && *str <= 13))
		str++;
	if (*str == '+' || *str == '-')
	{
		if (*str == '-')
			is_neg = 1;
		str++;
	}
	while (pos_in_base(*str, "0123456789abcdef") != -1)
	{
		i = i * str_base + pos_in_base(*str, "0123456789abcdef");
		str++;
	}
	if (is_neg)
		i *= -1;
	return (i);
}

#include <stdio.h>
#include <stdlib.h>
int	main(int argc, char **argv)
{
	printf("%d\n", ft_atoi_base(argv[1], atoi(argv[2])));
}
