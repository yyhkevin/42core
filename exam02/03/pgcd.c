#include <stdio.h>
#include <stdlib.h>

int	pgcd(int a, int b)
{
	int	tmp;

	while (b != 0)
	{
		tmp = a;
		a = b;
		b = tmp % b;
	}
	return (a);
}

int	main(int argc, char **argv)
{
	int	a;
	int	b;

	if (argc == 3)
	{
		a = atoi(argv[1]);
		b = atoi(argv[2]);
		printf("%d", pgcd(a, b));
	}
	printf("\n");
	return (0);
}
