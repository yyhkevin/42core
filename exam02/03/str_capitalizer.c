#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	capitalizer(char *str)
{
	int	is_word;

	while (*str)
	{
		is_word = 0;
		while (*str == 32 || (*str >= 9 && *str <= 13))
			ft_putchar(*str++);
		if (*str)
		{
			if (*str >= 'a' && *str <= 'z')
				*str -= 32;
			is_word = 1;
			ft_putchar(*str);
			str++;
		}
		while (is_word && *str >= 33 && *str <= 127)
		{
			if (*str >= 'A' && *str <= 'Z')
				*str += 32;
			ft_putchar(*str);
			str++;
		}
	}
}

int	main(int argc, char **argv)
{
	int	i;

	if (argc >= 2)
	{
		i = 1;
		while (i < argc)
		{
			capitalizer(argv[i]);
			i++;
			ft_putchar('\n');
		}
	}
	else
		ft_putchar('\n');
	return (0);
}
