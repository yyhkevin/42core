#include <unistd.h>

void	ft_puthex(int i)
{
	if (i > 15)
		ft_puthex(i / 16);
	i %= 16;
	write(1, &"0123456789abcdef"[i], 1);
}

int	ft_atoi(char *str)
{
	int	i;

	i = 0;
	while (*str == 32 || (*str >= 9 && *str <= 13))
		str++;
	if (*str == '+' || *str == '-')
		str++;
	while (*str >= '0' && *str <= '9')
		i = i * 10 + *str++ - '0';
	return (i);
}

int	main(int argc, char **argv)
{
	if (argc == 2)
		ft_puthex(ft_atoi(argv[1]));
	write(1, "\n", 1);
	return (0);
}
