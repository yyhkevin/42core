#include <unistd.h>

void	ft_putnbr(int n)
{
	if (n > 9)
		ft_putnbr(n / 10);
	n %= 10;
	write(1, &"0123456789"[n], 1);
}

int	is_prime(int num)
{
	int	i;

	if (num < 2)
		return (0);
	i = 2;
	while (i <= num / i)
	{
		if (num % i == 0)
			return (0);
		i++;
	}
	return (1);
}

int	sums_to_num(int num)
{
	int	i;
	int	sum;

	i = 2;
	sum = 0;
	while (i <= num)
	{
		if (is_prime(i))
			sum += i;
		i++;
	}
	return (sum);
}

int	ft_atoi(char *str)
{
	int	i;
	int	is_neg;

	i = 0;
	is_neg = 0;
	while (*str == 32 || (*str >= 9 && *str <= 13))
		str++;
	if (*str == '-' || *str == '+')
	{
		if (*str == '-')
			is_neg = 1;
		str++;
	}
	while (*str >= '0' && *str <= '9')
		i = i * 10 + *str++ - '0';
	if (is_neg)
		i *= -1;
	return (i);
}

int	main(int argc, char **argv)
{
	int	num;

	if (argc == 2)
	{
		num = ft_atoi(argv[1]);
		if (num <= 0)
			write(1, "0", 1);
		else
			ft_putnbr(sums_to_num(num));
	}
	else
		write(1, "0", 1);
	write(1, "\n", 1);
	return (0);
}
