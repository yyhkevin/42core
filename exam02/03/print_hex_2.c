#include <unistd.h>

void ft_putchar(char c)
{
	write(1, &c, 1);
}

int	ft_atoi(char *str)
{
	int	i;
	int	is_neg;

	is_neg = 0;
	i = 0;
	while (*str == 32 || (*str >= 9 && *str <= 13))
		str++;
	if (*str == '-' || *str == '+')
	{
		if (*str == '-')
			is_neg = 1;
		str++;
	}
	while (*str >= '0' && *str <= '9')
		i = i * 10 + *str++ - '0';
	if (is_neg)
		i *= -1;
	return (i);
}

void	ft_printhex(int n)
{
	if (n >= 16)
		ft_printhex(n / 16);
	n %= 16;
	ft_putchar("0123456789abcdef"[n]);
}

int	main(int argc, char **argv)
{
	if (argc == 2)
		ft_printhex(ft_atoi(argv[1]));
	ft_putchar('\n');
	return (0);
}
