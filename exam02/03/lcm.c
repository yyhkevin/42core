unsigned int	pgcd(unsigned int a, unsigned int b)
{
	int	tmp;

	while (b != 0)
	{
		tmp = a;
		a = b;
		b = tmp % b;
	}
	return (a);
}

unsigned int	lcm(unsigned int a, unsigned int b)
{
	return (a * b / pgcd(a, b));
}

#include <stdio.h>
#include <stdlib.h>
int	main(int argc, char **argv)
{
	int	a;
	int	b;

	a = atoi(argv[1]);
	b = atoi(argv[2]);
	printf("%d\n", lcm(a, b));
}
