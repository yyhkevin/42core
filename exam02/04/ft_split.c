#include <stdlib.h>

static int	issep(char c)
{
	return (c == 32 || (c >= 9 && c <= 13));
}

static int	count_words(char *str)
{
	int	i;

	i = 0;
	while (*str)
	{
		while (issep(*str))
			str++;
		if (*str)
			i++;
		while (*str >= 33 && *str <= 127)
			str++;
	}
	return (i);
}

static void	str_cpy(char *to, char *from, int start_index, int len)
{
	int	i;

	i = 0;
	while (i < len)
	{
		*to = from[start_index + i];
		i++;
		to++;
	}
	*to = 0;
}

static void	allocate_words(char *str, char **container)
{
	int	i;
	int	j;
	int	is_word;

	i = 0;
	while (str[i])
	{
		is_word = 0;
		while (issep(str[i]))
			i++;
		if (str[i])
		{
			is_word = 1;
			j = i;
		}
		while (is_word && str[i] >= 33 && str[i] <= 127)
			i++;
		if (is_word)
		{
			*container = malloc(sizeof(char) * (i - j + 1));
			str_cpy(*container, str, j, i - j);
			container++;
		}
	}
	*container = 0;
}

char	**ft_split(char *str)
{
	char	**splitted;

	splitted = malloc(sizeof(char *) * (count_words(str) + 1));
	allocate_words(str, splitted);
	return (splitted);
}

/*
int	main(int argc, char **argv)
{
	char	**splitted;

	splitted = ft_split(argv[1]);
	while (*splitted)
		printf("%s\n", *splitted++);
	return (0);
}
*/
