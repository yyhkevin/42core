/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/20 17:04:57 by keyu              #+#    #+#             */
/*   Updated: 2023/10/05 12:17:05 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <unistd.h>

int	ft_printf(const char *fmt, ...);
int	ft_putchar(char c);
int	ft_puthex(unsigned long int i, char isUpper, char requirePrefix);
int	ft_itoa_long(unsigned int n);
int	ft_itoa(int i);

// print specified utils
int	p_c(va_list args);
int	p_d(va_list args);
int	p_u(va_list args);
int	p_s(va_list args);
int	p_p(va_list args);
int	p_x(va_list args, char isUpper);

#endif
