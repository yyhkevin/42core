/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_p_others.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/05 12:14:49 by keyu              #+#    #+#             */
/*   Updated: 2023/10/05 12:17:34 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	p_c(va_list args)
{
	int	a;

	a = va_arg(args, int);
	return (ft_putchar(a));
}

int	p_d(va_list args)
{
	int	n;

	n = va_arg(args, int);
	return (ft_itoa(n));
}

int	p_u(va_list args)
{
	unsigned int		n;

	n = (unsigned int)va_arg(args, unsigned int);
	return (ft_itoa_long(n));
}

int	p_s(va_list args)
{
	int		count;
	char	*p;

	count = 0;
	p = va_arg(args, char *);
	if (!p)
	{
		write(1, "(null)", 6);
		return (6);
	}
	while (*(p + count))
		write(1, p + count++, 1);
	return (count);
}
