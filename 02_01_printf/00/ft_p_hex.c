/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_p_hex.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/05 12:13:33 by keyu              #+#    #+#             */
/*   Updated: 2023/10/05 12:17:29 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	p_p(va_list args)
{
	unsigned long int	i;

	i = (unsigned long int)va_arg(args, void *);
	if (!i)
	{
		write(1, "(nil)", 5);
		return (5);
	}
	return (ft_puthex(i, 0, 1));
}

int	p_x(va_list args, char isUpper)
{
	unsigned int	i;

	i = (unsigned int)va_arg(args, unsigned int);
	return (ft_puthex(i, isUpper, 0));
}
