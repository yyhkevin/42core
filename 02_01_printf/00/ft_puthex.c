/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puthex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/10/04 17:00:22 by keyu              #+#    #+#             */
/*   Updated: 2023/10/05 11:55:44 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static	int	recur(unsigned long int i, char *b16charset)
{
	int	length;

	length = 0;
	if (i > 15)
		length += recur(i / 16, b16charset);
	i %= 16;
	write(1, b16charset + i, 1);
	length++;
	return (length);
}

int	ft_puthex(unsigned long int i, char isUpper, char requirePrefix)
{
	char	*b16charset;
	int		len;

	len = 0;
	if (isUpper)
		b16charset = "0123456789ABCDEF";
	else
		b16charset = "0123456789abcdef";
	if (requirePrefix)
	{
		write(1, "0x", 2);
		len += 2;
	}
	len += recur(i, b16charset);
	return (len);
}
