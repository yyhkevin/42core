/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: keyu <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/20 16:54:25 by keyu              #+#    #+#             */
/*   Updated: 2023/10/05 12:17:13 by keyu             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static	int	p_specified(char c, va_list args)
{
	if (c == 'c')
		return (p_c(args));
	else if (c == '%')
		return (ft_putchar('%'));
	else if (c == 'd' || c == 'i')
		return (p_d(args));
	else if (c == 'u')
		return (p_u(args));
	else if (c == 's')
		return (p_s(args));
	else if (c == 'p')
		return (p_p(args));
	else if (c == 'x' || c == 'X')
		return (p_x(args, c == 'X'));
	return (0);
}

int	ft_printf(const char *fmt, ...)
{
	va_list	args;
	int		len;

	len = 0;
	va_start(args, fmt);
	while (*fmt)
	{
		if (*fmt == '%')
		{
			len += p_specified(*(++fmt), args);
			fmt++;
		}
		else
		{
			write(1, fmt++, 1);
			len++;
		}
	}
	va_end(args);
	return (len);
}
